#-------------------------------------------------------------------------------------------------------------#
# API source code for WebGUI Test                                
#                                                                              
#  Simple test written by Philip Shen  8/17/05                      
# Purpose: For WebGUI test functionality:                   
# 
#
#------------------------------------------------------------------------------------------------------------#

   #includes:
   require 'watir'   # the watir controller
   require 'watir/exceptions'
#-------------------------------------------------------------------------------------------------------------#
# class WindowHelper                               
#                                                                              
# Purpose: 
#------------------------------------------------------------------------------------------------------------#
class WindowHelper
require 'win32ole'

	def initialize( )
		  @autoit = WIN32OLE.new('AutoItX3.Control')
		  @rtuStr=String.new
  end
  
	def push_alert_button()
		  @autoit.WinWait "Microsoft Internet Explorer", ""
		  @autoit.Send "{ENTER}"
	end
  
	def push_confirm_button_ok()
	#	if @autoit.WinWait "Microsoft Internet Explorer","" then
	#		@rtuStr=@autoit.ControlGetText("Microsoft Internet Explorer", "", "Static2")
	#		@autoit.Send "{ENTER}"
	#		@autoit.Send "{ENTER}"
	#		@autoit.Send "{ENTER}"
	#		@rtuStr=@rtuStr+"\n"
	#		Dump_File.new("popup.txt",@rtuStr).dump_results
	#	else
	#		next
	#	end
	#	
    @autoit.WinWait "Microsoft Internet Explorer",""
    @autoit.Send "{ENTER}"
	end
  
	def push_confirm_button_cancel()
	  	@autoit.WinWait "Microsoft Internet Explorer", ""
	  	@autoit.Send "{ESCAPE}"
	end

	def push_security_alert_yes()
	  	@autoit.WinWait "Security Alert", ""
	  	@autoit.Send "{TAB}"
	  	@autoit.Send "{TAB}"
	  	@autoit.Send "{SPACE}"
	end

  
	def logon(title,name = 'john doe',password = 'john doe')
		  @autoit.WinWait title, ""
	  	@autoit.Send name
	  	@autoit.Send "{TAB}"
	  	@autoit.Send password
	  	@autoit.Send "{ENTER}"
	end
  
  def WindowHelper.check_autoit_installed
      begin
          WIN32OLE.new('AutoItX3.Control')
      rescue
          raise Watir::Exception::WatirException, "The AutoIt dll must be correctly registered for this feature to work properly"
      end
  end

	def Terminates_Browser_IE()
	#@autoit.ProcessClose("IExplorer.exe")
	
	@PID = @autoit.ProcessExists("IExplorer.exe")
	Show_Outcomes.new(" Close existed IE PID=#{@PID}.").show_results
		if @PID then
			@autoit.ProcessClose(@PID)
		end

	end

	def Weblogon(title,name = 'admin',password = 'admin')
	#  	@autoit.WinWait title, ""
      @autoit.Send name
	  	@autoit.Send "{TAB}"
	  	@autoit.Send password
	  	@autoit.Send "{ENTER}"
	end

  def Navigator_WAN0_DHCPPage
    #------------------------------
    # TAB*3          ----->wan_proto_sel selectlist
    # Home+pulldown*1----->static ip
    #------------------------------
    #@autoit.Send "{HOME}"    #@autoit.Send "{DOWN}"
    hKeyBoard=KeyBoard_Event.new
    hKeyBoard.Tab_Key
    hKeyBoard.Tab_Key
    hKeyBoard.Tab_Key
    
    hKeyBoard.HOME_Key
  end
  
  def Navigator_WAN0_StaticPage(stropt=nil)
    hKeyBoard=KeyBoard_Event.new
    
    if stropt.downcase == "afterdhcppage"
      #------------------------------
      # TAB*3          ----->wan_proto_sel selectlist
      # Home+pulldown*1----->static ip
      #------------------------------
      
      hKeyBoard.Tab_Key
      hKeyBoard.Tab_Key
      hKeyBoard.Tab_Key
    end
    
    hKeyBoard.DOWN_Key
    hKeyBoard.teardown
    
  end
  
  def Navigator_WAN0_StaticIP(str_wanip)
    #------------------------------
    # TAB*3        ----->go thru Navigator_WAN0_StaticPage process
    # TAB          ----->key in 10
    # TAB          ----->key in 5
    # TAB          ----->key in 30
    # TAB          ----->key in 226
    #------------------------------
    hKeyBoard=KeyBoard_Event.new
    
    hKeyBoard.Tab_Key;
    hKeyBoard.Tab_Key;
    hKeyBoard.Tab_Key
    
    str_wanip.split('.').each{|x|
      hKeyBoard.Tab_Key
      
      str_result=" Keyin:"+x
      Show_Outcomes.new(str_result).show_results()
      @autoit.Send x.to_s
    };#str_wanip.split('.').each
    
    hKeyBoard.teardown
  end
  
  
  def Typing_Data(str)
    str.split('.').each{|x|
      @autoit.Send "{TAB}"
      
      str_result=" Keyin:"+x
      Show_Outcomes.new(str_result).show_results()
      @autoit.Send x.to_s
    };#str_wanip.split('.').each
  end
  def Keyin_String(strdata)
    @autoit.Send "{TAB}"
    
    @autoit.Send "{END}"
    #-Shift + HOME
    @autoit.Send("+{HOME}") 
    
    str_result=" Keyin:"+strdata
    Show_Outcomes.new(str_result).show_results()
	  @autoit.Send strdata
  end
end
#-------------------------------------------------------------------------------------------------------------#
# class Data_DB                               
#                                                                              
# Purpose: Query setting parameter from Access database                  
#------------------------------------------------------------------------------------------------------------#
class Data_DB
  def initialize(srcfile,dbfilename,testcase)
    @srcfile=srcfile
    @dbfilename=dbfilename
    @testcase=testcase
  end
  
  def dump_setting_file()
    @syscmd=String.new()
    @syscmd=	"c:/tcl/bin/tclsh83.exe DUMPDATA-"
    @syscmd << @srcfile
    @syscmd << ' '
    @syscmd << @dbfilename
    @syscmd << ' '
    @syscmd << @testcase
  
    #puts syscmd
    system(@syscmd)
  end
  def teardown
    @srcfile=nil
    @dbfilename=nil
    @testcase=nil
    @syscmd=nil
  end
  
end

#-------------------------------------------------------------------------------------------------------------#
# class Browser_IE_Initial
# class Browser_IE_Close
# class Browser_IE_Status
#                                                                             
# Purpose: Open IE, Login DUT and Terimate IE                
#------------------------------------------------------------------------------------------------------------#
class Browser_IE_Initial
  def initialize(dut_ip,username='admin', password='admin')
    @dut_ip=dut_ip
    @test_site=String.new
    @str_result=String.new
    @str_windir=String.new
    @str_AlterTitle=String.new
    @username=username
    @password=password
    
    #return true
  end
  
  def start_ie_with_logger
     $ie = Watir::IE.new()
     $ie.set_fast_speed
  end
  
  def goto_webpag(strlang=nil,str_traget=nil)
    #Browser_IE_Initial.new(@dut_ip).chk_OSVer(strlang)
    self.chk_OSVer(strlang)
    
    @test_site="http://" + @dut_ip + "/"+str_traget
    #puts "test_site=#{@test_site}"
    #puts "#{@str_windir}; str_AlterTitle=#{@str_AlterTitle}"
      a = Thread.new {
		#system('ruby WegLogonDiag.rb')
		$ie.goto(@test_site)
		$ie.waitForIE			
		@str_result=" Action: entered #{@test_site} in the address bar."
		       
		#Show_Outcomes.new(@str_result).show_results()
    Show_Outcomes.new(@str_result).log_screen()
		}
      b = Thread.new { 
		WindowHelper.new.logon(@str_AlterTitle,@username,@password)
		}
      a.join
      b.join
      
      return true
  end

  def webpag_login()
    @test_site="http://" + @dut_ip + "/"
    
#    a = Thread.new {
      @str_result=" Action: entered #{@test_site} in the address bar."
      #Show_Outcomes.new(@str_result).show_results()
      Show_Outcomes.new(@str_result).log_screen()
      $ie.goto(@test_site)
      $ie.waitForIE
#    }
#		b = Thread.new { 
      WindowHelper.new.Weblogon(nil)
#    }
    
#    a.join
#    b.join
  end
  
  def goto_webpage_TChinese()
    
  end
  
  def chk_OSVer(lang_pack=nil)
    
    case lang_pack.downcase
      when /tc/
        @str_AlterTitle = "連線到 " + @dut_ip    
      when /sc/
        @str_AlterTitle = "連線到 " + @dut_ip
      else
        @str_AlterTitle = "Connect to " + @dut_ip  
    end
    
    @str_windir=ENV['windir']
    #if (@str_windir.include? "WINDOWS") then
    #  @str_AlterTitle = "Connect to " + @dut_ip
    #else
    #  @str_AlterTitle="Enter Network Password"
    #end
  end

  def teardown
    @test_site=nil
    @str_result=nil
  end
  
end

class Browser_IE_Close
  def end_ie
     $ie.close
     
     hshow=Show_Outcomes.new(' End Test.').show_results()
     
     #return true
  end
end

class Browser_IE_Status
  def initialize()
    @browser_status=String.new()
  end
  
  def browser_status()
    @browser_status=$ie.status() 
    @str_result=" IE Browser status=#{@browser_status}."
    Show_Outcomes.new(@str_result).show_results()
    return @browser_status
  end
  
end

class Browser_IE_Navigate
  def initialize(test_site)
    @test_site=test_site
  end
  def browser_navigate()
    $ie.goto(@test_site) 
    #Show_Outcomes.new(" Browser navigate #{@test_site}.").show_results()
    Show_Outcomes.new(" Browser navigate #{@test_site}.").log_screen
  end
  
end
#-------------------------------------------------------------------------------------------------------------#
# class Show_Outcomes
#                                                                              
# Purpose: Output execute results to screen
#
# Sep8/2005  Add logger library
#------------------------------------------------------------------------------------------------------------#
class Show_Outcomes
require 'logger'

  def initialize(str_out,logfile=nil)
    @str_out=str_out
    @str_time=Time.now
    @strtemp=String.new
    @log = Logger.new(STDERR)
    @logfile = logfile
    @log_file = Logger.new(@logfile)
  end
  
  def show_results()
    @strtemp<<@str_time.strftime(" %m/%d/%Y %H:%M:%S")
    @strtemp<<@str_out
    puts @strtemp
  end

  def log_screen()
    @log.level = Logger::INFO#DEBUG	,Default.
    @log.datetime_format = "%d%b%Y@%H:%M:%S"
    do_log_screen
  end
  
  def do_log_screen()
    @log.info { @str_out }
  end

  def log_logfile()
    @log_file.level = Logger::INFO#DEBUG	,Default.
    @log_file.datetime_format = "%d%b%Y@%H:%M:%S"
    do_log_logfile
  end

  def do_log_logfile()
    @log_file.info { @str_out }
  end
#  def teardown
#    @str_out=nil
#    @str_time=nil
#    @strtemp=nil
#  end
  
end
#-------------------------------------------------------------------------------------------------------------#
# class Show_Outcomes
#                                                                              
# Purpose: Output execute results to screen    
#------------------------------------------------------------------------------------------------------------#
class Dump_File
  def initialize(str_outfile,strContent,mode='w')
    @str_outfile=str_outfile
    @strContent=strContent
    #@strtemp=String.new
    @hFile=File.new(@str_outfile,mode)
  end
  
  def dump_results()
    #@strtemp<<@strCnt
    @hFile.write(@strContent)
    @hFile.close
  end

end
#-------------------------------------------------------------------------------------------------------------#
# class API_WebGUI_SelectBox
#                                                                              
# Purpose: Execute WebGUI test method of SelectLists    
#------------------------------------------------------------------------------------------------------------#
class API_WebGUI_SelectBox
  def initialize(objname,objvalue)
    @objname=objname
    @objvalue=objvalue
    @str_result=String.new()
    @array_item=Array.new()
  end
  
  def selectBox_existence
    return $ie.selectBox(:name, @objname).exists?
  end
  
  def selectBox_enabled
    return $ie.selectBox(:name, @objname).enabled?
  end
  
  def selectBox_setSelectedItems
    @array_item=$ie.select_list( :name , @objname).getAllContents() 
    @objvalue=@objvalue.to_i
    @str_result=" Action: select=#{@objname}; value=#{@array_item[@objvalue-1]}"
    Show_Outcomes.new(@str_result).show_results()
    return $ie.select_list( :name ,@objname).select(@array_item[@objvalue-=1])
  end

  def selectBox_getSelectedItems
    @array_item=$ie.select_list( :name , @objname).getSelectedItems()
    @str_result=" Get: select=#{@objname}; value=#{@array_item[0]}."
    Show_Outcomes.new(@str_result).show_results()
    return @array_item
  end

end
#-------------------------------------------------------------------------------------------------------------#
# class API_WebGUI_TextField
#                                                                              
# Purpose: Execute WebGUI test method of TextFiled  
#------------------------------------------------------------------------------------------------------------#
class API_WebGUI_TextField
  def initialize(objname,objvalue)
    @objname=objname
    @objvalue=objvalue
    @str_result=String.new()
    @setvalue=String.new()
  end
 
  def textField_existence
    return $ie.text_field(:name, @objname).exists?
  end
 
  def textField_enabled
    return $ie.text_field(:name, @objname).enabled?
  end
  
  def textField_setFieldValue
   @str_result=" Action: entered text field=#{@objname} value=#{@objvalue}"
   Show_Outcomes.new(@str_result).show_results()
   return $ie.text_field(:name, @objname).set(@objvalue)
  end

  def textField_getFieldValue
   @setvalue=$ie.text_field(:name, @objname).getContents()
   @str_result=" Get: text field=#{@objname} value=#{@setvalue}"
   Show_Outcomes.new(@str_result).show_results()
   return @setvalue
  end

end
#-------------------------------------------------------------------------------------------------------------#
# class API_WebGUI_Radio
#                                                                              
# Purpose: Execute WebGUI test method of Radio
#------------------------------------------------------------------------------------------------------------#
class API_WebGUI_Radio
  def initialize(objname,objvalue)
    @objname=objname
    @objvalue=objvalue
    @str_result=String.new()
  end
  
  def radio_existence
    return $ie.radio(:name, @objname,@objvalue).exists?
  end
  
  def radio_enabled
    return $ie.radio(:name, @objname,@objvalue).enabled?
  end
  
  def radio_click
    @str_result=" Action: clicked radio=#{@objname}."
    Show_Outcomes.new(@str_result).show_results()
    return $ie.radio(:name, @objname,@objvalue).click  
  end
  
  def radio_clear
    @str_result=" Action: clear radio=#{@objname}."
    Show_Outcomes.new(@str_result).show_results()
    return $ie.radio(:name, @objname,@objvalue).clear
  end

end
#-------------------------------------------------------------------------------------------------------------#
# class API_WebGUI_Button
#                                                                              
# Purpose: Execute WebGUI test method of Button
#------------------------------------------------------------------------------------------------------------#
class API_WebGUI_Button
  def initialize(objname)
    @objname=objname
    @str_result=String.new()
  end
  def button_existence
    return $ie.button(:name, @objname).exists?
  end
  
  def button_enabled
    return $ie.button(:name, @objname).enabled?
  end
  
  def button_click
    @str_result=" Action: clicked button=#{@objname}."
    Show_Outcomes.new(@str_result).show_results()
    return $ie.button(:name, @objname).click 
  end
  
  def button_value_click
    @str_result=" Action: clicked button_value=#{@objname}."
    Show_Outcomes.new(@str_result).show_results()
    return $ie.button(:value, @objname).click 
  end
end

class API_WebGUI_CheckBox
  def initialize(objname)
    @objname=objname
    @str_result=String.new()
  end
  def checkbox_existence
    return $ie.checkbox(:name, @objname).exists?
  end
  
  def checkbox_enabled
    return $ie.checkbox(:name, @objname).enabled?
  end

  def checkbox_set
    @str_result=" Action: set checkbox=#{@objname}."
    Show_Outcomes.new(@str_result).show_results()
    return $ie.checkbox(:name, @objname).set 
  end
  
end
class API_WebGUI_Attach_ExistingIEWindow
  def initialize(objvalue)
    @objvalue=objvalue
  end
  def attach_url
    @str_result=" Action: attach existingIEwindow url=#{@objvalue}."
    Show_Outcomes.new(@str_result).show_results()
    case Watir::IE::VERSION
      when /1.5.1/
        $ie._attach_init(:url, @objvalue)
      when /1.4.1/
        $ie.attach_init(:url, @objvalue)
    end
    
  end
  def attach_title
    @str_result=" Action: attach existingIEwindow title=#{@objvalue}."
    Show_Outcomes.new(@str_result).show_results()
    case Watir::IE::VERSION
      when /1.5.1/
        $ie._attach_init(:title, @objvalue)
      when /1.4.1/
        $ie.attach_init(:title, @objvalue)
    end
  end  
end
#-------------------------------------------------------------------------------------------------------------#
# class API_WebGUI_Test_SelectBox
#                                                                              
# Purpose: Execute WebGUI test item of Select Box
#------------------------------------------------------------------------------------------------------------#
class API_WebGUI_Test_SelectBox
  def initialize(what,how)
    @what=what
    @how=how
   #object_select_list(param[2],param[3])
    @hSelectBox=API_WebGUI_SelectBox.new(@what,@how)
  end

  def Do_test
    begin
  	@hSelectBox.selectBox_existence()
    rescue false
	next
    end
      Show_Outcomes.new(" SelectBox #{@what} is exist.").show_results()
 		
    begin
	@hSelectBox.selectBox_enabled()
   rescue false
	Show_Outcomes.new(" SelectBox #{@what} is disabled.").show_results()
	next
   end
		
   begin
	@hSelectBox.selectBox_setSelectedItems()
   rescue Watir::Exception::ObjectDisabledException
	Show_Outcomes.new(" SelectBox #{@what} can't set.").show_results()
	next		
   end
		
   begin
	@hSelectBox.selectBox_getSelectedItems()
   rescue Watir::Exception::ObjectDisabledException
	Show_Outcomes.new(" SelectBox #{@what} can't get.").show_results()
	#next		
   end
  
 end
  
end
#-------------------------------------------------------------------------------------------------------------#
# class API_WebGUI_Test_TextField
#                                                                              
# Purpose: Execute WebGUI test item of Text Field
#------------------------------------------------------------------------------------------------------------#
class API_WebGUI_Test_TextField
  def initialize(what,how)
    @what=what
    @how=how
   #object_select_list(param[2],param[3])
    @hTextField=API_WebGUI_TextField.new(@what,@how)
  end

  def Do_test
	 begin
		@hTextField.textField_existence()
	rescue false
		next
	end
	Show_Outcomes.new(" Textfield #{@what} is exist.").show_results()
			
	begin
		@hTextField.textField_enabled()
	rescue false
		Show_Outcomes.new(" Textfield #{@what} is disabled.").show_results()
		next
	end
		
	begin
		#a = Thread.new {
		#	system('ruby WegConfirmDiag.rb')
		# }
	        #b = Thread.new { 
			@hTextField.textField_setFieldValue()
		#}
	        #b.join
	        #a.join
	rescue Watir::Exception::ObjectDisabledException
		#next
		Show_Outcomes.new(" Textfield #{@what} can't set.").show_results()
				
	end
		
	@hTextField.textField_getFieldValue()
  
  end

end
#-------------------------------------------------------------------------------------------------------------#
# class API_WebGUI_Test_Radio
#                                                                              
# Purpose: Execute WebGUI test item of Radio
#------------------------------------------------------------------------------------------------------------#
class API_WebGUI_Test_Radio
  def initialize(what,how)
    @what=what
    @how=how
   #object_select_list(param[2],param[3])
    @hRadio=API_WebGUI_Radio.new(@what,@how)
  end

  def Do_test
	begin
		@hRadio.radio_existence()
	rescue false
		next
	end
	Show_Outcomes.new(" Radio #{@what} value#{@how} is exist.").show_results()
			
		
	begin
		@hRadio.radio_enabled()
	rescue false
		Show_Outcomes.new(" Radio #{@what} value#{@how} is disabled.").show_results()
		next
	end
		
	begin
		@hRadio.radio_click()
	rescue Watir::Exception::ObjectDisabledException
		Show_Outcomes.new(" Radio #{@what} can't click.").show_results()
		#next
	end
  
  end

end
#-------------------------------------------------------------------------------------------------------------#
# class API_WebGUI_Test_Button
#                                                                              
# Purpose: Execute WebGUI test item of Button
#------------------------------------------------------------------------------------------------------------#
class API_WebGUI_Test_Button
  def initialize(what)
    @what=what
   # @how=how
   #object_select_list(param[2],param[3])
    @hButton= API_WebGUI_Button.new(@what)
  end

  def Do_test
	 begin
		@hButton.button_existence()
	rescue false
		next
	end
	Show_Outcomes.new(" Button #{@what} is exist.").show_results()
		
	begin
	#	a = Thread.new {
	#		system('ruby WegConfirmDiag.rb')
	#	 }
	#        b = Thread.new { 
			@hButton.button_click()
	#	}
	#        b.join
	#        a.join
	rescue Watir::Exception::ObjectDisabledException
		Show_Outcomes.new(" Button #{@what} can't click.").show_results()
		#next
	end
  end
  
end
#-------------------------------------------------------------------------------------------------------------#
# class API_WebGUI_TextField_FireEvent
#                                                                              
# Purpose: Execute WebGUI test method of TextFiled  
#------------------------------------------------------------------------------------------------------------#
class API_WebGUI_TextField_FireEvent
  def initialize(objname,objvalue)
    @objname=objname
    @objvalue=objvalue
    @str_result=String.new()
  end
 
  def textField_enabled
    return $ie.text_field(:name, @objname).enabled?
  end
  
  def textField_setFieldValue
   @str_result=" Action: entered text field=#{@objname} value=#{@objvalue}"
   Show_Outcomes.new(@str_result).show_results()
   return $ie.text_field(:name, @objname).set(@objvalue)
  end

  def textField_FireEvent
   
   @str_result=" Fire Event= onBlur"
   Show_Outcomes.new(@str_result).show_results()
   @setvalue=$ie.text_field(:name, @objname).fireEvent("onBlur")
   #return @setvalue
  end

end
#-------------------------------------------------------------------------------------------------------------#
# class API_WebGUI_Test_TextField_FireEvent
#                                                                              
# Purpose: Execute WebGUI test item of Text Field
#------------------------------------------------------------------------------------------------------------#
class API_WebGUI_Test_TextField_FireEvent
  def initialize(what,how)
    @what=what
    @how=how
    @hTextField=API_WebGUI_TextField_FireEvent.new(@what,@how)
  end

  def check_dialog(extra_file,&block)
        #check_dialog(extra_file, &block)
        Thread.new { system("rubyw #{extra_file}.rb") }
	#Thread.new {
	#		WindowHelper.new.push_confirm_button_ok()
	#	}
        block.call
    end
    
    def Do_test
	begin
		@hTextField.textField_enabled()
	rescue false
		Show_Outcomes.new(" Textfield #{@what} is disabled.").show_results()
		next
	end
		
	begin
		#a = Thread.new {
		 check_dialog('jscriptExtraConfirmOk'){ 
			 
			@hTextField.textField_setFieldValue()
			@hTextField.textField_FireEvent()
			}
		#}
		#b = Thread.new { 
			#system("rubyw jscriptExtraConfirmOk.rb")
		#	WindowHelper.new.push_confirm_button_ok()
		#}
	        #a.join
	        #b.join
		
	rescue Watir::Exception::ObjectDisabledException
		next
		Show_Outcomes.new(" Textfield #{@what} can't set.").show_results()
	end
	
	Dump_PopUp_Window_Text.new("popup.txt").show_results
	
  end

end
#-------------------------------------------------------------------------------------------------------------#
# class API_WebGUI_Test_Button_FireEvent
#                                                                              
# Purpose: Execute WebGUI test item of Button
#------------------------------------------------------------------------------------------------------------#
class API_WebGUI_Test_Button_FireEvent
  def initialize(what)
    @what=what
    @hButton= API_WebGUI_Button.new(@what)
  end

  def check_dialog(extra_file,&block)
        Thread.new { system("rubyw #{extra_file}.rb") }
        block.call
    end
    
  def Do_test
	 begin
		@hButton.button_existence()
	rescue false
		next
	end
	Show_Outcomes.new(" Button #{@what} is exist.").show_results()
		
	begin
		check_dialog('jscriptExtraConfirmOk'){ 
			@hButton.button_click()
			}
	rescue Watir::Exception::ObjectDisabledException
		Show_Outcomes.new(" Button #{@what} can't click.").show_results()
		#next
	end
	
	Dump_PopUp_Window_Text.new("popup.txt").show_results
  end
  
end


#-------------------------------------------------------------------------------------------------------------#
# class Dump_PopUp_Window_TextField
#                                                                              
# Purpose: Get log file that get from popup window static2 text
#------------------------------------------------------------------------------------------------------------#
class Dump_PopUp_Window_Text
  def initialize(str_srcfile)
	@line=String.new
	@str_srcfile=str_srcfile
	@hFile=File.new(@str_srcfile,'r')
  end
	
  def show_results
	 @hFile.each_line {|@line| 
			if !(@line.empty?) then
			Show_Outcomes.new(" PopWindow text=#{@line.dump}.").show_results()
			end
		}
  end		
end
#-------------------------------------------------------------------------------------------------------------#
# class Dump_DUT_SysInfo
#                                                                              
# Purpose: Dump SysInfo.htm to httpinfo.txt
#------------------------------------------------------------------------------------------------------------#
class Dump_DUT_SysInfo
  def initialize(host, target,method = 'GET',username = 'admin', passwd = 'admin')
	@host=host
	@method=method
	@target=target
	@username=username
	@passwd=passwd
	@syscmd=String.new
  end
  def dump_sysinfo_file()
    @syscmd =	"httpauthpntEX.exe "
    @syscmd <<	@host
    @syscmd <<	' '
    @syscmd <<	@method
    @syscmd <<	' '
    @syscmd <<	@target
    @syscmd <<	' '
    @syscmd <<	'"'
    @syscmd <<	'"'
    @syscmd <<	' '
    @syscmd <<	@username
    @syscmd <<	' '
    @syscmd <<	@passwd
    begin
	 system(@syscmd)
    rescue
	return    
    end
    
    return
    #puts "#{@syscmd}"
  end
  
  def dump_sysinfo_Telco()
    @syscmd =	"httpauthpntEX.exe "
    @syscmd <<	@host
    @syscmd <<	' '
    @syscmd <<	@method
    @syscmd <<	' '
    @syscmd <<	@target+' '+"sysinfo"
    @syscmd <<	' '
    @syscmd <<	@username+' '+@passwd
    
    begin
	 system(@syscmd)
    rescue
      return    
    end
  end
  
  def teardown
    @host=nil
    @method=nil
    @target=nil
    @username=nil
    @passwd=nil
    @syscmd=nil
  end

end
#-------------------------------------------------------------------------------------------------------------#
# class Get_Text_DUTSysInfo
#                                                                              
# Purpose: Return spcific string to act as dirctory that stores results
#------------------------------------------------------------------------------------------------------------#
class Get_Text_DUTSysInfo
  def initialize(basename,srcfile)
	@line=String.new
	@basename=basename
	@srcfile=srcfile
	@httpinfo_file=File.join(@basename,@srcfile)
	@hFile=File.new(@httpinfo_file,'r')
	@str_mode=String.new
	@str_firmware=String.new
	@str_country=String.new
	@str_return=String.new
  end

  def filter_results
	return @line[(@line.index(':')-@line.length+1) .. -2] 
  end
  
  def get_results
	 @hFile.each_line {|@line| 
			if !(@line.empty?) then
				case @line
					when /^(m|M)ode*/
					@str_mode=filter_results()
					when /^(f|F)irm*/
					@str_firmware=filter_results()
					when /^(c|C)ountry*/
					@str_country=filter_results()
				end
			end
		}
	@str_return=@str_mode+'-'+@str_country+@str_firmware
	return @str_return[0..(@str_return.index(' ')-@str_return.length-1)]
  end	
  
end
#-------------------------------------------------------------------------------------------------------------#
# class MkDir_webpage
#                                                                              
# Purpose: Makek dirctory to stores results
#------------------------------------------------------------------------------------------------------------#
class MkDir_webpage
  def initialize(what)
	@what=what
  end
  def chk_existence
	begin
		Dir.entries(@what) 
	rescue SystemCallError
		Dir.mkdir(@what) 
		Show_Outcomes.new(" Make directory #{@what}.").show_results()	
	end
	
  end
end
#-------------------------------------------------------------------------------------------------------------#
# class Get_Webpage_href
#                                                                              
# Purpose: Return array that stores every href of each page from href.txt
#------------------------------------------------------------------------------------------------------------#
class Get_Webpage_href
  def initialize(pathname,srcfile)
	@line=String.new
	@pathname=pathname
	@srcfile=srcfile
	@href_file=File.join(@pathname,@srcfile)
	@hFile=File.new(@href_file,'r')
	@str_http=String.new
	@str_href="href="
	@arr_return=Array.new
  end

  def filter_results
	return @line[(0-@line.length+@str_href.length) .. -3] 
  end
  
  def get_results
	 @hFile.each_line {|@line| 
			if !(@line.empty?) then
				case @line
					when /(.asp|.htm),$/
					@str_http=filter_results()
					#puts "#{@str_http}+'\N'"
					@arr_return.push(@str_http)
          
          when /[a-zA-Z0-9](html)%2F*/
          @str_http=filter_results()
					#p "#{@str_http}+'\n' "
					@arr_return.push(@str_http)
          
				end
			end
		}
	return @arr_return
  end	
  
end
#-------------------------------------------------------------------------------------------------------------#
# class Dump_File_Basename
#                                                                              
# Purpose: Dump File with both Basename filename
#
# Sep92005 Add logger to file 
#------------------------------------------------------------------------------------------------------------#
class Dump_File_Basename
  def initialize(pathname,outfile,strContent,logfiledir=nil)
    @pathname=pathname
    @outfile=outfile
    @strContent=strContent
    @outfile_path=File.join(@pathname,@outfile)
    @dir_entries=Array.new
    @logfiledir=logfiledir
  end
  
  def chk_outfile_existence()
	  @dir_entries=Dir.entries(@pathname)
	  return @dir_entries.include?(@outfile) 
  end

  def Dump_outfile()
	if   !chk_outfile_existence() then
		begin
			@hFile=File.new(@outfile_path,'w')
			@hFile.write(@strContent)
		rescue SystemCallError
			@hFile.close
			File.delete(@outfile_path)
		end
		@hFile.close
    Show_Outcomes.new(" Dump page #{@outfile}.",@logfiledir).log_screen
    Show_Outcomes.new(" Dump page #{@outfile}.",@logfiledir).log_logfile
	end
  end
end
#-------------------------------------------------------------------------------------------------------------#
# class DoTest_Dir_File
#                                                                              
# Purpose: Dump File with both Basename filename
#------------------------------------------------------------------------------------------------------------#
class DoTest_Dir_File
  def initialize(arrSrc,arrDest)
    @arrSrc=arrSrc
    @arrDest=arrDest
    @arrRet=Array.new
#    @basename=basename,pathname,basename
#    @pathname=pathname
#    @ArrDiff=Array.new
#    @filefullpath=String.new
#    @strContent=String.new
  end
  
  def DoDifference
    return @arrRet=@arrDest-@arrSrc
  end
  
  def DoUnion
    return @arrRet=(@arrDest|@arrSrc)
  end

  def DoIntersection
    return @arrRet=(@arrDest&@arrSrc)  
  end

#def Dump_outfile
#    @ArrDiff=DoTest_Dir_File.new(@arrSrc,@arrDest,nil,nil).DoDifference
    
#    MkDir_webpage.new(@pathname).chk_existence
#    @filefullpath=File.join(@pathname, @basename)
#    puts "ARRDIFF=#{@ArrDiff}"
#    if !(@ArrDiff.empty?) then
#	    @ArrDiff.each { |@x|
#		@strContent+= @x+'\n'   
#	    }
#	    @hFile=File.new(@filefullpath,'w')
#	    @hFile.write(@strContent)
#	    @hFile.close
#	    return 1.to_i
#    else
#	    return 0.to_i
#    end
    
#  end
  
end
#-------------------------------------------------------------------------------------------------------------#
# class Get_Webpage_help
#                                                                              
# Purpose: Return array that stores text in each help page
#------------------------------------------------------------------------------------------------------------#
class Get_Webpage_help
  def initialize()
	@str_return=String.new
  end
  
  def get_results
	 @str_return=$ie.document().body.innerText.strip
	return @str_return
  end	

end


#-------------------------------------------------------------------------------------------------------------#
# class KeyBoard_Event
#                                                                              
# Purpose: Trigger Keyboard event effect
#------------------------------------------------------------------------------------------------------------#
class KeyBoard_Event
require 'Win32API'

  def initialize()
    @KEYEVENTF_KEYUP = 0x2
    @VK_PRIOR        = 0x21 #Page UP key
    @VK_NEXT        = 0x22  #Page DOWN key
    @VK_END           =0x23
    @VK_HOME          =0x24
    @VK_LEFT          =0x25
    @VK_UP            =0x26
    @VK_RIGHT         =0x27
    @VK_DOWN          =0x28
    @SW_HIDE         = 0
    @SW_SHOW         = 5
    @SW_SHOWNORMAL   = 1
    @VK_CONTROL      = 0x11
    @VK_F4           = 0x73
    @VK_MENU         = 0x12
    @VK_RETURN       = 0x0D
    @VK_SHIFT        = 0x10
    @VK_SNAPSHOT     = 0x2C
    @VK_TAB         = 0x09  
    @VK_CLEAR        =  0x0C 
    @keybd_event = Win32API.new("user32", "keybd_event", ['I','I','L','L'], 'V')
    @vkKeyScan = Win32API.new("user32", "VkKeyScan", ['I'], 'I')
  end
  
  def Page_Down()
  # Page DOWN key
    @keybd_event.Call(@VK_NEXT, 1, 0, 0)
    @keybd_event.Call(@VK_NEXT, 1, @KEYEVENTF_KEYUP, 0)
    sleep(1)
  end
  
  def END_Key
    @keybd_event.Call(@VK_END, 1, 0, 0)
    @keybd_event.Call(@VK_END, 1, @KEYEVENTF_KEYUP, 0)
    sleep(0.5)  
  end
  
  def HOME_Key
    @keybd_event.Call(@VK_HOME, 1, 0, 0)
    @keybd_event.Call(@VK_HOME, 1, @KEYEVENTF_KEYUP, 0)
    sleep(0.5)
  end
  
  def UP_Key
    @keybd_event.Call(@VK_UP, 1, 0, 0)
    @keybd_event.Call(@VK_UP, 1, @KEYEVENTF_KEYUP, 0)
    sleep(0.5)
  end
  
  def DOWN_Key
    @keybd_event.Call(@VK_DOWN, 1, 0, 0)
    @keybd_event.Call(@VK_DOWN, 1, @KEYEVENTF_KEYUP, 0)
    sleep(0.5)  
  end
  
  def Blank_All
  # Shift + HOME
    @keybd_event.Call(@VK_SHIFT, 1, 0, 0)
    @keybd_event.Call(@VK_HOME, 1, 0, 0)
    @keybd_event.Call(@VK_HOME, 1, @KEYEVENTF_KEYUP, 0)
    @keybd_event.Call(@VK_SHIFT, 1, @KEYEVENTF_KEYUP, 0)
    sleep(0.5)  
  end
  
  def Paste()
  # Ctrl + V  : Paste
    @keybd_event.Call(@VK_CONTROL, 1, 0, 0)
    @keybd_event.Call(@vkKeyScan.Call(?V), 1, 0, 0)
    @keybd_event.Call(@vkKeyScan.Call(?V), 1, @KEYEVENTF_KEYUP, 0)
    @keybd_event.Call(@VK_CONTROL, 1, @KEYEVENTF_KEYUP, 0)
  end

  def Save_As()
  # Alt F + A : Save As
    @keybd_event.Call(@VK_MENU, 1, 0, 0)
    @keybd_event.Call(@vkKeyScan.Call(?F), 1, 0, 0)
    @keybd_event.Call(@vkKeyScan.Call(?F), 1, @KEYEVENTF_KEYUP, 0)
    @keybd_event.Call(@VK_MENU, 1, @KEYEVENTF_KEYUP, 0)
    @keybd_event.Call(@vkKeyScan.Call(?A), 1, 0, 0)
    @keybd_event.Call(@vkKeyScan.Call(?A), 1, @KEYEVENTF_KEYUP, 0)
    sleep(0.5)
  end

  def Tab_Key()
    @keybd_event.Call(@VK_TAB, 1, 0, 0)
    @keybd_event.Call(@VK_TAB, 1, @KEYEVENTF_KEYUP, 0)
    sleep(0.5)
  end
  
  def Enter_Key()
    @keybd_event.Call(@VK_RETURN, 1, 0, 0)
    @keybd_event.Call(@VK_RETURN, 1, @KEYEVENTF_KEYUP, 0)
    sleep(1)
  end
  
  def Exit()
    @keybd_event.Call(@VK_MENU, 1, 0, 0)
    @keybd_event.Call(@VK_F4, 1, 0, 0)
    @keybd_event.Call(@VK_F4, 1, @KEYEVENTF_KEYUP, 0)
    @keybd_event.Call(@VK_MENU, 1,@KEYEVENTF_KEYUP, 0)
    sleep(1) 
  end

  def teardown()
    @keybd_event = nil
    @vkKeyScan = nil
  end
  
end

class Screen_Capture
require 'Win32API'

  KEYEVENTF_KEYUP = 0x2
  SW_HIDE         = 0
  SW_SHOW         = 5
  SW_SHOWNORMAL   = 1
  VK_CONTROL      = 0x11
  VK_F4           = 0x73
  VK_MENU         = 0x12
  VK_RETURN       = 0x0D
  VK_SHIFT        = 0x10
  VK_SNAPSHOT     = 0x2C
  VK_TAB      = 0x09
  GMEM_MOVEABLE = 0x0002
  CF_TEXT = 1

  def initialize(dirname,filename, active_window_only=false, save_as_bmp=false)
    @dirname=dirname
    @filename=filename
    @active_window_only=active_window_only
    @save_as_bmp=save_as_bmp
  end

  # this method saves the current window or whole screen as either a bitmap or a jpeg
  # It uses paint to save the file, so will barf if a duplicate filename is selected, or  the path doesnt exist etc
  #    * dirname         - string  -  the name of the directory to save.
  #    * filename        - string  -  the name of the file to save. If its not fully qualified the current directory is used
  #    * active_window   - boolean - if true, the whole screen is captured, if false,  just the active window is captured
  #    * save_as_bmp     - boolean - if true saves the file as a bitmap, saves it as a jpeg otherwise
  def capture()

    keybd_event = Win32API.new("user32", "keybd_event", ['I','I','L','L'], 'V')
    vkKeyScan = Win32API.new("user32", "VkKeyScan", ['I'], 'I')
    winExec = Win32API.new("kernel32", "WinExec", ['P','L'], 'L')
    openClipboard = Win32API.new('user32', 'OpenClipboard', ['L'], 'I')
    setClipboardData = Win32API.new('user32', 'SetClipboardData', ['I', 'I'], 'I')
    closeClipboard = Win32API.new('user32', 'CloseClipboard', [], 'I')
    globalAlloc = Win32API.new('kernel32', 'GlobalAlloc', ['I', 'I'], 'I')
    globalLock = Win32API.new('kernel32', 'GlobalLock', ['I'], 'I')
    globalUnlock = Win32API.new('kernel32', 'GlobalUnlock', ['I'], 'I')
    memcpy = Win32API.new('msvcrt', 'memcpy', ['I', 'P', 'I'], 'I')

    @dirname=@dirname.tr('/','\\')
    @filename=@filename.tr('/','\\')
    @filename = Dir.getwd.tr('/','\\')+'\\'+@dirname+'\\'+@filename

    if @active_window_only ==false
          keybd_event.Call(VK_SNAPSHOT,0,0,0)   # Print Screen
    else
          keybd_event.Call(VK_SNAPSHOT,1,0,0)   # Alt+Print Screen
    end 

    winExec.Call('mspaint.exe', SW_SHOW)
    sleep(1)
     
    # Ctrl + V  : Paste
    KeyBoard_Event.new.Paste()

    # Alt F + A : Save As
    KeyBoard_Event.new.Save_As()

    # copy filename to clipboard
    hmem = globalAlloc.Call(GMEM_MOVEABLE, @filename.length+1)
    mem = globalLock.Call(hmem)
    memcpy.Call(mem, @filename, @filename.length+1)
    globalUnlock.Call(hmem)
    openClipboard.Call(0)
    setClipboardData.Call(CF_TEXT, hmem) 
    closeClipboard.Call 
    sleep(1)
      
    # Ctrl + V  : Paste
    KeyBoard_Event.new.Paste()

    if @save_as_bmp == false
      # goto the combo box
      KeyBoard_Event.new.Tab_Key()

      # select the first entry with J
      keybd_event.Call(vkKeyScan.Call(?J), 1, 0, 0)
      keybd_event.Call(vkKeyScan.Call(?J), 1, KEYEVENTF_KEYUP, 0)
      sleep(0.5)
    end  

    # Enter key
    KeyBoard_Event.new.Enter_Key()
     
    # Alt + F4 : Exit
    KeyBoard_Event.new.Exit()

  end
    
end

#-------------------------------------------------------------------------------------------------------------#
# class Browser_IE_Screen_Capture
#                                                                              
# Purpose: Caputer IE screen
#------------------------------------------------------------------------------------------------------------#
class Browser_IE_Screen_Capture
  def initialize(retval,test_site,dirname=nil,filename=nil,retrytimes=nil)
    @test_site=test_site
    @dirname=dirname
    @filename=filename
    @retrytimes=retrytimes
    @RetVal=retval
  end
  
  def DUT_login()
    
    case @RetVal

      when "200"
        Show_Outcomes.new("Return Value =#{@RetVal}. Webpage login.").log_screen
        Show_Outcomes.new("test_site =#{@test_site}.").log_screen
        Browser_IE_Initial.new(nil).start_ie_with_logger

        retarr=@test_site.split('/')
        retarr.shift
        retarr.shift
        dut_ip=retarr.shift
        #Browser_IE_Initial.new(nil).start_ie_with_logger()
        hbrowser=Browser_IE_Initial.new(dut_ip).webpag_login()
        $ie.wait()
        
        Browser_IE_Navigate.new(@test_site).browser_navigate
        $ie.wait()

      when /40(1|2|3)/
        Show_Outcomes.new("Return Value =#{@RetVal}. Alter Window login.").log_screen
        
        Browser_IE_Initial.new(nil).start_ie_with_logger

        retarr=@test_site.split('/')
        retarr.shift
        retarr.shift
        dut_ip=retarr.shift
        hbrowser=Browser_IE_Initial.new(dut_ip)
        hbrowser.goto_webpag
        $ie.wait
        
        Browser_IE_Navigate.new(@test_site).browser_navigate
        $ie.wait
      else
        p 'Unknow Login methode.'

    end  
  end
  
  def capture()
    for i in 1..@retrytimes
      #@filename = @filename + i.to_s
      Screen_Capture.new(@dirname,@filename+ i.to_s, true, false).capture
      #Page donw 
      KeyBoard_Event.new.Page_Down()
      
      Show_Outcomes.new(" Caputer #{@filename+ i.to_s}.").log_screen
    end

  end
  
end
#-------------------------------------------------------------------------------------------------------------#
# class Dump_WebPage_Obj
#                                                                              
# Purpose: Dump all objs in webpag like 'show_all_objects'
#------------------------------------------------------------------------------------------------------------#

class Dump_WebPage_Obj
 
 def initialize(props)
    @props=props
    @s=String.new()
 end
 
 def dump_page_obj
     #puts "-----------Objects in  page -------------" 
	    @doc =  $ie.document
            @s = ""
	    
	    @doc.all.each do |n|
        
        @props.each do |prop|
          begin
            p = n.invoke(prop)
              if (prop == "maxLength") && 
                  (p.to_s == "2147483647") then
                p="invaild"
              end    
            @s =@s+ "#{prop}=#{p}".to_s+","
          rescue WIN32OLERuntimeError
          next   
                        # this object probably doesnt have this property
          end
          
        end
        
        @s=@s+"\n"
      end

  return @s
 end
 
end

class Navigator_Gemtek
  def WAN0_StaticPage
    #------------------------------
    # TAB*3          ----->wan_proto_sel selectlist
    # Home+pulldown*1----->static ip
    #------------------------------
    #@autoit.Send "{HOME}"    #@autoit.Send "{DOWN}"
    hKeyBoard=KeyBoard_Event.new
    hKeyBoard.Tab_Key
    hKeyBoard.Tab_Key
    hKeyBoard.Tab_Key
    
    hKeyBoard.HOME_Key
    hKeyBoard.DOWN_Key
    hKeyBoard.teardown
    
  end
  
  def WAN0_StaticIP(str_wanip)
    #------------------------------
    # TAB*3        ----->go thru Navigator_WAN0_StaticPage process
    # TAB          ----->key in 10
    # TAB          ----->key in 5
    # TAB          ----->key in 30
    # TAB          ----->key in 226
    #------------------------------
    hKeyBoard=KeyBoard_Event.new
    
    hKeyBoard.Tab_Key;
    hKeyBoard.Tab_Key;
    hKeyBoard.Tab_Key
    
    str_wanip.split('.').each{|x|
      #@autoit.Send "{TAB}"
      hKeyBoard.Tab_Key
      
      str_result=" Keyin:"+x
      Show_Outcomes.new(str_result).show_results()
      @autoit.Send x.to_s
    };#str_wanip.split('.').each
    
    hKeyBoard.teardown
  end

  def WAN0_StaticIP_Mask_GW(hash_ini)
    #------------------------------
    # TAB*3        ----->go thru Navigator_WAN0_StaticPage process
    # TAB          ----->key in 10
    # TAB          ----->key in 5
    # TAB          ----->key in 30
    # TAB          ----->key in 226
    #------------------------------
    hKeyBoard=KeyBoard_Event.new
    
    hKeyBoard.Tab_Key;
    hKeyBoard.Tab_Key;
    hKeyBoard.Tab_Key
    
    str_wanip=      hash_ini[:CPE_Host_WAN_IP]
    str_wanip_mask= hash_ini[:CPE_Host_WAN_IPMask]
    str_wan_gw=     hash_ini[:CPE_Host_WAN_GW]
    
    #----keyin wan_ip;wanip_mask;wan_gw
    hWindowHelper=WindowHelper.new
    hWindowHelper.Typing_Data(str_wanip)
    hWindowHelper.Typing_Data(str_wanip_mask)
    hWindowHelper.Typing_Data(str_wan_gw)
    
    hKeyBoard.teardown
  end
  
  def TR069(hash_ini)
    #--------Select enable radio
    API_WebGUI_Radio.new('is_enable','1').radio_click
        
    #--------Key "TAB" twice
    hKeyBoard=KeyBoard_Event.new
    hKeyBoard.Tab_Key
    hKeyBoard.Tab_Key
    hKeyBoard.teardown
    #--------Key "TAB" and data string
        
        
    acs_server="http://"+hash_ini[:ACS_IP]+":"+hash_ini[:ACS_Port]+hash_ini[:ACS_URI]
    user_name=hash_ini[:CPE_Username]
    user_pwd=hash_ini[:CPE_Passwd]
    user_confm_pwd=user_pwd
    cwmpc_connRegUser=hash_ini[:ConnReqUsername]
    cwmpc_connRegPW=hash_ini[:ConnReqPassword]
    cwmpc_connRegPW_confirm=cwmpc_connRegPW
    
    hWindowHelper=WindowHelper.new
    hWindowHelper.Keyin_String(acs_server)
    hWindowHelper.Keyin_String(user_name)
    hWindowHelper.Keyin_String(user_pwd)
    hWindowHelper.Keyin_String(user_confm_pwd)
    hWindowHelper.Keyin_String(cwmpc_connRegUser)
    hWindowHelper.Keyin_String(cwmpc_connRegPW)
    hWindowHelper.Keyin_String(cwmpc_connRegPW_confirm)
        
    API_WebGUI_Button.new('but_save').button_click
  end

end