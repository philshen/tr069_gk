#-------------------------------------------------------------------------------------------------------------#
# API source code for TR069 Test                                
# 
# Purpose: For TR069 test by using tr69_device_test_suite from LinkSys                   
# Oct31/05 written by Philip Shen                        
# Dec01/05 class TR69_Rpt_Generator to creat colorful,different rpt format
# Feb09/06 Modify case expression in Class RPCMethods_Config.Update_CfgFile 
#          Add method Read_TestEnv_ini in Class TestEnv_Config.
#          Modify metnod both Get_ParameterNames() and 
#            Test_GetValues_atonce() in Class TR69_Do_Test 
#------------------------------------------------------------------------------------------------------------#

#-------------------------------------------------------------------------------------------------------------#
# class Show_Outcomes
#                                                                              
# Purpose: Output execute results to screen
#
# Sep8/2005  Add logger library
#------------------------------------------------------------------------------------------------------------#
class Show_Outcomes
require 'logger'

  def initialize(str_out,logfile=nil)
    @str_out=str_out
    @str_time=Time.now
    @strtemp=String.new
    @log = Logger.new(STDERR)
    @logfile = logfile
    @log_file = Logger.new(@logfile)
  end
  
  def show_results()
    @strtemp<<@str_time.strftime(" %m/%d/%Y %H:%M:%S")
    @strtemp<<@str_out
    puts @strtemp
  end

  def log_screen()
    @log.level = Logger::INFO#DEBUG	,Default.
    @log.datetime_format = "%d%b%Y@%H:%M:%S"
    do_log_screen
  end
  
  def do_log_screen()
#  @log.debug('do_log1') { "debug" }
    @log.info { @str_out }
#  @log.warn('do_log3') { "warn" }
#  @log.error('do_log4') { "error" }
#  @log.fatal('do_log6') { "fatal" }
#  @log.unknown { @str_out }
  end

  def log_logfile()
    @log_file.level = Logger::INFO#DEBUG	,Default.
    @log_file.datetime_format = "%d%b%Y@%H:%M:%S"
    do_log_logfile
  end

  def do_log_logfile()
    @log_file.info { @str_out }
  end
  
end
#-------------------------------------------------------------------------------------------------------------#
# class TestEnv_Configure
#                                                                             
# Purpose: Configure task.properties under folder tr69_device_test_suite/bin
#          Add method Read_TestEnv_ini for input argument from inifile.
#------------------------------------------------------------------------------------------------------------#
class TestEnv_Config
  
  def Modify_CfgStr(what,how)
  pos_eq=what.index('=')
  strhead=what[0..pos_eq]
  return rtustr=strhead+how+"\n"
  end

  def Update_CfgFile(pathname,filename,logfilepath=nil,\
                      acsHost=nil,acsPort=nil,\
                      cpeUsername=nil,cpePassword=nil,\
                      cpeHost=nil,cpePort=nil,\
                      needConnReq=nil,\
                      connReqUsername=nil,connReqPassword=nil,\
                      uri=nil,connReqURI=nil)
                      
  fullpath_cfgfile=File.join(pathname,filename)
  arr_FileCnt=Array.new
  arr_tempFileCnt=Array.new
  hFile=File.new(fullpath_cfgfile,'r')
  arr_FileCnt=hFile
  strLogCnt=String.new
  #p"fullpath_cfgfile is #{fullpath_cfgfile}"
  
  arr_FileCnt.each{|x|
    if !(x.include? "#") then
     case x
      when /[a-z]*=/
        #p "#{x};#{x.length} "
        if (acsHost != nil) && (x.include? "acsHost") then
          rtustr=Modify_CfgStr(x,acsHost)
          strLogCnt+="ACSHost=#{acsHost}."+" "
        elsif (acsPort != nil) && (x.include? "acsPort") 
          rtustr=Modify_CfgStr(x,acsPort)
          strLogCnt+="ACSPort=#{acsPort}."+" "
        elsif (cpeUsername != nil) && (x.include? "cpeUsername") 
          rtustr=Modify_CfgStr(x,cpeUsername)
          strLogCnt+="CPEUsername=#{cpeUsername}."+" "
        elsif (cpePassword != nil) && (x.include? "cpePassword") 
          rtustr=Modify_CfgStr(x,cpePassword)
          strLogCnt+="CPEPassword=#{cpePassword}."+" "
        elsif (cpeHost != nil) && (x.include? "cpeHost") 
          rtustr=Modify_CfgStr(x,cpeHost)
          strLogCnt+="CPEHost=#{cpeHost}."+" "
        elsif (cpePort != nil) && (x.include? "cpePort") 
          rtustr=Modify_CfgStr(x,cpePort)
          strLogCnt+="CPEPort=#{cpePort}."+" "
        elsif (needConnReq != nil) && (x.include? "needConnRequest") 
          rtustr=Modify_CfgStr(x,needConnReq)
          strLogCnt+="NeedConnRequest=#{needConnReq}."+" "  
        elsif (connReqUsername != nil) && (x.include? "connectionReqUsername") 
          rtustr=Modify_CfgStr(x,connReqUsername)
          strLogCnt+="ConnectionReqUsername=#{connReqUsername}."+" "
        elsif (connReqPassword != nil) && (x.include? "connectionReqPassword") 
          rtustr=Modify_CfgStr(x,connReqPassword)
          strLogCnt+="ConnectionReqPassword=#{connReqPassword}."+" "
        
        elsif (uri != nil) && (x.include? "uri")
          rtustr=Modify_CfgStr(x,uri)
          strLogCnt+="uri=#{uri}."+" "
        elsif (connReqURI != nil) && (x.include? "connReqURI")
          rtustr=Modify_CfgStr(x,connReqURI)
          strLogCnt+="connReqURI=#{connReqURI}."+" "
          
        else
          rtustr=x  
        end
        #p "ModifyStr=#{rtustr}"
        arr_tempFileCnt.push(rtustr)
     end
    
    else
      arr_tempFileCnt.push(x)
    end
    
  }
  hFile.close
  
  #Dump mdoified content to target file  
  hFile=File.new(fullpath_cfgfile,'w')
  arr_tempFileCnt.each{|y|
    hFile.puts "#{y}"
    
    hFile.puts "\n" if !(y.include? "#")  
  }  
  hFile.flush
  hFile.close
  #Log updated testenv cfg args
  Show_Outcomes.new(strLogCnt).log_screen
  Show_Outcomes.new(strLogCnt,logfilepath).log_logfile
  end

  def Read_TestEnv_ini(what)
    require 'API_inifile'
  
    if FileTest.exist?(what) == false
      Show_Outcomes.new("Pls check #{what} if exists!").log_screen
      return 1
    end
  
    rtu_hash = {}
    oIni = IniFile.new(what)
  
    oIni.sections { |s|
      #print "[#{s}]\n"
      oIni[s].each { |key, value|
        #print "  #{key} = #{value}\n"
        case key
        when /^CPE_Username$/
          rtu_hash[:CPE_Username]=value
        when /^CPE_Passwd$/
          rtu_hash[:CPE_Passwd]=value
        when /^CPE_Host_LAN_IP$/
          rtu_hash[:CPE_Host_LAN_IP]=value
        when /^CPE_Host_WAN_IP$/
          rtu_hash[:CPE_Host_WAN_IP]=value
        when /^CPE_Host_WAN_IPMask$/
          rtu_hash[:CPE_Host_WAN_IPMask]=value
        when /^CPE_Host_WAN_GW$/
          rtu_hash[:CPE_Host_WAN_GW]=value
        when /^CPE_Port$/
          rtu_hash[:CPE_Port]=value
        when /^ConnReqURI$/
          rtu_hash[:ConnReqURI]=value
        when /^ACS_IP$/
          rtu_hash[:ACS_IP]=value
        when /^ACS_Port$/
          rtu_hash[:ACS_Port]=value
        when /^ACS_URI$/
          rtu_hash[:ACS_URI]=value  
        when /^NeedConnRequest$/
          rtu_hash[:NeedConnRequest]=value
        when /^ConnReqUsername$/
          rtu_hash[:ConnReqUsername]=value
        when /^ConnReqPassword$/
          rtu_hash[:ConnReqPassword]=value
        when /^JavaSDKVer$/
          rtu_hash[:JavaSDKVer]=value
        when /^CSV_SetValues$/
          rtu_hash[:CSV_SetValues]=value
        when /^SSH_Username$/
          rtu_hash[:SSH_Username]=value
        when /^SSH_Password$/
          rtu_hash[:SSH_Password]=value  
        end  
      }
    }

    if (rtu_hash.length != 18)
      Show_Outcomes.new("Pls check #{what} content! It should have 18 variables.").log_screen
      return 1
    else
      return rtu_hash
    end
    
  end
  
end

#-------------------------------------------------------------------------------------------------------------#
# class RPCMethods_Configure
#                                                                             
# Purpose: Configure task.csv under folder tr69_device_test_suite/bin
#------------------------------------------------------------------------------------------------------------#
class RPCMethods_Config
  def Modify_CfgStr(what,how)
    if (what.include? how) then
      rtustr=what.delete "#"
    else
      rtustr=what
    end
  return rtustr
  end

  def Append_RemarkStr(what,how)
    if !(what.include? how) then
      rtustr="#"+ what
    else
      rtustr=what
    end
  return rtustr
  end
  
  def Remark_AllLines(what,arr_filecnt=nil)
    arr_tempfilecnt=Array.new
    
    arr_filecnt.each{|x|
      case x
        when /^\#/
          rtustr=x  
        else
          rtustr=Append_RemarkStr(x,what) 
      end
      #p "In RPCMethods_Config:Remark_Alllines, rtustr=#{rtustr}"
      arr_tempfilecnt.push(rtustr)
    }
    
    return arr_tempfilecnt
  end
  
  def Enable_PRCMethods_Task(what,arr_filecnt=nil)
    arr_tempfilecnt=Array.new
    
    arr_filecnt.each{|x|
      case x
        when /^\#ConnectionRequest/
          rtustr=Modify_CfgStr(x,"#")
          Show_Outcomes.new("Remove berfore ConnectionRequest symbol '#'.").log_screen
        
        when /^\#InformRpcTask,InformRpcTask.input$/
          #p "#{x};#{x.length} "
          rtustr=Modify_CfgStr(x,"#")
          #Show_Outcomes.new("RPCMethod=#{rtustr.strip}.").log_screen
          Show_Outcomes.new("Remove before InformRpcTask symbol '#'.").log_screen
        
        when what
          
          if (x.include? '#')
            rtustr=Modify_CfgStr(x,"#") 
            #Show_Outcomes.new("what=#{what}.").log_screen
            
            Show_Outcomes.new("Remove before #{rtustr.strip} symbol '#'.").log_screen
          end
          
        else
          rtustr=x 
      end
      
      arr_tempfilecnt.push(rtustr)
    }
    
    return arr_tempfilecnt
  end
  
  #-------------------------------------------------------------------------------------------------------------#
  # Cause tr69_device_test_suite/bin v..4 task.csv should ...
  #                                                                             
  # ConnectionRequestTask
  # InformRpcTask,InformRpcTask.input
  # #{strPRCMethods},#{strPRCMethods}.input
  #------------------------------------------------------------------------------------------------------------#
  def Update_CfgFile(pathname,filename,what,how)
    fullpath_cfgfile=File.join(pathname,filename)
    arr_FileCnt=Array.new
    arr_tempFileCnt=Array.new
    arr_tempFileCnt_2nd=Array.new
  
    hFile=File.new(fullpath_cfgfile,'r')
    arr_FileCnt=hFile
    strPRCMethods=what+how+"RpcTask"
    #p"#{strPRCMethods}"
  
    #All line add remark if not remark
    arr_tempFileCnt=Remark_AllLines("#", arr_FileCnt)
  
  
    #First fileter
    arr_tempFileCnt_2nd=\
      Enable_PRCMethods_Task(/^\##{strPRCMethods},#{strPRCMethods}.input$/, arr_tempFileCnt)
  
    hFile.close
  
    #Dump mdoified content to target file  
    hFile=File.new(fullpath_cfgfile,'w')
    arr_tempFileCnt_2nd.each{|z|
      hFile.puts "#{z}"  
    }  
    hFile.flush
    hFile.close
  end
  
end
#-------------------------------------------------------------------------------------------------------------#
# class IDGParamters_Configure
#                                                                             
# Purpose: Configure xxxxRpcTask.input under 
#           folder tr69_device_test_suite/bin/inputs
#------------------------------------------------------------------------------------------------------------#
class IDGParamters_Config
  def Modify_CfgStr(what,how)
    if (what.include? how) then
      rtustr=what.delete "#"
    else
      rtustr=what
    end
  return rtustr
  end

  def Append_RemarkStr(what,how,cpeparalist)
    if !(what.include? how) then
      rtustr="#"+ what
    else
      rtustr=cpeparalist+",true"
      #rtustr=what
    end
  return rtustr
  end

  def chk_file_existence(pathname,filename)
	  dir_entries=Dir.entries(pathname)
	  return dir_entries.include?(filename) 
  end
  
  def dump_results(pathname,filename,cpeparalist)
    fullpathfile=File.join(pathname,filename)
    strContent=cpeparalist+",ture"
    begin
      hFile=File.new(fullpathfile,'w')
      hFile.write(strContent)
    rescue SystemCallError
			hFile.close
			File.delete(fullpathfile)
    end    
    hFile.close
    
    Show_Outcomes.new("Put #{strContent} in #{fullpathfile}.").log_screen 
  end
  
  def Update_CfgFile(pathname,what,how,strcpeParaList)
    strCPEParamListFilename=what+how+"RpcTask.input"
    fullpath_cfgfile=File.join(pathname,strCPEParamListFilename)
  
  #--------------------------------------------
  #Setp1:Chk file if exist in folder tr69_device_test_suite/bin/inputs
  #--------------------------------------------
    if chk_file_existence(pathname,strCPEParamListFilename) then
      #p "strCPEParamListFilename=#{strCPEParamListFilename}"
      #p "cpeParaList=#{strcpeParaList}"
  #--------------------------------------------
  #Setp2:Update file contetn 
  #--------------------------------------------
      arr_tempFileCnt=Array.new
      arr_tempFileCnt.push(strcpeParaList+",true")
      #strcpeParaList=strcpeParaList.to_s
      #strcpeParaList += ",true"    
      #arr_tempFileCnt.push(strcpeParaList)
      
      #Dump mdoified content to target file  
      hFile=File.new(fullpath_cfgfile,'w')
      arr_tempFileCnt.each{|x|
        hFile.puts "#{x}"
        strtemp="----------------------------------------------------------------------------------"
        Show_Outcomes.new(strtemp).log_screen    
        Show_Outcomes.new("Update #{x} in #{strCPEParamListFilename}.").log_screen 
        Show_Outcomes.new(strtemp).log_screen
      }  
      hFile.flush
      hFile.close
      
    else
      dump_results(pathname,strCPEParamListFilename,strcpeParaList)
    end
    
  end

  def GetValues_CfgFile(pathname,what,how,pathnameRORW)
    strCPEParamListFilename=what+how+"RpcTask.input"
    fullpath_cfgfile=File.join(pathname,strCPEParamListFilename)
  
  #--------------------------------------------
  #Setp1:Chk file if exist in folder tr69_device_test_suite/bin/inputs
  #--------------------------------------------
    if chk_file_existence(pathname,strCPEParamListFilename) then
  #--------------------------------------------
  #Setp2:Update file contetn 
  #--------------------------------------------
        #Get Src file incluing RORW ParamLists 
    end
    
  end

end
#-------------------------------------------------------------------------------------------------------------#
# class Get_CPE_ParamLists
#                                                                              
# Purpose: Return array from srceen dump results.
#------------------------------------------------------------------------------------------------------------#
class Get_CPE_ParamLists
  def filter(method,methodtarge,arr_ScreenDump)
    
    #--------------------------------------------------------------
    #Inside validateRpcResponse method.
    #Received Message is : GetParameterNamesResponse{ParameterInfoStruct[0]={Name=InternetGatewayDevice.DeviceInfo.;Writable=0}
    #ParameterInfoStruct[1]={Name=InternetGatewayDevice.DeviceConfig.;Writable=0}
    #                     .
    #                     .
    #                     .
    #ParameterInfoStruct[13]={Name=InternetGatewayDevice.;Writable=0}
    #}
    #2007-11-19T18:19:20.860 CST : com.cisco.bac.dts.GetParameterNamesRpcTask PASSED.
    #--------------------------------------------------------------
    token_start="Inside validateRpcResponse method.\n"
    token_end="CST : "+"com.cisco.bac.dts."+method+methodtarge+"RpcTask"
    
    #arr_ScreenDump.each{|x| p "#{x.chomp}"}
  
    pos_start=arr_ScreenDump.rindex(token_start)
    pos_end  =nil
    
    arr_ScreenDump.each{|x|
      
      case x
        when /#{token_end}/
          pos_end  =arr_ScreenDump.rindex(x)
          #p "x= #{x.chomp}"
      end
    }
    
    #p "token_end=#{token_end}"    
    #p "pos_start=#{pos_start}; pos_end=#{pos_end}"
    
    if (pos_start!=nil) && (pos_end!=nil) then
      return arr_ScreenDump[(pos_start+1) .. (pos_end-1-1)] 
    else
      return false
    end
  end

  def filter_next(arr_XMLMessage)
  #--------------------------------------------------------------
  #Message format--->
  #ParameterInfoStruct[3]={Name=
  #InternetGatewayDevice.WANDevice.1.WANEthernetInterfaceConfig.
  #;Writable=0}
  #--------------------------------------------------------------
  token_start ="]={Name="
  token_end   ="}\n"
  arr_result=[]
  
  arr_XMLMessage.each{|x|
    case x
    
    when /(^Para|[A-Z]*\{Para)/
      pos_start=x.index(token_start)
      pos_end  =x.index(token_end)
      #p "#{x}"
       
      strtemp = x[pos_start+token_start.length .. (pos_end-1)]
      arr_result.push(strtemp)
    end
  }
  
  return arr_result
  end

  def get_name(arr_ParamLists)
  #token_end   =".;Writable"
  token_end   =";Writable"
  arr_result=[]
  
  arr_ParamLists.each{|x|
    case x
    when /\;Writable/
      pos_end  =x.index(token_end)
      #p "#{x}"
       
      strtemp = x[0 .. pos_end]
      arr_result.push(strtemp)
    end
  }
  
  return arr_result
  end

  def get_ROParam_name(arr_ParamLists)
  token_end   =";Writable=0"
  arr_result=[]
  #p "arr_ParamLists type=#{arr_ParamLists.class}"
  
  arr_ParamLists.each{|x|
    case x
    when /;Writable=0/
      pos_end  =x.index(token_end)-1
       
      strtemp = x[0 .. pos_end]
      arr_result.push(strtemp)
    end
  }
  
  return arr_result
  end

  def get_RWParam_name(arr_ParamLists)
  token_end   =";Writable=1"
  arr_result=[]
  
  arr_ParamLists.each{|x|
    case x
    when /;Writable=1/
      pos_end  =x.index(token_end)-1
       
      strtemp = x[0 .. pos_end]
      arr_result.push(strtemp)
    end
  }
  
  return arr_result
  end
  
  def get_XML_RPCResponse(arr_ScreenDump)
    #--------------------------------------------------------
    #Content-Type: text/xml
    #
    #<SOAP-ENV:Envelope xmlns:SOAP-ENV=.....
    #                   .
    #                   .
    #                   .
    #</SOAP-ENV:Envelope>
    #
    #Inside validateRpcResponse method.
    #--------------------------------------------------------
    token_start="Content-Type: text/xml\n"
    token_end="Inside validateRpcResponse method.\n"
  
    pos_start=arr_ScreenDump.rindex(token_start)
    pos_end=arr_ScreenDump.rindex(token_end)
  
    if (pos_start!=nil) && (pos_end!=nil) then
      return arr_ScreenDump[(pos_start+1+1) .. (pos_end-1-1)] 
    else
      return false
    end
    
  end
  #---------------------------------------------------------------------------
  # To parse SetParameterValues RPC to check setvaule status code and getvalue result
  #---------------------------------------------------------------------------  
  def get_XML_RPCResponse_Index(arr_ScreenDump)
    #--------------------------------------------------------
    #Content-Type: text/xml
    #
    #<SOAP-ENV:Envelope xmlns:SOAP-ENV=.....
    #                   .
    #                   .
    #                   .
    #</SOAP-ENV:Envelope>
    #
    #Inside validateRpcResponse method.
    #--------------------------------------------------------
    token_start="Content-Type: text/xml\n"
    token_end="Inside validateRpcResponse method.\n"
    arr_pos_start_numidx=[]
    arr_pos_end_numidx=[]
    arr_pos_start_end_numidx=[]
    idx_num=0
    
    arr_ScreenDump.each{|x|
      case x
        when /^#{token_start}$/
          arr_pos_start_numidx.push(idx_num)
          arr_pos_start_end_numidx.push(idx_num)
        when /^#{token_end}$/
          arr_pos_end_numidx.push(idx_num)
          arr_pos_start_end_numidx.push(idx_num)          
      end
      idx_num += 1
    }
    
    #p arr_pos_start_numidx
    #p arr_pos_end_numidx
    #p arr_pos_start_end_numidx
    arr_pos_start_end_numidx = arr_pos_start_end_numidx[-4..-1]
    #p "arr_pos_start_end_numidx= #{arr_pos_start_end_numidx}"
    #p arr_pos_start_end_numidx
    
    return arr_pos_start_end_numidx  
  end

end
#-------------------------------------------------------------------------------------------------------------#
# class Dump_File
#                                                                              
# Purpose: Output contents to file
#------------------------------------------------------------------------------------------------------------#
class Dump_File
  def dump_results(fullpathfile,arr_tempFileCnt,mode='a')
    arr_temp=[]
    arr_temp.push(Time.now.strftime(" %m/%d/%Y %H:%M:%S"))
    
    begin
      hFile=File.new(fullpathfile,mode)
      arr_tempFileCnt.each{|x|

        arr_temp.push(x);#arr_temp.push("\n")
        #hFile.puts "#{x}"
        #hFile.puts "\n"    
      }
      hFile.puts arr_temp  
      hFile.flush
    rescue SystemCallError
			hFile.close
			File.delete(fullpathfile)
    end    
    hFile.close
    Show_Outcomes.new("Log Results in #{fullpathfile}.").log_screen 
    
  end
  
  def dump_results_csv(fullpathfile,str_tempFileCnt,mode='a')
    begin
      hFile=File.new(fullpathfile,mode)
      
      str_temp=str_tempFileCnt.to_s+','+Time.now.strftime("%m/%d/%Y %H:%M:%S").to_s
      
      hFile.puts str_temp  
      hFile.flush
    rescue SystemCallError
			hFile.close
			File.delete(fullpathfile)
    end    
    hFile.close
    Show_Outcomes.new("Log Results in #{fullpathfile}.").log_screen 
    
  end
end
#-------------------------------------------------------------------------------------------------------------#
# class Get_CPE_ParamValues
#                                                                              
# Purpose: Return array from srceen dump results.
#          Should return the index of the last obj in array 
#------------------------------------------------------------------------------------------------------------#
class Get_CPE_ParamValues
  def filter(arr_ScreenDump)
    #----- same as Get_CPE_ParamLists.new.filter
    retval=Get_CPE_ParamLists.new.filter('Get','ParameterValues',arr_ScreenDump)
    
    return retval
    
  end
  
  def filter_type_value(arr_XMLMessage)
  #--------------------------------------------------------------
  #Message format--->
  #Received Message is : GetParameterValuesResponse{
  #ParameterValueStruct[0]={Name=InternetGatewayDevice.WANDevice.1.\
  #WANConnectionNumberOfEntries;Value={type=unsignedInt;value=1}}
  #}
  #--------------------------------------------------------------
  #--------------------------------------------------------------
  #Message format--->
  #Received Fault RPC response. The details are : \
  #Fault{faultcode=SOAP-ENV:Client;faultstring=CWMP fault;\
  #cwmp:Fault={FaultCode=9007;FaultString=Invalid parameter value;}
  #;}
  #--------------------------------------------------------------  
  token_start =String.new
  token_mid   =String.new
  token_end   =String.new

  arr_result=Array.new
  
  arr_XMLMessage.each{|x|
    case x
    
    when /;Value=/
      token_start ="Value={type="
      token_mid   =";value="
      token_end   ="}}\n"

    when /:Fault=/
      token_start ="Fault={FaultCode="
      token_mid   =";FaultString="
      token_end   =";}\n"
    
    end
    pos_start=x.index(token_start)
    pos_mid  =x.index(token_mid)
    pos_end  =x.index(token_end)

    strtype = x[pos_start.to_i+token_start.length .. (pos_mid.to_i - 1)]
    strvalue= x[pos_mid.to_i+token_mid.length .. (pos_end.to_i - 1)]
    #p "Type=#{strtype};Value=#{strvalue}"
    arr_result.push(strtype)
    arr_result.push(strvalue)
    
  }
  
  return arr_result
  end  

  def filter_type(arr_XMLMessage)
  #--------------------------------------------------------------
  #Message format--->
  #Received Message is : GetParameterValuesResponse{
  #ParameterValueStruct[0]={Name=InternetGatewayDevice.WANDevice.1.\
  #WANConnectionNumberOfEntries;Value={type=unsignedInt;value=1}}
  #}
  #--------------------------------------------------------------
  #--------------------------------------------------------------
  #Message format--->
  #Received Fault RPC response. The details are : \
  #Fault{faultcode=SOAP-ENV:Client;faultstring=CWMP fault;\
  #cwmp:Fault={FaultCode=9007;FaultString=Invalid parameter value;}
  #;}
  #--------------------------------------------------------------  
  token_start =String.new
  token_mid   =String.new
#  token_end   =String.new

  arr_result=Array.new
  
  arr_XMLMessage.each{|x|
    case x
    
    when /;Value=/
      token_start ="Value={type="
      token_mid   =";value="
#      token_end   ="}}\n"

    when /:Fault=/
      token_start ="Fault={FaultCode="
      token_mid   =";FaultString="
#      token_end   =";}\n"
    
    end
    pos_start=x.index(token_start)
    pos_mid  =x.index(token_mid)
#    pos_end  =x.index(token_end)

    strtype = x[pos_start.to_i+token_start.length .. (pos_mid.to_i - 1)]
#    strvalue= x[pos_mid.to_i+token_mid.length .. (pos_end.to_i - 1)]
    #p "Type=#{strtype};Value=#{strvalue}"
    arr_result.push(strtype)
  }
  
  return arr_result
  end  
  
  def filter_param_type_value(arr_XMLMessage)
  #--------------------------------------------------------------
  #Message format--->
  #Received Message is : GetParameterValuesResponse{
  #ParameterValueStruct[0]={Name=InternetGatewayDevice.WANDevice.1.\
  #WANConnectionNumberOfEntries;Value={type=unsignedInt;value=1}}
  #}
  #--------------------------------------------------------------
  #--------------------------------------------------------------
  #Message format--->
  #Received Fault RPC response. The details are : \
  #Fault{faultcode=SOAP-ENV:Client;faultstring=CWMP fault;\
  #cwmp:Fault={FaultCode=9007;FaultString=Invalid parameter value;}
  #;}
  #--------------------------------------------------------------  
  token_param =String.new; token_type =String.new
  token_value =String.new; token_end  =String.new

  arr_result=Array.new
  
  arr_XMLMessage.each{|x|
    case x
    
    when /;Value=/
      token_param ="]={Name="
      token_type  =";Value={type="
      token_value =";value="
      token_end   ="}}\n"

    when /:Fault=/
      token_param ="cwmp:"
      token_type  ="Fault={FaultCode="
      token_value =";FaultString="
      token_end   =";}\n"
    
    end
    pos_param=x.index(token_param)
    pos_type =x.index(token_type)
    pos_value=x.index(token_value)
    pos_end  =x.index(token_end)
    
    strparam= x[pos_param.to_i+token_param.length .. (pos_type.to_i - 1)]
    strtype = x[pos_type.to_i+token_type.length .. (pos_value.to_i - 1)]
    strvalue= x[pos_value.to_i+token_value.length .. (pos_end.to_i - 1)]
    #p "Type=#{strtype};Value=#{strvalue}"
    arr_result.push(strparam)
    arr_result.push(strtype)
    arr_result.push(strvalue)
  }
  
  return arr_result
  end

  def filter_SetParamValue(arr_ScreenDump)
    #------------------------------------------------------------------------------------------------
    #Inside validateRpcResponse method.
    #Received Message is : GetParameterValuesResponse{ParameterValueStruct[0]={Name=InternetGatewayDevice.WANDevice.1.WANConnectionDevice.1.WANIPConnection.1.Enable;Value={type=boolean;value=1}}
    #}
    #2007-11-28T16:25:06.946 CST : com.cisco.bac.dts.GetParameterValuesRpcTask PASSED.
    #2007-11-28T16:25:06.946 CST : com.cisco.bac.dts.SetParameterValuesRpcTask PASSED.
    #------------------------------------------------------------------------------------------------
    #------------------------------------------------------------------------------------------------
    #Inside validateRpcResponse method.
    #Received Fault RPC response. The details are : Fault{faultcode=Client;faultstring=CWMP fault;cwmp:Fault={FaultCode=9005;FaultString=Invalid Parameter Name;};}
    #2007-11-28T16:31:21.441 CST : com.cisco.bac.dts.GetParameterValuesRpcTask FAILED with Exception Error : The Message is not GetParameterValuesResponseMessage. It is : Fault
    #------------------------------------------------------------------------------------------------
    token_start="Inside validateRpcResponse method.\n"
    token_mid="CST : "+"com.cisco.bac.dts."+"Get"+"ParameterValues"+"RpcTask "+"PASSED" 
    token_end="CST : "+"com.cisco.bac.dts."+"Set"+"ParameterValues"+"RpcTask "+"PASSED" 
    
    #arr_ScreenDump.each{|x| p "#{x.chomp}"}
  
    pos_start=arr_ScreenDump.rindex(token_start)
    pos_mid  =nil;  pos_end  =nil
    
    arr_ScreenDump.each{|x|
      
      case x
        when /#{token_mid}/
          pos_mid  =arr_ScreenDump.rindex(x)
        when /#{token_end}/
          pos_end  =arr_ScreenDump.rindex(x)
          #p "x= #{x.chomp}"
      end
    }
    
    #p "token_end=#{token_end}"    
    #p "pos_start=#{pos_start}; pos_end=#{pos_end}"
    
    if (pos_mid!=nil) && (pos_end!=nil) then
      return true
    else
      return false
    end
  end
  
  def filter_SetParamValue_NameValue(arr_SetParamValues_XML,what)
    token_name_start="<Name>"
    token_name_end= "</Name>"
    token_value_start="<Value xsi:type=\"xsd:"
    token_value_mid=  "\">"
    token_value_end=  "</Value>"
    
    token_faultcode_start="<FaultCode>"
    token_faultcode_end="</FaultCode>"
    token_faultstring_start="<FaultString>"
    token_faultstring_end="</FaultString>"
    
    token_status_start="<Status>"
    token_status_end= "</Status>"
    
    #pos_token_name_start=nil; pos_token_name_end=nil
    #pos_token_value_start=nil; pos_token_value_mid=nil; pos_token_value_end=nil
    parameterValueStruct_Name=String.new;
    parameterValueStruct_Value, parameterValueStruct_Type, parameterValueStruct_Status=String.new
    faultcode, faultstring=String.new
    
    arr_SetParamValues_XML.each{|x|
      case x
        when /#{token_name_start}/
          pos_token_name_start=x.to_s.index(token_name_start)
          pos_token_name_end=x.to_s.index(token_name_end)
    
          parameterValueStruct_Name=x[\
                (pos_token_name_start+token_name_start.length) .. (pos_token_name_end-1)]
        
        when /#{token_value_start}/
          pos_token_value_start=x.to_s.index(token_value_start) 
          pos_token_value_mid=  x.to_s.index(token_value_mid) 
          pos_token_value_end=  x.to_s.index(token_value_end)

          parameterValueStruct_Type=x[\
                pos_token_value_start+token_value_start.length .. (pos_token_value_mid-1)]
          parameterValueStruct_Value=x[\
                pos_token_value_mid+token_value_mid.length .. (pos_token_value_end-1)]
        
        when /#{token_status_start}/
          pos_token_status_start=x.to_s.index(token_status_start)
          pos_token_status_end=x.to_s.index(token_status_end)
          
          parameterValueStruct_Status=x[\
                (pos_token_status_start+token_status_start.length) .. (pos_token_status_end-1)]
                
        when /#{token_faultcode_start}/
          pos_token_faultcode_start=x.to_s.index(token_faultcode_start)
          pos_token_faultcode_end=  x.to_s.index(token_faultcode_end)
          
          faultcode=x[\
                (pos_token_faultcode_start+token_faultcode_start.length) .. (pos_token_faultcode_end-1)]
        
        when /#{token_faultstring_start}/
          pos_token_faultstring_start=x.to_s.index(token_faultstring_start)
          pos_token_faultstring_end=  x.to_s.index(token_faultstring_end)
          
          faultstring=x[\
                (pos_token_faultstring_start+token_faultstring_start.length) .. (pos_token_faultstring_end-1)]
      end
    }
    
    #p "pos_token_name_start= #{pos_token_name_start}; pos_token_name_end= #{pos_token_name_end}"
    #p "pos_token_value_start= #{pos_token_value_start}; pos_token_value_mid= #{pos_token_value_mid}"
    #p "parameterValueStruct_Name= #{parameterValueStruct_Name}"
    #p "parameterValueStruct_Value= #{parameterValueStruct_Value}"
    #p "parameterValueStruct_Type=  #{parameterValueStruct_Type}"
    #p "parameterValueStruct_Status= #{parameterValueStruct_Status}"
    
    if what.to_s == 'true'
      ret_str=parameterValueStruct_Name+','+\
              parameterValueStruct_Type+','+parameterValueStruct_Value+','+\
              'SetParameterValuesResponse='+parameterValueStruct_Status
    elsif what.to_s == 'false'
      ret_str=faultcode+','+faultstring
    end
    
    return  ret_str
    
  end
  
  
end
#-------------------------------------------------------------------------------------------------------------#
# class Array_Test
#                                                                              
# Purpose: Do Array elements computation
#------------------------------------------------------------------------------------------------------------#
class Array_Test
  def initialize(arrSrc,arrDest)
    @arrSrc=arrSrc
    @arrDest=arrDest
    @arrRet=Array.new
  end
  
  def DoDifference
    return @arrRet=@arrDest-@arrSrc
  end
  
  def DoUnion
    return @arrRet=(@arrDest|@arrSrc)
  end

  def DoIntersection
    return @arrRet=(@arrDest&@arrSrc)  
  end
  
end
#-------------------------------------------------------------------------------------------------------------#
# class Check_DUT_httpd_alive
#                                                                              
# Purpose: Check DUT http server if alive
#------------------------------------------------------------------------------------------------------------#
class Check_DUT_httpd_alive
require 'ping'
require 'net/http'

  def initialize(host,logfiledir=nil,account=nil,password=nil)
    @host=host
    @logfiledir=logfiledir
    @account=account
    @password=password
  end
  
  def check_alive
  
    #----Nov19/07 Add by philip----#
    unless /linux$/ =~ RUBY_PLATFORM then
      retvalue = true
    else
      retvalue=Ping.pingecho(@host)
      #puts "In class Check_DUT_httpd_alive:check_alive, @host=#{@host}; retvalue = #{retvalue}"
    end    
    retvalue = true
    
    if retvalue then #if true
      #h = Net::HTTP.new(@host, 80)
      #resp, data = h.get('/', nil )
      begin
        h = Net::HTTP.new(@host, 80)
        resp, data = h.get('/', nil )
  
      rescue Errno::ECONNREFUSED
        Show_Outcomes.new("HTTP Srv isn't alive.").log_screen
        Show_Outcomes.new("HTTP Srv isn't alive.",@logfiledir).log_logfile  
        return false
      rescue Timeout::Error
        Show_Outcomes.new("TCP session time out.").log_screen
        Show_Outcomes.new("TCP session time out.",@logfiledir).log_logfile  
        return false
      rescue Errno::EBADF
        Show_Outcomes.new("Pls check cable connected or DUT powered on?").log_screen
        Show_Outcomes.new("Pls check cable connected or DUT powered on?",@logfiledir).log_logfile
        return false
      end  
      # puts "resp = #{resp}; #{resp.class}"
      Show_Outcomes.new("DUT HTTP Srv return Code = #{resp.code}.").log_screen
      strtemp="DUT HTTP Srv return Code = #{resp.code}."+'  '+"Message = #{resp.message}"
      Show_Outcomes.new(strtemp,@logfiledir).log_logfile
      #resp.each {|key, val| printf "%-14s = %-40.40s\n", key, val }
      # puts "data = #{data}; #{data.class}"    #puts data[0..55]
    
    else
      Show_Outcomes.new("Host can't reachable.").log_screen
      Show_Outcomes.new("Host can't reachable.",@logfiledir).log_logfile
      
      return false
    end
    
    return  resp.code
    
  end	
  
  def check_basic_auth
    
    Net::HTTP.start(@host) {|http|
      req = Net::HTTP::Get.new('/')
      req.basic_auth @account, @password
      resp = http.request(req)
      Show_Outcomes.new("DUT HTTP Srv return Code = #{resp.code}.").log_screen
      return  resp.code
      #print resp.body
    }
  end
  
end
#-------------------------------------------------------------------------------------------------------------#
# class Get_Text_DUTSysInfo
#                                                                              
# Purpose: Return spcific string to act as dirctory that stores results
#------------------------------------------------------------------------------------------------------------#
class Get_Text_DUTSysInfo
  def initialize(basename,srcfile)
	@line=String.new
	@basename=basename
	@srcfile=srcfile
	@httpinfo_file=File.join(@basename,@srcfile)
	@hFile=File.new(@httpinfo_file,'r')
	@str_mode=String.new
	@str_firmware=String.new
	@str_country=String.new
	@str_return=String.new
  end

  def filter_results
	return @line[(@line.index(':')-@line.length+1) .. -2] 
  end
  
  def get_results
	 @hFile.each_line {|@line| 
			if !(@line.empty?) then
				case @line
					when /^(m|M)ode*/
					@str_mode=filter_results()
					when /^(f|F)irm*/
					@str_firmware=filter_results()
					when /^(c|C)ountry*/
					@str_country=filter_results()
				end
			end
		}
	@str_return=@str_mode+'-'+@str_country+@str_firmware
	return @str_return[0..(@str_return.index(' ')-@str_return.length-1)]
  end	
  
end

#-------------------------------------------------------------------------------------------------------------#
# class TR69_Do_Test
#                                                                              
# Purpose: Excute each method of TR069 definition.
#------------------------------------------------------------------------------------------------------------#
class TR69_Do_Test
  def initialize(relative_currpath=nil,logbasename=nil,javasdkver=nil)
    @topdir = relative_currpath
    @dir_testsuite = File.join(@topdir,"tr69_device_test_suite")
    @dir_testsuiteBin = File.join(@dir_testsuite,"bin")
    @dir_testsuiteInputs = File.join(@dir_testsuiteBin,"inputs")
    
    @dir_testsuiteLIB = File.join(@dir_testsuite,"lib")
    @dir_testsuiteConf = File.join(@dir_testsuite,"conf")
    @javasdkname=javasdkver
  
    dir_Log = File.join(@topdir,'log')
    @logfilepath = File.join(dir_Log,logbasename)
    @logCPEParaLists = File.join(dir_Log,"paralist.txt")
    @logCPEParam = File.join(dir_Log,"paraname.txt")
    @logCPEParamRO = File.join(dir_Log,"paranameRO.txt")
    @logCPEParamRW = File.join(dir_Log,"paranameRW.txt")
    @logCPEParam_Next = File.join(dir_Log,"paraname_next.txt")
    
    @logCPEParamValues = File.join(dir_Log,"paranameValues.csv")
    @logCPETypeValues = File.join(dir_Log,"paraTypeValues.csv")
    @logCPEParamType  = File.join(dir_Log,"paranameType.txt")
    @logCPEType = File.join(dir_Log,"paraType.csv")
#    p"pwd=#{Dir.pwd}"
#    p"@dir_testsuite=#{@dir_testsuite}"
#    p"@logfilepath=#{@logfilepath}"
  end
  
  def start(str_taskcsv=nil)
  jrename=@javasdkname
  libs=@dir_testsuiteLIB
  conf=@dir_testsuiteConf
  
  jrepath=File.join(File.join(@topdir,jrename),"bin/java")
  #p "@topdir=#{@topdir}; @dir_testsuiteBin=#{@dir_testsuiteBin}"
  
  str_exec=jrepath+" -Xms32m -Xmx160m -Djava.endorsed.dirs="+\
            libs+"/endorsed -classpath "+\
            libs+"/bacdts.jar:"+\
            libs+"/saaj-api.jar:"+\
            libs+"/saaj-impl.jar:"+\
            libs+"/mail.jar:"+\
            libs+"/activation.jar:"+\
            libs+"/baccwmpsoap.jar:"+\
            libs+"/xercesImpl.jar:"+\
            libs+"/xml-apis.jar:"+\
            libs+"/xmlParserAPIs.jar:"+\
            libs+"/xercesImpl.jar:"+\
            libs+"/ant-bac-utils.jar:"+\
            conf+":"+\
            libs+" com.cisco.bac.cli.Dtscli "+\
            str_taskcsv
            
  Show_Outcomes.new("Execute Utility.  Pls waiting~~~~~~~~~").log_screen
  begin_time = Time.now
  
  
  
  #--------------chdir to path of  tr69_device_test_suite/bin --------------#
  Dir.chdir(@dir_testsuiteBin)
  
  #arr_screendump=IO.popen(str_exec).readlines
  hIO=IO.popen(str_exec)
  arr_screendump=hIO.readlines
  #Show_Outcomes.new("ExecResult=#{arr_screendump}",logfilepath).log_logfile
  #Show_Outcomes.new("Parent= #{Process.pid}.").log_screen
  hIO.close
  #--------------waiting hIO termination--------------#
  sleep 1.5;
  
  #--------------chdir to path of  $FILENAME--------------#
  path_currexefile=File.dirname(File.expand_path($FILENAME))
  #p "@logfilepath=#{@logfilepath}"
  Show_Outcomes.new("path_currexefile=#{path_currexefile}").log_screen
  
  #Dir.chdir(path_currexefile)
  
  elapsed_time = Time.now - begin_time
  Show_Outcomes.new("Executation duration=#{elapsed_time} sec.").log_screen
  Show_Outcomes.new("Executation duration=#{elapsed_time} sec.",@logfilepath).log_logfile
  
  
  return arr_screendump
  end
  
  def filter_rorw_parameters(arr_in,arr_out)
    arr_temp=[]
    #exclude entity arr_out in arr_in
    arr_out -= arr_in
    arr_out.each{|i| arr_temp.push(i.chomp)}
    
    return arr_temp    
  end
  
  def Run_First(method,methodtarge,cPEParam)
    #arr_CPEParamList.each{|x| p"#{x}"}
    IDGParamters_Config.new.Update_CfgFile(@dir_testsuiteInputs,\
                                      method,methodtarge,cPEParam)
    
    Show_Outcomes.new("Update #{cPEParam}",@logfilepath).log_logfile
    
    arr_screendump=self.start('task.csv')

    arr_CPEParamLists=Get_CPE_ParamLists.new.filter(method,methodtarge,arr_screendump)
    #Show_Outcomes.new("ParamResult=#{arr_CPEParamLists};Len=#{arr_CPEParamLists.length}").log_screen
    Show_Outcomes.new("#{arr_CPEParamLists};Len=#{arr_CPEParamLists.length}",
                  @logfilepath).log_logfile

    arr_next_CPEParamLists=Get_CPE_ParamLists.new.filter_next(arr_CPEParamLists)
    #Show_Outcomes.new("Next ParamResult=#{arr_next_CPEParamLists} Len=#{arr_next_CPEParamLists.length}").log_screen
    Dump_File.new.dump_results(@logCPEParaLists,arr_next_CPEParamLists)
  
    arr_next_CPEParameters=Get_CPE_ParamLists.new.get_name(arr_next_CPEParamLists)
    #Show_Outcomes.new("Next Param=#{arr_next_CPEParameters} Len=#{arr_next_CPEParameters.length}").log_screen
    #Dump_File.new.dump_results(LogCPEParam,arr_next_CPEParameters)

    #Get Read-Only and ReadWrite paramlist
    arr_RO=Get_CPE_ParamLists.new.get_ROParam_name(arr_next_CPEParamLists)
    arr_RW=Get_CPE_ParamLists.new.get_RWParam_name(arr_next_CPEParamLists)

    if !arr_RO.empty? then
      #exclude entity of arr_RO in arr_next_CPEParameters
      arr_temp=filter_rorw_parameters(arr_RO,arr_next_CPEParameters)
      
      Dump_File.new.dump_results(@logCPEParamRO,arr_temp)
    end

    if !arr_RW.empty? then
      #exclude entity of arr_RW in arr_next_CPEParameters
      arr_temp=filter_rorw_parameters(arr_RW,arr_next_CPEParameters)
      
      Dump_File.new.dump_results(@logCPEParamRW,arr_temp)
    end
  
    return  arr_next_CPEParameters
  end
  
  def Run_Second(arr_CPEParameters,what,how)
    arr_temp1=[];arr_temp2=[];arr_temp3=[];arr_temp4=[]
    
    #-----------Filter inculde next level paramter
    arr_ret=[]
    arr_CPEParameters.each{|x|
      #p "x=#{x}"
      case x
        when /\.;$/
          arr_ret.push(x)
      end
    }
    
    arr_ret.each{|x|
      #delete last char ";"
      y=x.chop;
      IDGParamters_Config.new.Update_CfgFile(@dir_testsuiteInputs,\
                                        what,how,y)
                                        
      Show_Outcomes.new("Update #{y}",@logfilepath).log_logfile
      
      arr_screendump=self.start('task.csv')

      arr_CPEParamLists=Get_CPE_ParamLists.new.filter(what,how,arr_screendump)
      #Show_Outcomes.new("ParamResult=#{arr_CPEParamLists};Len=#{arr_CPEParamLists.length}").log_screen
      Show_Outcomes.new("#{arr_CPEParamLists};Len=#{arr_CPEParamLists.length}",
                    @logfilepath).log_logfile
  
      arr_next_CPEParamLists=Get_CPE_ParamLists.new.filter_next(arr_CPEParamLists)
      #Show_Outcomes.new("Next ParamResult=#{arr_next_CPEParamLists} Len=#{arr_next_CPEParamLists.length}").log_screen
  
      #Make sure arr_temp1 is string array
      arr_next_CPEParamLists.each{|y| arr_temp1.push(y)}
  
      arr_next_CPEParameters=Get_CPE_ParamLists.new.get_name(arr_next_CPEParamLists)
      
      if !arr_next_CPEParameters.empty? then
        Show_Outcomes.new("Total #{arr_next_CPEParameters.length} RO/RW Parameters.").log_screen
        
        #Make sure arr_temp2 is string array
        arr_next_CPEParameters.each{|z| arr_temp2.push(z)}
      end
      
      #Get Read-Only and ReadWrite paramlist
      arr_RO=Get_CPE_ParamLists.new.get_ROParam_name(arr_next_CPEParamLists)
      arr_RW=Get_CPE_ParamLists.new.get_RWParam_name(arr_next_CPEParamLists)
      
      i=0
      if !arr_RO.empty? then 
        arr_RO.each{|x|
          i += 1
          Show_Outcomes.new("#{i}th RO Parameter=#{x}").log_screen    
        }
        
        #arr_temp3 << arr_RO;#can't count acutual array entity number
        arr_temp3 = arr_temp3+arr_RO
      end
      
      Show_Outcomes.new("--------------------------------------------------------").log_screen
      
      if !arr_RW.empty? then 
        arr_RW.each{|y|
          i += 1
          Show_Outcomes.new("#{i}th RW Parameter=#{y}").log_screen
        } 
        
        #arr_temp4 << arr_RW;#can't count acutual array entity number
        arr_temp4 = arr_temp4+arr_RW
      end  
    
    }

    Dump_File.new.dump_results(@logCPEParaLists,arr_temp1)
    
    #if !arr_temp2.empty? then 
    #Dump_File.new.dump_results(@logCPEParam,arr_temp2)
    #end
    
    #----------- Filter exist next level parameter -----------#
    arr_next_CPEParameters=[]
    arr_next_CPEParameters_chop=[]
    arr_temp2.each{|x|
      #p "x=#{x}"
      case x
        when /\.;$/
          arr_next_CPEParameters.push(x)
          arr_next_CPEParameters_chop.push(x.chop)
      end
    }
    Dump_File.new.dump_results(@logCPEParam_Next,arr_next_CPEParameters)
    
    #-----Exclude next level parameter
    #---arr_temp3 -= arr_next_CPEParameters_chop
    #-------above maybe not all right
    if !arr_temp3.empty? then
      Show_Outcomes.new("Total #{arr_temp3.length} RO Parameters.").log_screen
      arr_temp3.push("Total #{arr_temp3.length} RO Parameters.")
      Dump_File.new.dump_results(@logCPEParamRO,arr_temp3)
    end
    
    #-----Exclude next level parameter
    #-----arr_temp4 -= arr_next_CPEParameters_chop
    #-------above maybe not all right
    if !arr_temp4.empty? then 
      Show_Outcomes.new("Total #{arr_temp4.length} RW Parameters.").log_screen
      arr_temp4.push("Total #{arr_temp4.length} RW Parameters.")
      Dump_File.new.dump_results(@logCPEParamRW,arr_temp4)
    end
  
    return arr_next_CPEParameters
    
  end
#-------------------------------------------------------------------------------------------------------------#
# def Get_ParameterNames(cPEParam,dut_ip=nil)
#                                                                              
# Nov22-2005 Add Check_DUT_httpd_alive exception handle
#------------------------------------------------------------------------------------------------------------#
  def Get_ParameterNames(cPEParam,dut_ip=nil)
    method="Get";methodtarge="ParameterNames"
    method.to_s;methodtarge.to_s
    RPCMethods_Config.new.Update_CfgFile(@dir_testsuiteBin,'task.csv',\
                                        method,methodtarge)

    arr_temp1=[];arr_temp2=[]
    arr_tempIn=[];arr_tempOut=[]
    
    if dut_ip == nil
      retval=true
    else
      retval=Check_DUT_httpd_alive.new(dut_ip,@logfilepath).check_alive
    end
    
    if retval != false then
      arr_temp1=self.Run_First(method,methodtarge,cPEParam)
    else
      return false
    end
    
    if dut_ip == nil
      retval=true
    else
      retval=Check_DUT_httpd_alive.new(dut_ip,@logfilepath).check_alive
    end
    
    #----Nov21/07 Add by philip
    #----delete original cPEParam value
    arr_temp1.delete("InternetGatewayDevice.;")
    
    if retval != false then
      arr_temp2=self.Run_Second(arr_temp1,method,methodtarge)
    else
      return false
    end
    
    arr_tempIn=arr_temp2
    until arr_tempIn.empty?
      if dut_ip == nil
        retval=true
      else
        retval=Check_DUT_httpd_alive.new(dut_ip,@logfilepath).check_alive
      end
      
      if (retval != false) then
        arr_tempOut=self.Run_Second(arr_tempIn,method,methodtarge)
        arr_tempIn=arr_tempOut
      else
        return false
      end
    end
    
  end

  def Get_ParameterValues(logCPEParamRORW)
    what='Get';how='ParameterValues'
    what.to_s;how.to_s
    RPCMethods_Config.new.Update_CfgFile(@dir_testsuiteBin,'task.csv',\
                                      what,how)

    strCPEParamListFilename=what+how+"RpcTask.input"
    fullpath_cfgfile=File.join(@dir_testsuiteInputs,strCPEParamListFilename)
    arr_tempFileCnt=IO.readlines(logCPEParamRORW)
    arr_ParamRORW=[]
    arr_LogGetValues=[];arr_LogTypeValue=[];arr_LogType=[]

    arr_tempFileCnt.each{|x|
      case x
      when /^InternetGatewayDevice/ 
        arr_ParamRORW.push(x) 
      end
    }

    arr_ParamRORW.each{|x|
      hFile=File.new(fullpath_cfgfile,'w')
      hFile.puts "#{x.strip}"  
      hFile.flush
      hFile.close

      Show_Outcomes.new("Update #{x.strip} in #{strCPEParamListFilename}.").log_screen 
      Show_Outcomes.new("Update #{x.strip} in #{strCPEParamListFilename}.",@logfilepath).log_logfile
    
      str_TypeValue ="#{x.strip}"
      str_Type      =String.new
    
      arr_screendump=self.start(@dir_testsuite,@dir_testsuiteLIB)
      arr_GetValues=Get_CPE_ParamValues.new.filter(arr_screendump)
    
      if arr_GetValues == false then
        str_TypeValue += ','+'ERROR'+','+'ERROR'
      else
        Show_Outcomes.new("arr_GetValues=#{arr_GetValues};Len=#{arr_GetValues.length}").log_screen
    
        arr_TypeValue=Get_CPE_ParamValues.new.filter_type_value(arr_GetValues)
        arr_TypeValue.each{|y|; str_TypeValue += ','+"#{y}"}
        
        arr_Type=Get_CPE_ParamValues.new.filter_type(arr_GetValues)
        arr_Type.each{|z|; str_Type += ','+"#{z}"}
      end
    
      Show_Outcomes.new("str_TypeValue=#{str_TypeValue}").log_screen
      Show_Outcomes.new("str_Type=#{str_Type}").log_screen
    
      arr_LogGetValues.push(arr_GetValues)
      arr_LogTypeValue.push(str_TypeValue)
      arr_LogType.push(str_Type)
      
      if arr_LogTypeValue.length > 10 then 
        Dump_File.new.dump_results(@logCPETypeValues,arr_LogTypeValue.uniq)
        arr_LogTypeValue.clear
      end
      
      if arr_LogType.length > 10 then 
        Dump_File.new.dump_results(@logCPEParamType,arr_LogType.uniq)
        arr_LogType.clear
      end
    }

    Dump_File.new.dump_results(@logCPEParamValues,arr_LogGetValues.uniq)
    Dump_File.new.dump_results(@logCPETypeValues,arr_LogTypeValue.uniq)
    Dump_File.new.dump_results(@logCPEParamType,arr_LogType.uniq)
  end

  def Get_ParameterValues_atonce(arr_CPEParamRORW)
    what='Get';how='ParameterValues'
    what.to_s;how.to_s
    RPCMethods_Config.new.Update_CfgFile(@dir_testsuiteBin,'task.csv',\
                                      what,how)

    strCPEParamListFilename=what+how+"RpcTask.input"
    fullpath_cfgfile=File.join(@dir_testsuiteInputs,strCPEParamListFilename)
#    arr_tempFileCnt=IO.readlines(logCPEParamRORW)
    arr_tempFileCnt=arr_CPEParamRORW
    arr_ParamRORW=[]
    arr_LogGetValues=[];arr_LogTypeValue=[];arr_LogType=[]
    
    hFile=File.new(fullpath_cfgfile,'w')
    
    arr_tempFileCnt.each{|x|
      case x
      when /^InternetGatewayDevice/ 
        arr_ParamRORW.push(x)
        hFile.puts x
      end
    }
    
    hFile.flush
    hFile.close

      #Show_Outcomes.new("Update #{arr_ParamRORW} in #{strCPEParamListFilename}.").log_screen 
      Show_Outcomes.new("Update #{arr_ParamRORW} in #{strCPEParamListFilename}.",@logfilepath).log_logfile
    
      str_TypeValue=String.new;str_TypeValue="GetParamterValues"
      
      arr_screendump=self.start(@dir_testsuite,@dir_testsuiteLIB)
      arr_GetValues=Get_CPE_ParamValues.new.filter(arr_screendump)
    
      if arr_GetValues == false then
        str_TypeValue += ','+'ERROR'+','+'ERROR'
        arr_LogTypeValue.push(str_TypeValue)
      else
        Show_Outcomes.new("arr_GetValues=#{arr_GetValues};Len=#{arr_GetValues.length}").log_screen
    
        arr_ParamTypeValue=Get_CPE_ParamValues.new.filter_param_type_value(arr_GetValues)
        #count purpose
        until arr_ParamTypeValue.empty?
          str_Value = arr_ParamTypeValue.pop
          str_Type  = arr_ParamTypeValue.pop
          str_Param = arr_ParamTypeValue.pop
          str_TypeValue += str_Param.to_s
          str_TypeValue += ','+str_Type.to_s
            #---------------------------------------------------------
            #Get Type only not include value to compare tr69 standard
            #---------------------------------------------------------
            arr_LogType.push(str_TypeValue)
          str_TypeValue += ','+str_Value.to_s
          
          #Show_Outcomes.new("str_TypeValue=#{str_TypeValue}").log_screen+"\n"
          arr_LogTypeValue.push(str_TypeValue)
          str_TypeValue=String.new
        end
        
        #arr_LogTypeValue.reverse_each{|z|; Show_Outcomes.new("#{z}").log_screen
        #}
        Show_Outcomes.new("arr_LogTypeValue= #{arr_LogTypeValue}",@logfilepath).log_logfile
        Show_Outcomes.new("arr_LogType= #{arr_LogType}",@logfilepath).log_logfile
      end

    arr_LogGetValues.push(arr_GetValues)

    Dump_File.new.dump_results(@logCPEParamValues,arr_LogGetValues.uniq)
    #Dump_File.new.dump_results(@logCPETypeValues,arr_LogTypeValue)
    Dump_File.new.dump_results(@logCPEParamType,arr_LogType)
  end
  def  pnt_stderror(arrtemp)
    arrtemp.each{|x| print x}
  end
  def SetGet_ParameterValues_atonce(arrCPEParamRW,what,hash_csv_line=nil)
    #---------csv file header 
    #assert_equal([:testcase_id, :prefix, :numindex, :paramname, :paramvalue],line.headers)
    how='ParameterValues'
    what.to_s;how.to_s
    RPCMethods_Config.new.Update_CfgFile(@dir_testsuiteBin,'task.csv',what,how)

    strCPEParamListFilename=what+how+"RpcTask.input"
    fullpath_cfgfile=File.join(@dir_testsuiteInputs,strCPEParamListFilename)
    arr_tempCnt=arrCPEParamRW
    arr_ParamRW=[]
    arr_LogGetValues=[];arr_LogTypeValue=[];arr_LogType=[]
    
    hFile=File.new(fullpath_cfgfile,'w')
    
    arr_tempCnt.each{|x|
      case x
      when /^InternetGatewayDevice/ 
        arr_ParamRW.push(x)
        hFile.puts x
      end
    }
    
    hFile.flush
    hFile.close
    
    str_ParamTypeValue=what+how
    arr_LogParamTypeValue=[]
    
    arr_screendump=self.start('task.csv')
    
    #---- initial value
    str_ParamTypeValue=String.new
    
    case what
      when /Set/
        #---- Cause Apply setparanamevalue error msg
        if hash_csv_line[:paramname].downcase == 'apply'
          ret_str='false_Apply'
        else  
          hGet_CPE_ParamLists=Get_CPE_ParamLists.new
          hGet_CPE_ParamValues=Get_CPE_ParamValues.new  
        
          ret_value=hGet_CPE_ParamValues.filter_SetParamValue(arr_screendump)
          #arr_SetValues=hGet_CPE_ParamLists.get_XML_RPCResponse(arr_screendump)
          #---------------------------------------------------------------------------
          # To parse SetParameterValues RPC to check setvaule status code and getvalue result by Shin
          #---------------------------------------------------------------------------
          arr_pos_start_end_numidx=hGet_CPE_ParamLists.get_XML_RPCResponse_Index(arr_screendump)
          
          arr_temp1=arr_screendump[(arr_pos_start_end_numidx[0]+1+1) .. (arr_pos_start_end_numidx[1]-1-1)]
          arr_temp2=arr_screendump[(arr_pos_start_end_numidx[2]+1+1) .. (arr_pos_start_end_numidx[3]-1-1)]
          
          
          #--------- Only on Windows ---------#
          #arr_SetValues=+arr_temp1
          #arr_SetValues.each{|x| arr_SetParamValues_XML.push(x.chomp)}
          #--------- Only on Windows ---------#
          
          arr_SetParamValues_XML=[]
          arr_temp1.each{|x| arr_SetParamValues_XML.push(x.chomp)}
          arr_temp2.each{|x| arr_SetParamValues_XML.push(x.chomp)}
          
          #pnt_stderror(arr_temp1)
          #pnt_stderror(arr_temp2)
          #pnt_stderror(arr_SetParamValues_XML)
          
          ret_str=hGet_CPE_ParamValues.filter_SetParamValue_NameValue(arr_SetParamValues_XML,ret_value.to_s)
        end  
        
        if ret_value == false
          str_ParamTypeValue += ','+ret_str
        end
        arr_LogParamTypeValue.push(ret_str)
        
      when /Get/
        arr_GetValues=Get_CPE_ParamValues.new.filter(arr_screendump)
        
        if arr_GetValues == false then
          str_ParamTypeValue += ','+'ERROR'+','+'ERROR'
          arr_LogParamTypeValue.push(str_ParamTypeValue)
        else
          #Show_Outcomes.new("#{str_ParamTypeValue}=#{arr_GetValues};Len=#{arr_GetValues.length}").\
          #                  log_screen
          arr_ParamTypeValue=Get_CPE_ParamValues.new.filter_param_type_value(arr_GetValues)
          
          #---spolit purpose on screen
          str_temp='--------------------------------------------------------------------------'  
          Show_Outcomes.new(str_temp).log_screen
          
          #---count purpose
          until arr_ParamTypeValue.empty?
            #p "empty=#{arr_ParamTypeValue.empty?}"
            str_Value = arr_ParamTypeValue.pop
            str_Type  = arr_ParamTypeValue.pop
            str_Param = arr_ParamTypeValue.pop
            str_ParamTypeValue += str_Param.to_s
            str_ParamTypeValue += ','+str_Type.to_s
            #---------------------------------------------------------
            #Get Type only not include value to compare tr69 standard
            #---------------------------------------------------------
            arr_LogType.push(str_ParamTypeValue)
        
            str_ParamTypeValue += ','+str_Value.to_s
        
            Show_Outcomes.new("str_ParamTypeValue=#{str_ParamTypeValue}").log_screen
            arr_LogParamTypeValue.push(str_ParamTypeValue)
        
            str_ParamTypeValue=String.new
          end
          #---spolit purpose on screen
          str_temp='--------------------------------------------------------------------------'  
          Show_Outcomes.new(str_temp).log_screen
    
          arr_LogParamTypeValue.each{|x| 
            Show_Outcomes.new("arr_LogParamTypeValue= #{x}",@logfilepath).log_logfile}
          arr_LogType.each{|x|
            Show_Outcomes.new("arr_LogType= #{x}",@logfilepath).log_logfile}
        end
        
        #---------Nov29/07 add by philip
        if arr_GetValues == false then
          #Remove header
          arr_LogParamTypeValue.shift
        end
        
    end;#case what
    
    #---------------------------------------------------------
    #Chose output result file by Get or Set
    #---------------------------------------------------------
    if what == 'Get' then
      Dump_File.new.dump_results(@logCPEParamType,arr_LogType)
      Dump_File.new.dump_results(@logCPEParamValues,arr_LogParamTypeValue)
    elsif what == 'Set' then
      #Dump_File.new.dump_results(@logCPETypeValues,arr_LogParamTypeValue)
      Dump_File.new.\
          dump_results(@logCPEParamValues,arr_LogParamTypeValue)
    end
    
    return arr_LogParamTypeValue
  end

  def Compare_ParameterType(fullpath_srcfilename,fullpath_dstfilename)
    arr_ParamRORW=[];arr_TR69ParamRORW=[]

    arr_dstFileCnt=IO.readlines(fullpath_dstfilename)
    arr_dstFileCnt.each{|x|
      case x
        when /^InternetGatewayDevice/ 
          arr_ParamRORW.push(x) 
      end
    }
  
    arr_srcFileCnt=IO.readlines(fullpath_srcfilename)
    arr_srcFileCnt.each{|x|
      case x
        when /^InternetGatewayDevice/ 
          arr_TR69ParamRORW.push(x) 
      end
    }
    
  
    arr_result=Array_Test.new(arr_TR69ParamRORW,arr_ParamRORW).DoDifference
    Dump_File.new.dump_results(@logCPEType,arr_result)
    Show_Outcomes.new("Dwt TR069 Std= #{arr_result}",@logfilepath).log_logfile
  end

  def filter_available_ParanamesList(logCPEParamRORW)
    arr_ParamTemp=[];arr_ParamTemp2=[];arr_ParamTemp3=[]
    
    #-------- Read ParameterNames from logfile "paranameRO.txt" "paranameRW.txt"
    arrCPEParamRW=IO.readlines(logCPEParamRORW)
    #assert_equal(Array,arrCPEParamRW.class)
    
    arr_tempCnt=arrCPEParamRW
    
    #-------- Filter timestamp line
    arr_tempCnt.each{|x|
      #---Returns a new String with the last character removed
      x=x.to_s.chop
      case x
      when /^InternetGatewayDevice/ 
        arr_ParamTemp.push(x)
      end
    }
    
    #assert_equal(25,arr_ParamTemp.length)
    
    #-------- Filter suffix of '.' and '.;' line
    arr_ParamTemp.each{|x|
      case x
      when /(\.|\.;)$/ 
        arr_ParamTemp2.push(x)
        #p x
      end    
    }
    
    #assert_equal(10,arr_ParamTemp2.length)
    
    arr_ParamTemp3=arr_ParamTemp-arr_ParamTemp2
    #-- clear element
    arrCPEParamRW.clear
    
    #-------- Get rid of suffix of paramnames ';'
    arr_ParamTemp3.each{|x|
      #p "#{x.to_s} " if x.to_s.include?(";")
      #p "#{x.to_s.chomp(";")} "   
      arrCPEParamRW.push(x.to_s.chomp(";") )
    }
    
    #assert_equal(15,arr_ParamTemp3.length)
    
    #arrCPEParamRW.each{|x| p x}
    #assert_equal(15,arrCPEParamRW.length)
    
    return arrCPEParamRW
  end
  
  def show_progress(totalnum,currnum)
    
    percent=(currnum.to_f/totalnum.to_f*100)
    str_temp='--------------------------------------------------------------------------'  
    Show_Outcomes.new(str_temp).log_screen
    str_temp="Total paranames="+totalnum.to_s+"; "+\
              "current times= #{currnum.to_s}"+"; "+"completion rate= #{percent.to_s}%"
    Show_Outcomes.new(str_temp).log_screen
    str_temp='--------------------------------------------------------------------------'  
    Show_Outcomes.new(str_temp).log_screen
  end
#-------------------------------------------------------------------------------------------------------------#
# def Test_GetValues_atonce(fullpath_stdlogfile,fullpath_srclogfile,dut_ip)
#                                                                              
# Purpose 
#         1.Get both type and value of paramters from DUT. 
#------------------------------------------------------------------------------------------------------------#
  def Test_GetValues_atonce(fullpath_stdlogfile,fullpath_srclogfile,
                            dut_ip=nil)
    
    #If input src file not exist then return false
    if FileTest.exist?(fullpath_srclogfile) == false then
      Show_Outcomes.new("Pls input #{fullpath_srclogfile} first.").log_screen
      return false
    end
    
    arr_ParamRORW=[]
    arr_ParamRORW_used=[]
    
    arrCPEParamRORW=self.filter_available_ParanamesList(fullpath_srclogfile)
    
    arrCPEParamRORW.each{|x|
      
      arr_ParamRORW.push(x) 
      arr_ParamRORW=arr_ParamRORW.uniq
      #--------------------------------
      #Select 5 items to do test
      #--------------------------------
      if arr_ParamRORW.length > 4 then
        if dut_ip == nil
          retval=true
        else
          retval=Check_DUT_httpd_alive.new(dut_ip,@logfilepath).check_alive
        end
        
        if retval != false then
          self.SetGet_ParameterValues_atonce(arr_ParamRORW,'Get')
        else
          return false
        end
        #-----Nov22/07 add by philip
        #Store tested parameters in arr_ParamRORW_used
        #----------------------------
        arr_ParamRORW_used += arr_ParamRORW
        
        arr_ParamRORW.clear
        
        self.show_progress(arrCPEParamRORW.length,arr_ParamRORW_used.length)
      end
    }
    
    #--------------------------------
    #Get rests remainder of 5 
    #--------------------------------
    arr_ParamRORW_rests = arrCPEParamRORW-arr_ParamRORW_used
      
    if !arr_ParamRORW_rests.empty? 
      self.SetGet_ParameterValues_atonce(arr_ParamRORW_rests,'Get')
      show_progress(arrCPEParamRORW.length,arrCPEParamRORW.length)
    end
  
  end
#-------------------------------------------------------------------------------------------------------------#
# def Hash_table_select(table_opt,hash_update=nil)
#                                                                              
# Purpose 
# Dec15     Cause _line index over 2 so add argument hash_update. 
#------------------------------------------------------------------------------------------------------------#
  def Hash_table_select(table_opt,hash_update=nil)
    require 'setting'
    handle=nil
    
    case table_opt.to_s.downcase
      when /^_mgtsrv$/ 
        handle=TR69_RW_param.new._MgtSrv
      when /^_time$/
        handle=TR69_RW_param.new._Time
      when /^_userinf$/
        handle=TR69_RW_param.new._UserInf
      when /^_lancfgsec$/
        handle=TR69_RW_param.new._LANCfgSec
      when /^_ippingdiag$/
        handle=TR69_RW_param.new._IPPingDiag

      when /^layer3forward$/  
      handle=TR69_RW_param_Layer3Forward.new.Layer3Forward
      when /^_forward$/
        handle=TR69_RW_param_Layer3Forward.new._Forward
      when /^_lanethinfcfg$/  
        handle=TR69_RW_param_LANDev.new._LANEthInfCfg
      when /^_lanusbinfcfg$/  
        handle=TR69_RW_param_LANDev.new._LANUSBInfCfg
      
      #---Casue Port-base VLAN  
      when /^_lanhostcfgmgt_vlan1$/  
        handle=TR69_RW_param_LANDev.new('1')._LANHostCfgMgt
      when /^_lanhostcfgmgt_vlan2$/  
        handle=TR69_RW_param_LANDev.new('2')._LANHostCfgMgt
      when /^_lanhostcfgmgt_vlan3$/  
        handle=TR69_RW_param_LANDev.new('3')._LANHostCfgMgt
      when /^_lanhostcfgmgt_vlan4$/  
        handle=TR69_RW_param_LANDev.new('4')._LANHostCfgMgt
      when /^_lanhostcfgmgt_vlan5$/  
        handle=TR69_RW_param_LANDev.new('5')._LANHostCfgMgt
      when /^_lanhostcfgmgt_vlan6$/  
        handle=TR69_RW_param_LANDev.new('6')._LANHostCfgMgt
      when /^_lanhostcfgmgt_vlan1_ipinf$/  
        handle=TR69_RW_param_LANDev.new('1')._LANHostCfgMgt_IPInf
      when /^_lanhostcfgmgt_vlan2_ipinf$/  
        handle=TR69_RW_param_LANDev.new('2')._LANHostCfgMgt_IPInf
      when /^_lanhostcfgmgt_vlan3_ipinf$/  
        handle=TR69_RW_param_LANDev.new('3')._LANHostCfgMgt_IPInf
      when /^_lanhostcfgmgt_vlan4_ipinf$/  
        handle=TR69_RW_param_LANDev.new('4')._LANHostCfgMgt_IPInf
      when /^_lanhostcfgmgt_vlan5_ipinf$/  
        handle=TR69_RW_param_LANDev.new('5')._LANHostCfgMgt_IPInf
      when /^_lanhostcfgmgt_vlan6_ipinf$/  
        handle=TR69_RW_param_LANDev.new('6')._LANHostCfgMgt_IPInf
        
      #---Casue Port-base VLAN    
      when /^_wlancfg_vlan1$/  
        handle=TR69_RW_param_LANDev.new('1')._WLANCfg
      when /^_wlancfg_vlan2$/  
        handle=TR69_RW_param_LANDev.new('2')._WLANCfg  
      when /^_wlancfg_vlan3$/  
        handle=TR69_RW_param_LANDev.new('3')._WLANCfg  
      when /^_wlancfg_vlan4$/  
        handle=TR69_RW_param_LANDev.new('4')._WLANCfg  
      when /^_wlancfg_vlan5$/  
        handle=TR69_RW_param_LANDev.new('5')._WLANCfg  
      when /^_wlancfg_vlan6$/  
        handle=TR69_RW_param_LANDev.new('6')._WLANCfg  
      when /^_wlancfg_wepkey$/  
        handle=TR69_RW_param_LANDev.new._WLANCfg_WEPKey
      when /^_wlancfg_presharedkey$/  
        handle=TR69_RW_param_LANDev.new._WLANCfg_PreSharedKey
 
      when /^_wancomminfcfg$/  
        handle=TR69_RW_param_WANDev.new._WANCommInfCfg
      when /^_wanethinfcfg$/  
        handle=TR69_RW_param_WANDev.new._WANEthInfCfg
      when /^_wandslinfcfg$/  
        handle=TR69_RW_param_WANDev.new._WANDSLInfCfg
      when /^_wandsldiag$/  
        handle=TR69_RW_param_WANDev.new._WANDSLDiag
      
      when /^_wandsllinkcfg$/    
        handle=TR69_RW_param_WANDev_WANConDev.new._WANDSLLinkCfg
      when /^_wanatmf5loopbackdiag$/      
        handle=TR69_RW_param_WANDev_WANConDev.new._WANATMF5LoopbackDiag
      when /^_wanpostlinkcfg$/    
        handle=TR69_RW_param_WANDev_WANConDev.new._WANPOSTLinkCfg
      when /^_wanipconn$/      
        handle=TR69_RW_param_WANDev_WANConDev.new._WANIPConn
      when /^_wanipconn_portmap$/    
        handle=TR69_RW_param_WANDev_WANConDev.new._WANIPConn_PortMap
      when /^_wanpppconn$/      
        handle=TR69_RW_param_WANDev_WANConDev.new._WANPPPConn
      when /^_wanpppconn_portmap$/
        handle=TR69_RW_param_WANDev_WANConDev.new._WANPPPConn_PortMap
      
      when /^vocprofile$/
        handle=TR104_RW_param_VocProfile.new.VocProfile
      when /^_srvprdinfo$/
        handle=TR104_RW_param_VocProfile.new._SrvPrdInfo
      when /^_sip$/
        handle=TR104_RW_param_VocProfile.new._SIP
      when /^_sip_evtsubscrb$/
        handle=TR104_RW_param_VocProfile.new._SIP_EvtSubscrb
      when /^_sip_respmap$/
        handle=TR104_RW_param_VocProfile.new._SIP_RespMap
      when /^_mgcp$/
        handle=TR104_RW_param_VocProfile.new._MGCP
      when /^_h323$/
        handle=TR104_RW_param_VocProfile.new._H323
      when /^_rtp$/
        handle=TR104_RW_param_VocProfile.new._RTP
      when /^_rtp_rtcp$/
        handle=TR104_RW_param_VocProfile.new._RTP_RTCP
      when /^_rtp_srtp$/
        handle=TR104_RW_param_VocProfile.new._RTP_SRTP
      when /^_rtp_redcy$/
        handle=TR104_RW_param_VocProfile.new._RTP_Redcy
      when /^_numplan$/
        handle=TR104_RW_param_VocProfile.new._NumPlan
      when /^_numplan_prefixinfo$/
        handle=TR104_RW_param_VocProfile.new._NumPlan_PrefixInfo
      when /^_tone_event$/
        handle=TR104_RW_param_VocProfile.new._Tone_Event
      when /^_tone_descrp$/
        handle=TR104_RW_param_VocProfile.new._Tone_Descrp
      when /^_tone_patt$/
        handle=TR104_RW_param_VocProfile.new._Tone_Patt
      when /^_buttmap_butt$/
        handle=TR104_RW_param_VocProfile.new._ButtMap_Butt
      when /^_faxt38$/
        handle=TR104_RW_param_VocProfile.new._FaxT38
      when /^_line$/
        handle=TR104_RW_param_VocProfile.new._Line
      when /^_line_sip_eventsubscrb$/
        hline=TR104_RW_param_VocProfile.new._Line
        
        handle=TR104_RW_param_VocProfile.new.\
              _Line_SIP_EventSubscrb(hline.update(hash_update))
      when /^_line_mgcp$/
        hline=TR104_RW_param_VocProfile.new._Line
        
        handle=TR104_RW_param_VocProfile.new.\
              _Line_MGCP(hline.update(hash_update))
      when /^_line_h323$/
        hline=TR104_RW_param_VocProfile.new._Line
        
        handle=TR104_RW_param_VocProfile.new.\
              _Line_H323(hline.update(hash_update))
      when /^_line_ringer_event$/
        hline=TR104_RW_param_VocProfile.new._Line
#        hline.update(arr_update).each{|key,value| p"#{key}, #{value}"}
        handle=TR104_RW_param_VocProfile.new.\
              _Line_Ringer_Event(hline.update(hash_update))
      when /^_line_ringer_descrp$/
        handle=TR104_RW_param_VocProfile.new._Line_Ringer_Descrp
      when /^_line_ringer_patt$/
        hline=TR104_RW_param_VocProfile.new._Line
        
        handle=TR104_RW_param_VocProfile.new.\
              _Line_Ringer_Patt(hline.update(hash_update))
      when /^_line_callingfeatures$/
        hline=TR104_RW_param_VocProfile.new._Line
        
        handle=TR104_RW_param_VocProfile.new.\
              _Line_CallingFeatures(hline.update(hash_update))
      when /^_line_vocprocess$/
        hline=TR104_RW_param_VocProfile.new._Line
        
        handle=TR104_RW_param_VocProfile.new.\
              _Line_VocProcess(hline.update(hash_update))
      when /^_line_codec_list$/
        hline=TR104_RW_param_VocProfile.new._Line
        
        handle=TR104_RW_param_VocProfile.new.\
              _Line_Codec_List(hline.update(hash_update))
      when /^_line_stats$/
        hline=TR104_RW_param_VocProfile.new._Line
        
        handle=TR104_RW_param_VocProfile.new.\
              _Line_Stats(hline.update(hash_update))
      when /^_phyinf_tests$/
        handle=TR104_RW_param_VocProfile.new._PhyInf_Tests
      else
        handle=nil
    end
    
    return handle
  end
  
  def ParamList_Addindex(handle_HashTable,retwhat,csv_line)
    str_SetParam=String.new;arr_SetParamValue=[]
    str_GetParam=String.new;arr_GetParamValue=[]
    str_ChkSetParam=String.new;arr_ChkSetParamValue=[]
    
    handle_HashTable.each{|key, value|
      #--------------------------
      #case sensitive equality
      #--------------------------
      if key === csv_line[:paramname] then
              
        #make paramlist string
        str_SetParam += handle_HashTable[:prefix].to_s
              
        #Chk if key==:numindex exist
        if handle_HashTable.has_key?(:numindex)
          str_SetParam += handle_HashTable[:numindex].to_s+'.'
          #p "key = #{key}; csv_line[:paramname] = #{csv_line[:paramname]}"
        end
        str_SetParam += key
              
        str_GetParam  =str_SetParam
        #For after setvalues checking
        str_ChkSetParam=str_SetParam
              
        str_SetParam += ','+csv_line[:paramvalue]
        str_SetParam += ','+value
          
        str_ChkSetParam += ','+value
        str_ChkSetParam += ','+csv_line[:paramvalue]
              
        arr_SetParamValue.push(str_SetParam)
        arr_GetParamValue.push(str_GetParam)
        arr_ChkSetParamValue.push(str_ChkSetParam)
          
        str_SetParam=String.new
        str_GetParam=String.new
        str_ChkSetParam=String.new
              
      end;#if key === csv_line[:paramname] then
            
    };#handle_HashTable.each
    
    case retwhat.to_s
      when /^Set/
        return arr_SetParamValue
      when /^Get/
        return arr_GetParamValue
      when /^ChkSet/
        return arr_ChkSetParamValue
    end
  end
  
  def GetSet_ParamList_make(handle_HashTable,retwhat,csvline,opt_hash=nil)
    
    handle_HashTable.update(opt_hash) if (opt_hash != nil)
    
    retarr=self.ParamList_Addindex(handle_HashTable,retwhat,csvline)
    return retarr
  end
  
  def testcaseid_number(str_testcaseid)
    arr_testcaseid=[]
  
    str_testcaseid.split(',').each{|numidx|
      #p "#{numidx}"
      if numidx.include?("-")
        arr_temp=numidx.split('-')
        #length_numidx=arr_temp[1].to_i-arr_temp[0].to_i+1
        #p "#{length_numidx}; #{length_numidx.class}"
        for t in arr_temp[0].to_i..arr_temp[1].to_i
          arr_testcaseid.push("%04d" %t)
        end    
      else
        arr_testcaseid.push(numidx)
      end
    };#str_testcaseid.split(',').each
  
    return arr_testcaseid
  end
  
  def Test_SetValues(arr_SetParamValue,csvlogfile,hash_csv_line)
    #---------csv file header 
    #assert_equal([:testcase_id, :prefix, :numindex, :paramname, :paramvalue],line.headers)
    
    arr_RetSetParamValue=\
        self.SetGet_ParameterValues_atonce(arr_SetParamValue,'Set',hash_csv_line)
    
    #arr_GetParamValue,
    strtemp1=nil;
    
    #Show_Outcomes.new(arr_RetSetParamValue).log_screen
    
    arr_RetSetParamValue.each{|x|
      case x
        when /9[0-9][0-9][0-9]/
          strtemp1="!!!Value Setting is Failed!!!"
        when /false/  
          strtemp1="!!!Value Setting is Failed!!!"
        else
          strtemp1="!!!Value Setting is Successful!!!"  
      end
    };#arr_RetSetParamValue.each
    
    strtemp2="\narr_RetSetParamValue is #{arr_RetSetParamValue}"
    strtemp3=arr_RetSetParamValue.to_s+','+strtemp1+','+hash_csv_line[:testcase_id]
    
    Show_Outcomes.new(strtemp1).log_screen
    Show_Outcomes.new(strtemp1,@logfilepath).log_logfile
#    Show_Outcomes.new(strtemp2).log_screen
    Show_Outcomes.new(strtemp2,@logfilepath).log_logfile
    #Dump_File.new.dump_results(@logCPEParamValues,strtemp2)
    
    Dump_File.new.dump_results_csv(csvlogfile,strtemp3)
    
  end

  def Test_SetValues_batch(arr_SetParamValue,arr_GetParamValue,arr_ChkSetParamValue,
                          csvlogfile)
    if arr_SetParamValue.empty? then
      strtemp="~~~~~ No meet paramters to do test. ~~~~~"
      Show_Outcomes.new(strtemp).log_screen
      Show_Outcomes.new(strtemp,@logfilepath).log_logfile
    else
      arr_vaildGetParamValue=[];
      arr_vaildSetParamValue=[]
      arr_vaildChkSetParamValue=[]
      strlog=String.new
      
      #--------------------------
      #each parameter list check
      #--------------------------
      for tempint in 0..(arr_GetParamValue.length-1)
        Show_Outcomes.new("-------------------------------------------------------------").log_screen
        strtemp ="SetValues Param is #{arr_SetParamValue[tempint.to_i]}."
        Show_Outcomes.new(strtemp).log_screen
        Show_Outcomes.new("-------------------------------------------------------------").log_screen

        retStr=self.Test_SetValues(arr_GetParamValue[tempint],arr_SetParamValue[tempint])
        
        retval=retStr.to_s.include? "Successful"
        arr_vaildGetParamValue.push(arr_GetParamValue[tempint]) if retval
        arr_vaildSetParamValue.push(arr_SetParamValue[tempint]) if retval
        arr_vaildChkSetParamValue.push(arr_ChkSetParamValue[tempint]) if retval
        
        strlog+=strtemp+"\n"+retStr+"\n"
      end
      #--------------------------
      #all vaild parameter lists check
      #--------------------------
      if arr_vaildSetParamValue.empty? == false then
        strtemp=String.new
        Show_Outcomes.new("-------------------------------------------------------------").log_screen
        Show_Outcomes.new("Check SetValues Param List:").log_screen
        arr_vaildChkSetParamValue.each{|x| 
                    Show_Outcomes.new("#{x}").log_screen
                    }
        Show_Outcomes.new("-------------------------------------------------------------").log_screen
        
        arr_RetGetParamValue=self.\
            SetGet_ParameterValues_atonce(arr_vaildGetParamValue,'Get')
        
        arr_PassFail=Array_Test.new(arr_vaildChkSetParamValue,\
                      arr_RetGetParamValue).DoDifference
        ( arr_PassFail.empty? ) ? strtemp2="!!!Result is PASS!!!" \
            : strtemp2="!!!Result is FAIL!!!"
        
        Show_Outcomes.new(strtemp2).log_screen
        Show_Outcomes.new(strtemp2,@logfilepath).log_logfile
        
        #arr_RetGetParamValue.each{|x| p "#{x}" }
        
        strtemp ="GetValues of Vaild Param "+"\n"
        arr_RetGetParamValue.each{|x| strtemp += x+"\n" }
        strlog+=strtemp+strtemp2
      end
      Dump_File.new.dump_results(csvlogfile,strlog)
    end #if arr_SetParamValue.empty?
    
  end

  def Test_SetValues_atonce(handle_hash,hash_opt,csvline,csvlogfile)
    arr_SetParamValue=[];arr_GetParamValue=[]
    arr_ChkSetParamValue=[]

    arr_SetParamValue=    GetSet_ParamList_make(handle_hash,'Set',csvline,hash_opt)
    arr_GetParamValue=    GetSet_ParamList_make(handle_hash,'Get',csvline,hash_opt)
    #arr_ChkSetParamValue= GetSet_ParamList_make(handle_hash,'ChkSet',csvline,hash_opt)
    
    #-------Nov28/07 Remark by philip
    #-------Cause tr69_device_test_suite v1.4 could co getparametervalue after setparametervalue
    #self.Test_SetValues_batch(arr_SetParamValue,arr_GetParamValue,arr_ChkSetParamValue,
    #      csvlogfile)
    
    self.Test_SetValues(arr_SetParamValue,csvlogfile)
    #,arr_GetParamValue
    
  end                  
end
#-------------------------------------------------------------------------------------------------------------#
# class TR69_Rpt_Generator
#                                                                              
# Purpose: Using library DocDiff ver:0.3.2  from Author: Hisashi MORITA.
#------------------------------------------------------------------------------------------------------------#
class TR69_Rpt_Generator
  require 'API_docdiff'
  
  def initialize(dir_Log,targe_file=nil,src_file=nil,logfilename=nil)
    @targefile = File.join(dir_Log,targe_file)
    @srcfile = File.join(dir_Log,src_file)
    @fullpathLogfile=File.join(dir_Log,logfilename)
    @doc1=nil;  @doc2=nil
    @default_config = {
      :resolution    => "line",
      :encoding      => "auto",
      :eol           => "auto",
      :format        => "html",
      :cache         => true,
      :digest        => false,
      :verbose       => false
    }
    @hash_tag_title={}
#    p"pwd=#{Dir.pwd}"
  end

  def file_check()
    if !(@targefile && @srcfile) then
    Show_Outcomes.new("Specify at least 2 target files.").log_screen
    return false; end

    if !(FileTest.exist?(@targefile)) then
    Show_Outcomes.new("No such file: #{@targefile}.").log_screen
    return false;  end
    
    if !(FileTest.exist?(@srcfile)) then
    Show_Outcomes.new("No such file: #{@srcfile}.").log_screen
    return false;  end
    
    if !(FileTest.file?(@targefile)) then
    Show_Outcomes.new("#{@targefile} is not a file.").log_screen
    return false;  end

    if !(FileTest.file?(@srcfile)) then
    Show_Outcomes.new("#{@srcfile} is not a file.").log_screen
    return false;  end
  end

  def file_attr_check()
    file1_content=nil;file2_content=nil
    
    File.open(@targefile, "r"){|f| file1_content = f.read}
    File.open(@srcfile, "r"){|f| file2_content = f.read}
    
    encoding1 = CharString.guess_encoding(file1_content)
    encoding2 = CharString.guess_encoding(file2_content)
    case
      when (encoding1 == "UNKNOWN" or encoding2 == "UNKNOWN")
        strtemp="Document encoding unknown (#{encoding1}, #{encoding2})."
        Show_Outcomes.new(strtemp.to_s).log_screen
        Show_Outcomes.new(strtemp.to_s,@fullpathLogfile).log_logfile
        return false
      when encoding1 != encoding2
        strtemp="Document encoding mismatch (#{encoding1}, #{encoding2})."
        Show_Outcomes.new(strtemp.to_s).log_screen
        Show_Outcomes.new(strtemp.to_s,@fullpathLogfile).log_logfile
        return false
    end
    strtemp="#{@targefile} encoding is #{encoding1}; #{@srcfile} encoding is #{encoding2}."
    Show_Outcomes.new(strtemp.to_s).log_screen
    Show_Outcomes.new(strtemp.to_s,@fullpathLogfile).log_logfile
    
    eol1 = CharString.guess_eol(file1_content)
    eol2 = CharString.guess_eol(file2_content)
    case
      when (eol1.nil? or eol2.nil?)
        strtemp="Document eol is nil (#{eol1.inspect}, #{eol2.inspect}).  The document might be empty."
        Show_Outcomes.new(strtemp.to_s).log_screen
        Show_Outcomes.new(strtemp.to_s,@fullpathLogfile).log_logfile
        return false
      when (eol1 == 'UNKNOWN' or eol2 == 'UNKNOWN')
        strtemp="Document eol unknown (#{eol1.inspect}, #{eol2.inspect})."
        Show_Outcomes.new(strtemp.to_s).log_screen
        Show_Outcomes.new(strtemp.to_s,@fullpathLogfile).log_logfile
        return false
      when (eol1 != eol2)
        strtemp="Document eol mismatch (#{eol1}, #{eol2})."
        Show_Outcomes.new(strtemp.to_s).log_screen
        Show_Outcomes.new(strtemp.to_s,@fullpathLogfile).log_logfile
        return false
    end
    strtemp="#{@targefile} eol is #{eol1.inspect}, #{@srcfile} eol is #{eol2.inspect}."
    Show_Outcomes.new(strtemp.to_s).log_screen
    Show_Outcomes.new(strtemp.to_s,@fullpathLogfile).log_logfile
    
    @doc1 = Document.new(file1_content, encoding1, eol1)
    @doc2 = Document.new(file2_content, encoding2, eol2)
  end

  def file_process(fullpathtragefile,fullpathsrcfile)
    arrSrc=[];arrDst=[];arrSort1=[];arrSort2=[]
    #@targefile same as ../log/diff1.txt
    #@srcfile same as../log/diff2.txt
    File.delete(@targefile) if FileTest.exist?(@targefile)
    File.delete(@srcfile) if FileTest.exist?(@srcfile)
#    p"fullpathtragefile=#{fullpathtragefile}"
#    p"fullpathsrcfile=#{fullpathsrcfile}"

    arr_FileSrc=IO.readlines(fullpathtragefile)
    arr_FileSrc.each{|x|
      case x
      when /^InternetGatewayDevice./
        arrSrc.push(x)
      end
    }
    
    arr_FileDst=IO.readlines(fullpathsrcfile)
    arr_FileDst.each{|x|
      case x
      when /^InternetGatewayDevice./
        arrDst.push(x)
      end
    }
    
    arrSort1=arrSrc.sort
    hFile=File.new(@targefile,"w")
    hFile.puts arrSort1
    hFile.flush;hFile.close

    arrSort2=arrDst.sort
    hFile=File.new(@srcfile,"w")
    hFile.puts arrSort2
    hFile.flush;hFile.close

  end

  def html_header(title_opts)
    ["<?xml version=\"1.0\" encoding=\"#{@encoding||''}\"?>#{@eol_char||''}",
     "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"#{@eol_char||''}",
     "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">#{@eol_char||''}",
     "<html><head>#{@eol_char||''}",
     "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=#{@encoding||''}\" />#{@eol_char||''}",
     "<title>Difference-#{title_opts}</title>#{@eol_char||''}",
     "<style type=\"text/css\">#{@eol_char||''}" +
     " body {font-family: monospace;}#{@eol_char||''}" +
     " span.del {background: hotpink; border: thin inset;}#{@eol_char||''}" +
     " span.add {background: deepskyblue; font-weight: bolder; border: thin outset;}#{@eol_char||''}" +
     " span.before_change {background: hotpink; border: thin inset;}#{@eol_char||''}" +
     " span.after_change {background: deepskyblue; font-weight: bolder; border: thin outset;}#{@eol_char||''}" +
     " li.entry .position {font-weight: bolder; margin-top: 0em; margin-bottom: 0em; padding-top: 0em; padding-bottom: 0em;}#{@eol_char||''}" +
     " li.entry .body {margin-top: 0em; margin-bottom: 0em; padding-top: 0em; padding-bottom: 0em;}#{@eol_char||''}" +
     "</style>#{@eol_char||''}",
     "</head><body>#{@eol_char||''}"]
#    p "#{title_opts}" 
  end
  
  def run(fullpathOutputfile=nil,html_tag_title=nil,
            nocommon_opt=true)
    if self.file_check()==false then return false end
    
    return false unless self.file_attr_check()
    docdiff = DocDiff.new()
    docdiff.config.update(@default_config)
    
    @hash_tag_title={:header => html_header(html_tag_title)} \
      if html_tag_title
    
    output = docdiff.run(@doc1, @doc2,
                       docdiff.config[:resolution],
                       docdiff.config[:format],
                       docdiff.config[:digest],
                       @hash_tag_title,
                       nocommon_opt)
    strtemp="Compare #{File.basename(fullpathOutputfile,'.*')} parameters between Std."
    Show_Outcomes.new(strtemp).log_screen
    Show_Outcomes.new(strtemp.to_s,@fullpathLogfile).log_logfile
    #Delete output reslut HTML file
    File.delete(fullpathOutputfile) if FileTest.exist?(fullpathOutputfile)
    
    strtemp="Output file is #{fullpathOutputfile}"
    Show_Outcomes.new(strtemp.to_s).log_screen
    Show_Outcomes.new(strtemp.to_s,@fullpathLogfile).log_logfile
    #Dump_File.new.dump_results(fullpathlogfile,output)
    hFile=File.new(fullpathOutputfile,'a')
    hFile.puts output
    hFile.flush;hFile.close
  end
end

class Navigator_TR069
  def initialize(logfilepath,str_os_lang)
    @logfilepath=logfilepath
    @str_os_lang=str_os_lang
    @retstr_navigator_web=nil
  end
  
  def LANHostCfgMgt_check_minmaxaddress(hash_csv_line,web_objvalue)
    if web_objvalue == 'minaddress'
      str_ret_ip_1=API_WebGUI_TextField.new("ip_start_1",nil).textField_getFieldValue
      str_ret_ip_2=API_WebGUI_TextField.new("ip_start_2",nil).textField_getFieldValue
      str_ret_ip_3=API_WebGUI_TextField.new("ip_start_3",nil).textField_getFieldValue
      str_ret_ip_4=API_WebGUI_TextField.new("ip_start_4",nil).textField_getFieldValue
    
    elsif web_objvalue == 'maxaddress'
      str_ret_ip_1=API_WebGUI_TextField.new("ip_end_1",nil).textField_getFieldValue
      str_ret_ip_2=API_WebGUI_TextField.new("ip_end_2",nil).textField_getFieldValue
      str_ret_ip_3=API_WebGUI_TextField.new("ip_end_3",nil).textField_getFieldValue
      str_ret_ip_4=API_WebGUI_TextField.new("ip_end_4",nil).textField_getFieldValue
    end
    
    str_ret=str_ret_ip_1+'.'+str_ret_ip_2+'.'+str_ret_ip_3+'.'+str_ret_ip_4
        
    if hash_csv_line[:paramvalue] == str_ret
      @retstr_navigator_web= "reslut=#{str_ret} is pass"
    else
      @retstr_navigator_web= "reslut=#{str_ret} is fail"
    end
  end
  
  def check_textField(hash_csv_line,web_objvalue)
    str_ret=API_WebGUI_TextField.new(web_objvalue,nil).textField_getFieldValue
        
    if hash_csv_line[:paramvalue] == str_ret
      @retstr_navigator_web= "reslut=#{str_ret} is pass"
    else
      @retstr_navigator_web= "reslut=#{str_ret} is fail"
    end
  end
  def LANHostCfgMgt_check_SelectedItems(hash_csv_line,web_objvalue)
    arr_ret=API_WebGUI_SelectBox.new(web_objvalue,nil).selectBox_getSelectedItems
        
    if (hash_csv_line[:paramvalue] == '0') && (arr_ret.to_s.downcase == "disable")
      @retstr_navigator_web="reslut=#{arr_ret.to_s} is pass" 
    elsif (hash_csv_line[:paramvalue] == '0') &&  (arr_ret.to_s.downcase == "enable")
      @retstr_navigator_web="reslut=#{arr_ret.to_s} is faile"
    elsif (hash_csv_line[:paramvalue] == '1') && (arr_ret.to_s.downcase == "enable")
      @retstr_navigator_web="reslut=#{arr_ret.to_s} is pass"
    elsif (hash_csv_line[:paramvalue] == '1') && (arr_ret.to_s.downcase == "disable")    
      @retstr_navigator_web="reslut=#{arr_ret.to_s} is fail"
    end
  end
  def  LANHostCfgMgt_check(hash_csv_line)
    
    #--------push button(:name, "vi_net") i=1~6
    case hash_csv_line[:prefix]
      when /_VLAN(1|1_)/
        API_WebGUI_Button.new('v1_net').button_click
      when /_VLAN(2|2_)/
        API_WebGUI_Button.new('v2_net').button_click
      when /_VLAN(3|3_)/
        API_WebGUI_Button.new('v3_net').button_click
      when /_VLAN(4|4_)/
        API_WebGUI_Button.new('v4_net').button_click
      when /_VLAN(5|5_)/
        API_WebGUI_Button.new('v5_net').button_click
      when /_VLAN(6|6_)/
        API_WebGUI_Button.new('v6_net').button_click        
    end;#case hash_csv_line[:paramvalue]
    
    #--------attach new IE (:title, 'LAN Network Configuration')
    sleep 10
    API_WebGUI_Attach_ExistingIEWindow.new('LAN Network Configuration').attach_title
    
    case hash_csv_line[:paramname].downcase
      when /dhcpserverenable/
        self.LANHostCfgMgt_check_SelectedItems(hash_csv_line,"dhcp_sel")
        
      when /ipinterfaceipaddress/
        arr_ret=API_WebGUI_SelectBox.new("ip_option",nil).selectBox_getSelectedItems
        str_ret=API_WebGUI_TextField.new("ipaddr_4",nil).textField_getFieldValue
        
        @retstr_navigator_web= "reslut=#{arr_ret.to_s+'.'+str_ret} is pass"  \
          if hash_csv_line[:paramvalue] == (arr_ret.to_s+'.'+str_ret)
        @retstr_navigator_web= "reslut=#{arr_ret.to_s+'.'+str_ret} is fail" \
          if hash_csv_line[:paramvalue] != (arr_ret.to_s+'.'+str_ret)
      
      when /IPInterfaceSubnetMask/
      
      when /minaddress/
        self.LANHostCfgMgt_check_minmaxaddress(hash_csv_line,'minaddress')
      
      when /maxaddress/
        self.LANHostCfgMgt_check_minmaxaddress(hash_csv_line,'maxaddress')
      
      when /subnetmask/
        
      when /dnsservers/
        self.check_textField(hash_csv_line,"dns_1")
        
      when /domainname/
        self.check_textField(hash_csv_line,"domain_name")
        
      when /iprouters/ 
        self.check_textField(hash_csv_line,"default_router")
        
    end;#case hash_csv_line[:paramname].downcase
    
    Show_Outcomes.new(@retstr_navigator_web).log_screen
    Show_Outcomes.new(@retstr_navigator_web,@logfilepath).log_logfile 
    
    button_value="�ر�" if @str_os_lang == 'en'
    button_value='??' if @str_os_lang == 'tc'  
    #--------push button 'cancel' (:value, '??').click
    API_WebGUI_Button.new(button_value).button_value_click
    
  end

end

class Net_SSH_SFTP
  def sftp_file(*args)
    env_username = ENV['USER'] || ENV['USERNAME']
    host = args.shift
    
    if args.first.nil? || args.first.is_a?( String )
      username = args.shift || env_username
    end
    if args.first.nil? || args.first.is_a?( String )
      password = args.shift
    end
    if args.first.nil? || args.first.is_a?( String )
      srcfile_path = args.shift
    end
    if args.first.nil? || args.first.is_a?( String )
      dstfile_path = args.shift
    end
    #p host,username,password,srcfile_path,dstfile_path
    
    Net::SSH.start(host, username, password) do |session|
      session.sftp.connect do |sftp|
        sftp.put_file srcfile_path,dstfile_path
      end
    end
    
  end
  def ssh_sendcmd(*args)
    env_username = ENV['USER'] || ENV['USERNAME']
    host = args.shift
    
    if args.first.nil? || args.first.is_a?( String )
      user = args.shift || env_username
    end
    if args.first.nil? || args.first.is_a?( String )
      passwd = args.shift
    end
    if args.first.nil? || args.first.is_a?( String )
      str_cmd = args.shift
    end
    
    out=nil
    Net::SSH.start( host, user, passwd ) do |session|
      shell = session.shell.sync
      
      out = shell.send_command(str_cmd)
      
    end
    
    return out
  end

end