#$prefix_LANCfgSec_IGD=$prefix_LANCfgSec.to_s+'InternetGatewayDevice.'

class TR69_RW_param
  def _DeviceInfo
    {:prefix=>'InternetGatewayDevice.DeviceInfo.',
    "ProvisioningCode"=>"string", "Apply"=>"string"}
  end
  def _MgtSrv
    {:prefix=>'InternetGatewayDevice.ManagementServer.',
    "URL"=>"string",     "PeriodicInformEnable"=>"boolean",
    "Password"=>"string",  "PeriodicInformInterval"=>"unsignedInt", 
    "Username"=>"string",   "PeriodicInformTime"=>"dateTime",
    "ConnectionRequestUsername"=>"string",
    "ConnectionRequestPassword"=>"string",
    "UpgradesManaged"=>"boolean"}
  end  
  def _Time
    {:prefix=>'InternetGatewayDevice.Time.',
    "NTPServer1"=>"string",       "NTPServer2"=>"string",
    "NTPServer3"=>"string",       "NTPServer4"=>"string",
    "NTPServer5"=>"string",       "LocalTimeZone"=>"string",
    "LocalTimeZoneName"=>"string",
    "CurrentLocalTime"=>"dateTime",
    "DaylightSavingsUsed"=>"boolean",
    "DaylightSavingsStart"=>"dateTime",
    "DaylightSavingsEnd"=>"dateTime"}
  end
  def _UserInf
    {:prefix=>'InternetGatewayDevice.UserInterface.',
    "PasswordRequired"=>"boolean",
    "PasswordUserSelectable"=>"boolean",
    "UpgradeAvailable"=>"boolean",
    "WarrantyDate"=>"dateTime",
    "ISPName"=>"string",      "ISPHelpDesk"=>"string",
    "ISPHomePage"=>"string",  "ISPHelpPage"=>"string",
    "ISPLogo"=>"base64",      "ISPLogoSize"=>"unsignedInt",
    "ISPMailServer"=>"string", "ISPNewsServer"=>"string",
    "TextColor"=>"string",    "BackgroundColor"=>"string",
    "ButtonColor"=>"string",  "ButtonTextColor"=>"string",
    "AutoUpdateServer"=>"string", "UserUpdateServer"=>"string",
    "ExampleLogin"=>"string",
    "ExamplePassword"=>"string"}
  end
  def _LANCfgSec
    {:prefix=>'InternetGatewayDevice.LANConfigSecurity.',
    "ConfigPassword"=>"string"}
  end
  def _IPPingDiag
    {:prefix=>'InternetGatewayDevice.IPPingDiagnostics.',
    "DiagnosticsState"=>"string",
    "Interface"=>"string",
    "Host"=>"string",
    "NumberOfRepetitions"=>"unsignedInt",
    "Timeout"=>"unsignedInt",
    "DataBlockSize"=>"unsignedInt"}
  end
  def _TraceRouteDiag
    {:prefix=>'InternetGatewayDevice.TraceRouteDiagnostics.',
      "DiagnosticsState"=>"string",
      "Host"=>"string",
      "Timeout"=>"unsignedInt", 
      "DataBlockSize"=>"unsignedInt",
      "MaxHopCount"=>"unsignedInt",
      "DSCP"=>"unsignedInt"}
  end
end

class TR69_RW_param_Layer3Forward
  def initialize
    @prefix_Layer3Forward='InternetGatewayDevice.Layer3Forwarding.'
  end
  def Layer3Forward
    {:prefix  =>  @prefix_Layer3Forward,
    "DefaultConnectionService"=>"string"}
  end  
  def _Forward
    {:prefix  =>  @prefix_Layer3Forward+'Forwarding.1.',
    "Enable"=>"boolean", "Type"=>"string",
    "DestIPAddress"=>"string",
    "DestSubnetMask"=>"string",
    "SourceIPAddress"=>"string", "SourceSubnetMask"=>"string",
    "GatewayIPAddress"=>"string","Interface"=>"string",
    "ForwardingMetric"=>"int",
    "MTU"=>"unsignedInt"}
  end  
end

class TR69_RW_param_LANDev
  def initialize(vlan_idx=nil)
    @prefix_LANDev='InternetGatewayDevice.LANDevice.'
    @prefix_LANDev_vlan_idx='InternetGatewayDevice.LANDevice.'+vlan_idx.to_s+'.'
    
    @prefix_LANDev_1='InternetGatewayDevice.LANDevice.1.'
  end
  def _LANEthInfCfg
    {:prefix=>@prefix_LANDev_1.to_s+'LANEthernetInterfaceConfig.',
    :numindex=> '1',
    "Enable"=>"boolean",
    "MACAddressControlEnabled"=>"boolean",
    "MaxBitRate"=>"string","DuplexMode"=>"string"}
  end  
  def _LANUSBInfCfg
    {:prefix=>@prefix_LANDev_1.to_s+'LANUSBInterfaceConfig.',
    :numindex=> '1',
    "Enable"=>"boolean",
    "MACAddressControlEnabled"=>"boolean"}
  end
  #---------Cause Port-base VLAN
  def _LANHostCfgMgt
    {:prefix=>@prefix_LANDev_vlan_idx.to_s+'LANHostConfigManagement.',
    "DHCPServerConfigurable"=>"boolean",
    "DHCPServerEnable"=>"boolean",
    "MinAddress"=>"string",  "MaxAddress"=>"string",
    "ReservedAddresses"=>"string", "SubnetMask"=>"string",
    "DNSServers"=>"string",  "DomainName"=>"string",
    "IPRouters"=>"string", "DHCPLeaseTime"=>"int",
    "UseAllocatedWAN"=>"string",
    "AssociatedConnection"=>"string",
    "PassthroughLease"=>"unsignedInt",
    "PassthroughMACAddress"=>"string",
    "AllowedMACAddresses"=>"string", 
    "Apply"=>"string"}
  end
  def _LANHostCfgMgt_IPInf
    {:prefix=>_LANHostCfgMgt[:prefix]+'IPInterface.',
    :numindex=> '1',
    "Enable"=>"boolean",
    "IPInterfaceIPAddress"=>"string",
    "IPInterfaceSubnetMask"=>"string",
    "IPInterfaceAddressingType"=>"string", 
    "Apply"=>"string"}
  end
  #---------Cause Port-base VLAN  
  def _WLANCfg
    {:prefix  =>  @prefix_LANDev_vlan_idx.to_s+'WLANConfiguration.',
    :numindex=> '1',
    "Enable"=>"boolean",
    "MaxBitRate"=>"string",
    "Channel"=>"unsignedInt",
    "SSID"=>"string",
    "BeaconType"=>"string",
    "MACAddressControlEnabled"=>"boolean",
    "WEPKeyIndex"=>"unsignedInt",
    "KeyPassphrase"=>"string",
    "BasicEncryptionModes"=>"string",
    "BasicAuthenticationMode"=>"string",
    "WPAEncryptionModes"=>"string",
    "WPAAuthenticationMode"=>"string",
    "IEEE11iEncryptionModes"=>"string",
    "IEEE11iAuthenticationModes"=>"string",
    "BasicDataTransmitRates"=>"string",
    "OperationalDataTransmitRates"=>"string",
    "PossibleDataTransmitRates"=>"string",
    "BeaconAdvertisementEnabled"=>"boolean",
    "RadioEnabled"=>"boolean",
    "AutoRateFallBackEnabled"=>"boolean",     
    "LocationDescription"=>"string",
    "InsecureOOBAccessEnabled"=>"boolean",
    "RegulatoryDomain"=>"string",
    "DeviceOperationMode"=>"string",
    "DistanceFromRoot"=>"unsignedInt",
    "PeerBSSID"=>"string",
    "AuthenticationServiceMode"=>"string", 
    "Apply"=>"string"}
  end  
  def _WLANCfg_WEPKey
    {:prefix  =>  _WLANCfg[:prefix].to_s+'1.WEPKey.',
    :numindex=> '1',
    "WEPKey"=>"string"}
  end  
  def _WLANCfg_PreSharedKey
    {:prefix  =>  _WLANCfg[:prefix].to_s+'1.PreSharedKey.',
    :numindex=> '1',
    "PreSharedKey"=>"string",
    "KeyPassphrase"=>"string",
    "AssociatedDeviceMACAddress"=>"string"}
  end  
end

class TR69_RW_param_WANDev
  def initialize
    @prefix_WANDev='InternetGatewayDevice.WANDevice.1.'
  end
  def _WANCommInfCfg
    {:prefix  => @prefix_WANDev.to_s+'WANCommonInterfaceConfig.',
    "EnabledForInternet"=>"boolean"}
  end  
  def _WANEthInfCfg
    {:prefix  => @prefix_WANDev.to_s+'WANEthernetInterfaceConfig.',
    "Enable"=>"boolean","MaxBitRate"=>"unsignedInt",
    "DuplexMode"=>"string"}
  end  
  def _WANDSLInfCfg
    {:prefix=>@prefix_WANDev.to_s+'WANDSLInterfaceConfig.',
    "Enable"=>"boolean"}
  end  
  def _WANDSLDiag
    {:prefix  =>  @prefix_WANDev.to_s+'WANDSLDiagnostics.',
    "LoopDiagnosticsState"=>"string"}
  end  
end

class TR69_RW_param_WANDev_WANConDev
  def initialize
    prefix_WANDev='InternetGatewayDevice.WANDevice.1.'
    @prefix_WANDev_WANConDev=prefix_WANDev.to_s+'WANConnectionDevice.'
  end
  def _WANDSLLinkCfg
    {:prefix  => @prefix_WANDev_WANConDev.to_s+'1.WANDSLLinkConfig.',
    "Enable"=>"boolean","LinkType"=>"string",
    "DestinationAddress"=>"string",
    "ATMEncapsulation"=>"string",  "FCSPreserved"=>"boolean",
    "VCSearchList"=>"string",  "ATMReceivedBlocks"=>"string",
    "ATMPeakCellRate"=>"unsignedInt",
    "ATMMaximumBurstSize"=>"unsignedInt",
    "ATMSustainableCellRate"=>"unsignedInt"}
  end  
  def _WANATMF5LoopbackDiag
    {:prefix  => @prefix_WANDev_WANConDev.to_s+'1.WANATMF5LoopbackDiagnostics.',
    "DiagnosticsState"=>"string",  "NumberOfRepetitions"=>"unsignedInt",
    "Timeout"=>"unsignedInt"}
  end  
  def _WANPOSTLinkCfg
    {:prefix  => @prefix_WANDev_WANConDev.to_s+'1.WANPOTSLinkConfig.',
    "Enable"=>"boolean",
    "ISPPhoneNumber"=>"string",
    "ISPInfo"=>"string",  "LinkType"=>"string",
    "NumberOfRetries"=>"unsignedInt",
    "DelayBetweenRetries"=>"unsignedInt",}
  end
  def _WANIPConn
    {:prefix  => @prefix_WANDev_WANConDev.to_s+'1.WANIPConnection.',
    :numindex=> '1',
    "Enable"=>"boolean",
    "ConnectionType"=>"string",
    "Name"=>"string",
    "AutoDisconnectTime"=>"unsignedInt",
    "NATEnabled"=>"boolean",
    "AddressingType"=>"string",
    "ExternalIPAddress"=>"string",
    "SubnetMask"=>"string",  "DefaultGateway"=>"string",
    "DNSEnabled"=>"boolean", "DNSOverrideAllowed"=>"boolean",
    "DNSServers"=>"string",  "MaxMTUSize"=>"unsignedInt",
    "MACAddress"=>"string",  "MACAddressOverride"=>"boolean",
    "ConnectionTrigger"=>"string",
    "RouteProtocolRx"=>"string"}
  end  
  def _WANIPConn_PortMap
    {:prefix  => _WANIPConn[:prefix]+'PortMapping.',
    :numindex=> '1',
    "PortMappingLeaseDuration"=>"unsignedInt", 
    "RemoteHost"=>"string",
    "ExternalPort"=>"unsignedInt", "InternalPort"=>"unsignedInt",
    "PortMappingProtocol"=>"string",
    "InternalClient"=>"string",
    "PortMappingDescription"=>"string",
    "PortMappingEnabled"=>"boolean"}
  end  
  def _WANPPPConn
    {:prefix  => @prefix_WANDev_WANConDev.to_s+'WANPPPConnection.',
    :numindex=> '1',
    "Enable"=>"boolean",
    "ConnectionType"=>"string",
    "Name"=>"string",
    "AutoDisconnectTime"=>"unsignedInt",
    "NATEnabled"=>"boolean",
    "IdleDisconnectTime"=>"unsignedInt",
    "Username"=>"string",
    "Password"=>"string",
    "CurrentMRUSize"=>"unsignedInt",
    "DNSEnabled"=>"boolean",
    "DNSOverrideAllowed"=>"boolean",
    "DNSServers"=>"string",
    "MACAddress"=>"string",
    "MACAddressOverride"=>"boolean",
    "PPPoEACName"=>"string",
    "PPPoEServiceName"=>"string",
    "ConnectionTrigger"=>"string",
    "RouteProtocolRx"=>"string",
    "MaxMRUSize"=>"unsignedInt",
    "WarnDisconnectDelay"=>"unsignedInt"}
  end  
  def _WANPPPConn_PortMap
    {:prefix  => _WANPPPConn[:prefix]+'PortMapping.',
    :numindex=> '1',
    "PortMappingEnabled"=>"boolean",
    "PortMappingLeaseDuration"=>"unsignedInt",
    "RemoteHost"=>"string",
    "ExternalPort"=>"unsignedInt",
    "InternalPort"=>"unsignedInt",
    "PortMappingProtocol"=>"string",
    "InternalClient"=>"string",
    "PortMappingDescription"=>"string"}
  end  
end

class TR104_RW_param_VocProfile
  def initialize
    @prefix_VocProfile='InternetGatewayDevice.Services.VoiceService.1.VoiceProfile.1.'
    @prefix_VocProfile_Tone =@prefix_VocProfile.to_s+'Tone.'
    @prefix_VocProfile_Line =@prefix_VocProfile.to_s+'Line.'
    @prefix_VocProfile_ButtMap =@prefix_VocProfile.to_s+'ButtonMap.'
  end
  
  def VocProfile
    {:prefix=>@prefix_VocProfile,
    "Enable"=>"boolean",
     "Reset"=>"boolean",    "Name"=>"string",
     "SignalingProtocol"=>"string",
     "MaxSessions"=>"unsignedInt",
     "DTMFMethod"=>"string",
     "DTMFMethodG711"=>"string",
     "DigitMap"=>"string",  "DigitMapEnable"=>"boolean",
     "FaxPassThrough"=>"string",
     "ModemPassThrough"=>"boolean"}
  end
  
  def _SrvPrdInfo
    {:prefix=>@prefix_VocProfile.to_s+'ServiceProviderInfo.',
    "Name"=>"string",   "URL"=>"string",
     "ContactPhoneNumber"=>"string",
     "EmailAddress"=>"string"}
  end

  def _SIP
    {:prefix=>@prefix_VocProfile.to_s+'SIP.',
    "ProxyServer"=>"string",
    "ProxyServerPort"=>"unsignedInt",
    "RegistrarServer"=>"string",
    "RegistrarServerPort"=>"unsignedInt",
    "UserAgentPort"=>"unsignedInt",
    "UserAgentTransport"=>"string",
    "RegistrationPeriod"=>"unsignedInt",
    "TimerT1"=>"unsignedInt",
    "TimerT2"=>"unsignedInt",
    "TimerT4"=>"unsignedInt",
    "TimerA"=>"unsignedInt",
    "TimerB"=>"unsignedInt",
    "TimerC"=>"unsignedInt",
    "TimerD"=>"unsignedInt",
    "TimerE"=>"unsignedInt",
    "TimerF"=>"unsignedInt",
    "TimerG"=>"unsignedInt",
    "TimerH"=>"unsignedInt",
    "TimerI"=>"unsignedInt",
    "TimerJ"=>"unsignedInt",
    "TimerK"=>"unsignedInt",
    "RegisterExpires"=>"unsignedInt",
    "RegisterRetryInterval"=>"unsignedInt",
    "InboundAuth"=>"string",
    "InboundAuthUsername"=>"string",
    "InboundAuthPassword"=>"string",
    "UseCodecPriorityInSDPResponse"=>"boolean",
    "DSCMark"=>"unsignedInt",
    "VLANIDMark"=>"int",
    "EthernetPriorityMark"=>"int"}
  end

  def _SIP_EvtSubscrb
    {:prefix => _SIP[:prefix]+'EventSubscribe.',
    :numindex=> '1',
    "Event"=>"string",  "NotifierPort"=>"unsignedInt",
    "Notifier"=>"string",
    "NotifierTransport"=>"string",
    "ExpireTime"=>"unsignedInt"}
  end

  def _SIP_RespMap
    {:prefix=> _SIP[:prefix]+'ResponseMap.',
    :numindex=> '1',
    "SIPResponseNumber"=>"unsignedInt", "TextMessage"=>"string",
    "Tone"=>"unsignedInt"}
  end
  def _MGCP
    {:prefix=>@prefix_VocProfile.to_s+'MGCP.',
    "CallAgent1"=>"string",  "CallAgentPort1"=>"unsignedInt",
    "CallAgent2"=>"string",  "CallAgentPort2"=>"unsignedInt",
    "RetranIntervalTimer"=>"unsignedInt",
    "MaxRetranCount"=>"unsignedInt",
    "RegisterMode"=>"string",
    "LocalPort"=>"unsignedInt",
    "Domain"=>"string",
    "User"=>"string",
    "DSCPMark"=>"unsignedInt", "VLANIDMark"=>"int",
    "EthernetPriorityMark"=>"int", 
    "AllowPiggybackEvents"=>"boolean",
    "SendRSIPImmediately"=>"boolean"}
  end
  def _H323
    {:prefix =>@prefix_VocProfile.to_s+'H323.',
    "Gatekeeper"=>"string",  "GatekeeperPort"=>"unsignedInt",
    "GatekeeperID"=>"string", "TimeToLive"=>"unsignedInt",
    "H235Authentication"=>"boolean", "AuthPassword"=>"string",
    "SendersID"=>"string", "DSCPMark"=>"unsignedInt",
    "VLANIDMark"=>"int",
    "EthernetPriorityMark"=>"int"}
  end
  def _RTP
    {:prefix =>@prefix_VocProfile.to_s+'RTP.',
    "LocalPortMin"=>"unsignedInt",
    "LocalPortMax"=>"unsignedInt",
    "DSCPMark"=>"unsignedInt",
    "VLANIDMark"=>"int",
    "EthernetPriorityMark"=>"int",
    "TelephoneEventPayloadType"=>"unsignedInt"}
  end  
  def _RTP_RTCP
    {:prefix =>_RTP[:prefix]+'RTCP.',
    "Enable"=>"boolean", "TxRepeatInterval"=>"unsignedInt",
    "LocalCName"=>"string"}
  end
  def _RTP_SRTP
    {:prefix =>_RTP[:prefix]+'SRTP.',
    "Enable"=>"boolean", "KeyingMethods"=>"string",
    "EncryptionKeySizes"=>"string"}
  end
  def _RTP_Redcy
    {:prefix=>_RTP[:prefix]+'Redundancy.',
    "Enable"=>"boolean", "PayloadType"=>"unsignedInt",
    "FaxAndModemRedundancy"=>"int",  "ModemRedundancy"=>"int",
    "DTMFRedundancy"=>"int", "VoiceRedundancy"=>"int",
    "MaxSessionsUsingRedundancy"=>"unsignedInt"}
  end
  def _NumPlan
    {"prefix"=>@prefix_VocProfile.to_s+'NumberingPlan.',
    "MinimumNumberOfDigits"=>"unsignedInt", 
    "MaximumNumberOfDigits"=>"unsignedInt",
    "InterDigitTimerStd"=>"unsignedInt",
    "InterDigitTimerOpen"=>"unsignedInt",
    "InvalidNumberTone"=>"unsignedInt"}
  end
  def _NumPlan_PrefixInfo
    {:prefix=>_NumPlan[:prefix]+'PrefixInfo.',
    :numindex=> '1',
    "PrefixRange"=>"string",
    "PrefixMinNumberOfDigits"=>"unsignedInt",
    "PrefixMaxNumberOfDigits"=>"unsignedInt",
    "NumberOfDigitsToRemove"=>"unsignedInt",
    "PosOfDigitsToRemove"=>"unsignedInt",
    "DialTone"=>"unsignedInt",
    "FacilityAction"=>"string",
    "FacilityActionArgument"=>"string"}
  end
  
  def _Tone_Event
    {:prefix =>@prefix_VocProfile_Tone.to_s+'Event.',
    :numindex=> '1',
    "ToneID"=>"unsignedInt"}
  end
  def _Tone_Descrp
    {:prefix=>@prefix_VocProfile_Tone.to_s+'Description.',
    :numindex=> '1',
    "ToneEnable"=>"boolean", "ToneName"=>"string",
    "TonePattern"=>"unsignedInt",  "ToneFile"=>"string",
    "ToneRepetitions"=>"unsignedInt",
    "ToneText"=>"string"}
  end
  def _Tone_Patt
    {:prefix =>@prefix_VocProfile_Tone.to_s+'Pattern.',
    :numindex=> '1',
    "EntryID"=>"unsignedInt",   "ToneOn"=>"boolean",
    "Frequency1"=>"unsignedInt", "Power1"=>"int",
    "Frequency2"=>"unsignedInt", "Power2"=>"int",
    "Frequency3"=>"unsignedInt", "Power3"=>"int",
    "Frequency4"=>"unsignedInt", "Power4"=>"int",
    "ModulationFrequency"=>"unsignedInt",
    "ModulationPower"=>"int",
    "Duration"=>"unsignedInt",
    "NextEntryID"=>"unsignedInt"}
  end
  def _ButtMap_Butt
    {:prefix=>@prefix_VocProfile_ButtMap.to_s+'Button.',
    :numindex=> '1',
    "FacilityAction"=>"string",
    "FacilityActionArgument"=>"string",
    "QuickDialNumber"=>"string", "ButtonMessage"=>"string",
    "UserAccess"=>"boolean"}
  end  
  def _FaxT38
    {:prefix =>@prefix_VocProfile.to_s+'FaxT38.',
    "Enable"=>"boolean", "BitRate"=>"unsignedInt",
    "HighSpeedPacketRate"=>"unsignedInt",
    "HighSpeedRedundancy"=>"unsignedInt",
    "LowSpeedRedundancy"=>"unsignedInt",
    "TCFMethod"=>"string"}
  end  
  def _Line
    {:prefix=>@prefix_VocProfile_Line,
    :numindex=> '1',
    "Enable"=>"string",  "DirectoryNumber"=>"string",
    "PhyReferenceList"=>"string",  "SIP.AuthUserName"=>"string",
    "SIP.AuthPassword"=>"string",
    "SIP.URI"=>"string"}
  end
  def _Line_SIP_EventSubscrb(handle_update)
    {:prefix=>handle_update[:prefix]+handle_update[:numindex]\
              +'.'+'SIP.EventSubscribe.',
    :numindex=> '1',
    "AuthUserName"=>"string",  "AuthPassword"=>"string"}
  end  
  def _Line_MGCP(handle_update)
    {:prefix=>handle_update[:prefix]+handle_update[:numindex]\
              +'.'+'MGCP.',
    :numindex=> '1',
    "LineName"=>"string"}
  end  
  def _Line_H323(handle_update)
    {:prefix=>handle_update[:prefix]+handle_update[:numindex]\
              +'.'+'H323.',
    :numindex=> '1',
    "H323ID"=>"string"}
  end
  def _Line_Ringer_Event(handle_update)
    {:prefix=>handle_update[:prefix]+handle_update[:numindex]\
              +'.'+"Ringer.Event.",
    :numindex=> '1',
    "RingID"=>"unsignedInt"}
  end  
  def _Line_Ringer_Descrp(handle_update)
    {:prefix=>handle_update[:prefix]+handle_update[:numindex]\
              +'.'+'Ringer.Description.',
    :numindex=> '1',
    "RingEnable"=>"boolean", "RingName"=>"string",
    "RingPattern"=>"unsignedInt",
    "RingFile"=>"string"}
  end  
  def _Line_Ringer_Patt(handle_update)
    {:prefix=>handle_update[:prefix]+handle_update[:numindex]\
              +'.'+'Ringer.Pattern.',
    :numindex=> '1',
    "EntryID"=>"unsignedInt",  "RingerOn"=>"boolean",
    "Duration"=>"unsignedInt",
    "NextEntryID"=>"unsignedInt"}
  end  
  def _Line_CallingFeatures(handle_update)
    {:prefix=>handle_update[:prefix]+handle_update[:numindex]\
              +'.'+'CallingFeatures.',
    :numindex=> '1',
    "CallerIDEnable"=>"boolean",
    "CallerIDNameEnable"=>"boolean",
    "CallerIDName"=>"string",
    "CallWaitingEnable"=>"boolean",
    "MaxSessions"=>"unsignedInt",
    "CallForwardUnconditionalEnable"=>"boolean",
    "CallForwardUnconditionalNumber"=>"string",
    "CallForwardOnBusyEnable"=>"boolean",
    "CallForwardOnBusyNumber"=>"string",
    "CallForwardOnNoAnswerEnable"=>"boolean",
    "CallForwardOnNoAnswerNumber"=>"string",
    "CallForwardOnNoAnswerRingCount"=>"unsignedInt",
    "CallTransferEnable"=>"boolean",
    "MWIEnable"=>"boolean",
    "AnonymousCallBlockEnable"=>"boolean",
    "AnonymousCalEnable"=>"boolean",
    "DoNotDisturbEnable"=>"boolean",
    "CallReturnEnable"=>"boolean",
    "RepeatDialEnable"=>"boolean"}
  end  
  def _Line_VocProcess(handle_update)
    {:prefix=>handle_update[:prefix]+handle_update[:numindex]\
              +'.'+'VoiceProcessing.',
    "TransmitGain"=>"int","ReceiveGain"=>"int",
    "EchoCancellationEnable"=>"boolean"}
  end  
  def _Line_Codec_List(handle_update)
    {:prefix=>handle_update[:prefix]+handle_update[:numindex]\
              +'.'+'Codec.List.',
    :numindex=> '1',
    "PacketizationPeriod"=>"string",
    "SilenceSuppression"=>"boolean",
    "Enable"=>"boolean", "Priority"=>"unsignedInt"}
  end  
  def _Line_Stats(handle_update)
    {:prefix=>handle_update[:prefix]+handle_update[:numindex]\
              +'.'+'Stats',
    :numindex=> '1',          
    "ResetStatistics"=>"boolean"}
  end  
  def _PhyInf_Tests
    {:prefix =>@prefix_VocProfile.to_s+'PhyInterface.1.Tests.',
    "TestState"=>"string", "TestSelector"=>"string"}
  end
end
