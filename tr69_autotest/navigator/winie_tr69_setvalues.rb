#-------------------------------------------------------------------------------------------------------------#
# SetParameterValues test for TR069 Test on Windows XP using IE
# 
# Purpose: For TR069 test by using library Net::SSH to remote login Linux.                   
# Dec18/07 Initialize by Philip Shen                        
# Jan02/08 Modify it from test_ssh_sftp.rb
#------------------------------------------------------------------------------------------------------------#

path_tr69_autotest=File.dirname(File.dirname(File.expand_path(__FILE__)))
$LOAD_PATH.unshift File.join(path_tr69_autotest, 'lib') if $0 == __FILE__

require 'API_TR69'   # the TR069 Test API
require "faster_csv"
require 'net/ssh'
require 'net/sftp'
require 'WebGUI_Lib/API_WebGUI'   # the Web GUI Test API   
require "timeoutx"

class Win_IE_Navgator_TR069
  Path_tr69_autotest=File.dirname(File.dirname(File.expand_path(__FILE__)))
  
  def initialize
    @path_TR069_QAGemtek=File.dirname(Path_tr69_autotest)
    
    @dir_Log = File.join(@path_TR069_QAGemtek,'log')
    @logfilename=File.basename($0,'.*')+".txt"
    @logfilepath = File.join(@dir_Log,@logfilename)
    
    @retstr_navigator_web=String.new
    
    @str_os_lang='en';#'tc'
  end

  def setWAN0_StaticIP_Mask_GW(hash_inifile)
    cpe_lan_ip=hash_inifile[:CPE_Host_LAN_IP]
    cpe_username=hash_inifile[:CPE_Username]
    cpe_password=hash_inifile[:CPE_Passwd]
    cpe_wan_ip=hash_inifile[:CPE_Host_WAN_IP]
    
    retcode=Check_DUT_httpd_alive.new(cpe_lan_ip,@logfilepath).check_alive
    
    case retcode
      when "200"
        p "Return Value =#{retcode}. Webpage login."

      when /40(0|1)/
        #p "Return Value =#{retcode}. Alter Window login."
        Browser_IE_Initial.new(nil).start_ie_with_logger()
        
        hbrowser=Browser_IE_Initial.new(cpe_lan_ip,cpe_username,cpe_password)
                
        str_url='cfg_interface_quick_setup.htm'
        hbrowser.goto_webpag(@str_os_lang,str_url)
        
        #--------Goto WAN0 DHCP page
        str_result=" Goto webpage WAN0 DHCP"
        Show_Outcomes.new(str_result).show_results()
        hWin_Help=WindowHelper.new
        hWin_Help.Navigator_WAN0_DHCPPage
        
        str_status=Browser_IE_Status.new.browser_status
        
        stropt=String.new
        @str_os_lang=='en'? str_winie_status='Done':str_winie_status='完成'
        
        #--------Check Navigator if default setting
        if str_status.length == 0
          stropt=''
        elsif str_status.length > 0
          #--------Check page select status
          while str_status != str_winie_status
            sleep 1
            str_status=Browser_IE_Status.new.browser_status
          end
          
          stropt='afterDHCPpage'
        end
        
        #--------Goto WAN0 Static IP page after trigger DHCP page 
        str_result=" Goto webpage WAN0 Static IP:"+cpe_wan_ip
        Show_Outcomes.new(str_result).show_results()
        hWin_Help.Navigator_WAN0_StaticPage(stropt)
        
        $ie.wait()
        str_status=Browser_IE_Status.new.browser_status
        #--------Check page select status
        while str_status != str_winie_status
          sleep 1
          str_status=Browser_IE_Status.new.browser_status
        end
        
        #--------keyin wan_ip; wan_ip_mask; wan_ip_gw
        Navigator_Gemtek.new.WAN0_StaticIP_Mask_GW(hash_inifile)
        API_WebGUI_Button.new('but_save').button_click
        
        
        $ie.wait()
        str_status=Browser_IE_Status.new.browser_status
        
        timer=1
        while str_status == 'Error on page.'
          break if timer > 4
          sleep 3
          str_status=Browser_IE_Status.new.browser_status
          timer += 1
        end
        
        hbrowser.teardown()
        
        Browser_IE_Close.new.end_ie()
    
      else
        print 'Unknow Login methode.'
        return
        
    end;#case retcode
    
  end
  
  def setTR069(hash_inifile)
    cpe_lan_ip=hash_inifile[:CPE_Host_LAN_IP]
    cpe_username=hash_inifile[:CPE_Username]
    cpe_password=hash_inifile[:CPE_Passwd]
    
    retcode='000'
    status = TimeoutX::timeout(200) {
      # Something that should be interrupted if it takes too much time...
      begin_time = Time.now
      while retcode != '401'
        sleep 10
        retcode=Check_DUT_httpd_alive.new(cpe_lan_ip,@logfilepath).check_alive
      end
      
      elapsed_time = Time.now - begin_time
      Show_Outcomes.new("Navigator After Reset Startup Duration=#{elapsed_time} sec.").log_screen
      Show_Outcomes.new("Navigator After Reset Startup Duration=#{elapsed_time} sec.",@logfilepath).log_logfile  
    }
    
    case retcode
      when "200"
        p "Return Value =#{retcode}. Webpage login."

      when /40(0|1)/
        #p "Return Value =#{retcode}. Alter Window login."
        Browser_IE_Initial.new(nil).start_ie_with_logger()
        
        hbrowser=Browser_IE_Initial.new(cpe_lan_ip,cpe_username,cpe_password)
                
        #--------Goto cfg TR069 page
        str_result=" Goto TR069 configuration page."
        Show_Outcomes.new(str_result).show_results()
        
        str_url="cfg_tasks_TR-069.htm"
        hbrowser.goto_webpag(@str_os_lang,str_url.to_s)
        
        #--------Config TR069 page
        Navigator_Gemtek.new.TR069(hash_inifile)
        
        $ie.wait()
        str_status=Browser_IE_Status.new.browser_status
        
        timer=1
        @str_os_lang=='en'? str_winie_status='Done':str_winie_status='完成'
        while str_status != str_winie_status
          break if timer > 4
          sleep 3
          str_status=Browser_IE_Status.new.browser_status
          timer += 1
        end
        
        hbrowser.teardown()
        
        Browser_IE_Close.new.end_ie()
    
      else
        print 'Unknow Login methode.'
        return
        
    end;#case retcode
    
  end
  
  def setVLAN(hash_inifile)
    cpe_lan_ip=hash_inifile[:CPE_Host_LAN_IP]
    cpe_username=hash_inifile[:CPE_Username]
    cpe_password=hash_inifile[:CPE_Passwd]
    
    retcode=Check_DUT_httpd_alive.new(cpe_lan_ip,@logfilepath).check_alive
    arr_vlanid=%w(0 0 2233 401 4069 69 3322)
    arr_pt_vlanid=%w(0 2 1 2 3 4 5 6 1 3 )
    arr_temp=[]
    case retcode
      when "200"
        p "Return Value =#{retcode}. Webpage login."

      when /40(0|1)/
        Browser_IE_Initial.new(nil).start_ie_with_logger()
        
        hbrowser=Browser_IE_Initial.new(cpe_lan_ip,cpe_username,cpe_password)
                
        hbrowser.goto_webpag(@str_os_lang,'cfg_interface_LAN_setVLAN.htm')
        
        2.upto(6){|i|
          API_WebGUI_TextField.new("v#{i}_vid", arr_vlanid[i]).textField_setFieldValue  
        }
        
        #--------Set VLAN1~6 in each LAN side port
        1.upto(9){|x|
          
          objname="p#{x}v#{arr_pt_vlanid[x]}"
          handle_chkbox=API_WebGUI_CheckBox.new(objname)
          
          handle_chkbox.checkbox_set if handle_chkbox.checkbox_enabled
        }
        
        #--------Set SSID1~4 
        1.upto(4){|i|
          objname="w#{i}v#{i}"
          handle_chkbox=API_WebGUI_CheckBox.new(objname)
          
          handle_chkbox.checkbox_set if handle_chkbox.checkbox_enabled  
        }
        
        API_WebGUI_Button.new('but_save').button_click
        
        $ie.wait()
        str_status=Browser_IE_Status.new.browser_status
        
        timer = 1
        while str_status == 'Error on page.'
          #str_result=" Timer= #{timer}."
          #Show_Outcomes.new(str_result).show_results
          break if timer > 6
          sleep 3
          str_status=Browser_IE_Status.new.browser_status
          timer += 1
        end
        
        hbrowser.teardown()
        Browser_IE_Close.new.end_ie()
    
      else
        print 'Unknow Login methode.'
        return
        
    end;#case retcode
    
  end
  
  def resetdefaults_reboot(str_url,hash_inifile)
    cpe_lan_ip=hash_inifile[:CPE_Host_LAN_IP]
    cpe_username=hash_inifile[:CPE_Username]
    cpe_password=hash_inifile[:CPE_Passwd]
    
    retcode='000'
    
    status = TimeoutX::timeout(200) {
      # Something that should be interrupted if it takes too much time...
      begin_time = Time.now
      while retcode != '401'
        sleep 10
        retcode=Check_DUT_httpd_alive.new(cpe_lan_ip,@logfilepath).check_alive
      end
      
      elapsed_time = Time.now - begin_time
      Show_Outcomes.new("Navigator Startup Duration=#{elapsed_time} sec.").log_screen
      Show_Outcomes.new("Navigator Startup Duration=#{elapsed_time} sec.",@logfilepath).log_logfile  
    }
    
    case retcode
      when "200"
        p "Return Value =#{retcode}. Webpage login."

      when /40(0|1)/
        Browser_IE_Initial.new(nil).start_ie_with_logger()
        
        hbrowser=Browser_IE_Initial.new(cpe_lan_ip,cpe_username,cpe_password)
        
        if str_url == "admin_reset_def.htm"
          #--------Goto cfg Reset Default page
          str_result=" Goto Reset Default page."  
        elsif str_url == "admin_reboot.htm"
          #--------Goto cfg Reboot page
          str_result=" Goto Reboot page."  
        end
        Show_Outcomes.new(str_result).show_results()
        
        hbrowser.goto_webpag(@str_os_lang,str_url.to_s)
        #------ Below invaild ------#
        #startClicker('OK')
        #$ie.button(:value, "  恢复出厂默?值  ").click
        #------ Above invaild ------#
        
        #------ PopUp Click 'OK' ------#
        $ie.send_keys('{Tab}{Tab}{Enter}{Enter}')
        
        $ie.wait()
        str_status=Browser_IE_Status.new.browser_status
        
        timer=1
        @str_os_lang=='en'? str_winie_status='Done':str_winie_status='完成'
        while str_status != str_winie_status
          break if timer > 4
          sleep 3
          str_status=Browser_IE_Status.new.browser_status
          timer += 1
        end
        
        hbrowser.teardown()
        
        Browser_IE_Close.new.end_ie()
    
      else
        print 'Unknow Login methode.'
        return
        
    end;#case retcode
    
  end

  def setWLAN_basic(hash_inifile)
    cpe_lan_ip=hash_inifile[:CPE_Host_LAN_IP]
    cpe_username=hash_inifile[:CPE_Username]
    cpe_password=hash_inifile[:CPE_Passwd]
    
    retcode=Check_DUT_httpd_alive.new(cpe_lan_ip,@logfilepath).check_alive
    
    case retcode
      when "200"
        p "Return Value =#{retcode}. Webpage login."

      when /40(0|1)/
        Browser_IE_Initial.new(nil).start_ie_with_logger()
        
        hbrowser=Browser_IE_Initial.new(cpe_lan_ip,cpe_username,cpe_password)
                
        #--------Goto cfg WLAN basic page
        str_result=" Goto wireless_basic configuration page."
        Show_Outcomes.new(str_result).show_results()
        
        str_url="cfg_wireless_basic.htm"
        hbrowser.goto_webpag(@str_os_lang,str_url.to_s)
        
        #--------Set SSID1~4
        n=3
        0.upto(n) {|i|
          API_WebGUI_TextField.new("wl_ssid#{i}","Nav_ssid#{i+1}").textField_setFieldValue
        }
        
        API_WebGUI_Button.new('but_save').button_click
        
        $ie.wait()
        str_status=Browser_IE_Status.new.browser_status
        
        timer=1
        @str_os_lang=='en'? str_winie_status='Done':str_winie_status='完成'
        while str_status != str_winie_status
          break if timer > 4
          sleep 3
          str_status=Browser_IE_Status.new.browser_status
          timer += 1
        end
             
        hbrowser.teardown()
        
        Browser_IE_Close.new.end_ie()
    
      else
        print 'Unknow Login methode.'
        return
        
    end;#case retcode
    
  end
  
  def TR069_LANHostCfgMgt(hash_csv_line,hash_inifile)
    cpe_lan_ip=hash_inifile[:CPE_Host_LAN_IP]
    cpe_username=hash_inifile[:CPE_Username]
    cpe_password=hash_inifile[:CPE_Passwd]
    
    retcode='000'
    status = TimeoutX::timeout(200) {
      # Something that should be interrupted if it takes too much time...
      begin_time = Time.now
      while retcode != '401'
        sleep 10
        retcode=Check_DUT_httpd_alive.new(cpe_lan_ip,@logfilepath).check_alive
      end
      
      elapsed_time = Time.now - begin_time
      Show_Outcomes.new("ACS Server Setting Duration=#{elapsed_time} sec.").log_screen
      Show_Outcomes.new("ACS Server Setting Duration=#{elapsed_time} sec.",@logfilepath).log_logfile  
    }
    
    case retcode
      when "200"
        p "Return Value =#{retcode}. Webpage login."

      when /40(0|1)/
        #p "Return Value =#{retcode}. Alter Window login."
        Browser_IE_Initial.new(nil).start_ie_with_logger()
        
        hbrowser=Browser_IE_Initial.new(cpe_lan_ip,cpe_username,cpe_password)
                
        hbrowser.goto_webpag(@str_os_lang,'cfg_interface_LAN_setVLAN.htm')
        
        #--------go to next new IE
        $ie.wait()
        
        handle_Nav_TR069=Navigator_TR069.new(@logfilepath,@str_os_lang)
        handle_Nav_TR069.LANHostCfgMgt_check(hash_csv_line)
        
        #--------attach new IE (:title, 'LAN Wizard')
        API_WebGUI_Attach_ExistingIEWindow.new('LAN Wizard').attach_title
        
        $ie.wait()
        
        hbrowser.teardown()
        Browser_IE_Close.new.end_ie()
    
      else
        print 'Unknow Login methode.'
        return
        
    end;#case retcode
    
  end
  
  def all_websetting(hash_inifile)
    #--------Open M$IE using watir to set DUT value.
    n = 1;#50
    times=1
    1.upto(n) do
      strtemp="Power Reset Cycle Test #{times} time(s) total #{n} time(s)."
      p strtemp
      Show_Outcomes.new(" ",@logfilepath).log_logfile
      Show_Outcomes.new(strtemp,@logfilepath).log_logfile
      
      self.resetdefaults_reboot("admin_reset_def.htm",hash_inifile)
      self.setTR069(hash_inifile)
      self.setWLAN_basic(hash_inifile)
      self.setWAN0_StaticIP_Mask_GW(hash_inifile)
      self.setVLAN(hash_inifile)
      
      times += 1
      sleep 10
    end
    
    strtemp="End Power Reset Cycle Test #{times-1} time(s) total #{n} time(s)."
    p strtemp
    Show_Outcomes.new(" ",@logfilepath).log_logfile
    Show_Outcomes.new(strtemp,@logfilepath).log_logfile
      
  end
end
class Win_IE_SetParamterValues
#**************************************************************************#
# 1.Red INI file to get environmental info.
#**************************************************************************#
  Path_tr69_autotest=File.dirname(File.dirname(File.expand_path(__FILE__)))
  Dir_autotest_test=File.join(Path_tr69_autotest,'test')
  Dir_autotest_navigator=File.join(Path_tr69_autotest,'navigator')
  Dir_autotest_dest=Dir_autotest_navigator
  
  attr_reader :logfilepath
  
  def initialize(inifile)
    @inifile_env=inifile
    @path_TR069_QAGemtek=File.dirname(Path_tr69_autotest)
    
    @dir_Log = File.join(@path_TR069_QAGemtek,'log')
    @logfilename=File.basename($0,'.*')+".txt"
    @logfilepath = File.join(@dir_Log,@logfilename)
    
    @retstr_navigator_web=String.new
    
    path_tr69autotest_navigator="/temp/TR069_QAGemtek/tr69_autotest/navigator"
    path_tr69autotest_test="/temp/TR069_QAGemtek/tr69_autotest/test"
    @path_tr69autotest_destination=path_tr69autotest_navigator
    
    @path_ruby="/usr/local/bin/ruby"
    @str_ruby_setvalue="web_tr69_setvalues.rb"
  end

  
  #**************************************************************************#
  # Adapte from sync-shell-demo.rb
  #**************************************************************************#
  def sshlogin_sftp(inifile,hash_inifile)
    host=hash_inifile[:ACS_IP]
    user=hash_inifile[:SSH_Username]
    passwd=hash_inifile[:SSH_Password]
    
    #----sftp testenv.ini from M$ to Linux
    strtemp="putting local file #{inifile} to remote host #{host}..."
    puts strtemp
    Show_Outcomes.new(strtemp,@logfilepath).log_logfile  
    Net_SSH_SFTP.new.sftp_file(host, user, passwd,inifile, \
                      File.join(@path_tr69autotest_destination,inifile))
    
    #----sftp testcases_setvalues.csv from M$ to Linux
    strtemp="putting local file #{hash_inifile[:CSV_SetValues]} to remote host #{host}..."
    puts strtemp
    Show_Outcomes.new(strtemp,@logfilepath).log_logfile  
    Net_SSH_SFTP.new.sftp_file(host, user, passwd,hash_inifile[:CSV_SetValues], \
                File.join(@path_tr69autotest_destination,hash_inifile[:CSV_SetValues]))
  end
  

  def pnt_testcase_setparamvalues(hash_csv_line)
    strtemp = "TestCase ID= #{hash_csv_line[:testcase_id]}"
    puts strtemp
    strtemp = "ParamPrefix= #{hash_csv_line[:prefix]};ParamNumIdx= #{hash_csv_line[:numindex]}; ParamName= #{hash_csv_line[:paramname]}; ParamValue= #{csv_line[:paramvalue]}"
    puts strtemp  
  end
  
  def dump_results_csv(fullpathfile,str_tempFileCnt,mode='a')
    begin
      hFile=File.new(fullpathfile,mode)
      
      str_temp=str_tempFileCnt.to_s+','+Time.now.strftime("%m/%d/%Y %H:%M:%S").to_s
      
      hFile.puts str_temp  
      hFile.flush
    rescue SystemCallError
			hFile.close
			File.delete(fullpathfile)
    end    
    hFile.close
    Show_Outcomes.new("Log Results in #{fullpathfile}.").log_screen 
    
  end
  
  def navigator_TR069_WinIE_SetParameterValues(inifile,str_testcaseid,hash_inifile)
    host=hash_inifile[:ACS_IP]
    user=hash_inifile[:SSH_Username]
    passwd=hash_inifile[:SSH_Password]
    testcases_CSV_FILE_PATH = File.join(Dir_autotest_dest, hash_inifile[:CSV_SetValues])
    
    
    hdtr69_do_test=TR69_Do_Test.new(@path_TR069_QAGemtek,@logfilename,hash_inifile[:JavaSDKVer])
    arr_testcaseid=hdtr69_do_test.testcaseid_number(str_testcaseid)
    if arr_testcaseid.length != 2
      puts "number of #{str_testcaseid} isn't equal 2 ..."
      exit
    end
    
    str_cmd=@path_ruby+" "+File.join(@path_tr69autotest_destination,@str_ruby_setvalue)+" "+\
            File.join(@path_tr69autotest_destination,inifile)+" "+str_testcaseid
            
    strtemp="Issue command= #{str_cmd}"
    Show_Outcomes.new(strtemp,@logfilepath).log_screen
    Show_Outcomes.new(strtemp,@logfilepath).log_logfile
    out=Net_SSH_SFTP.new.ssh_sendcmd(host,user,passwd,str_cmd)
    
    str_setparametervalue=nil
    puts "Got inof msg from ACS Server......"
    
    out.stdout.to_a.each {|x| 
      p x.chop
      Show_Outcomes.new(x.chop,@logfilepath).log_logfile
    
      case x.downcase
        when /internetgateway/
          str_setparametervalue=x.chomp if !(x.downcase.include?('apply'))
      end
    }
    
    strtemp= "End got inof msg from ACS Server......"
    Show_Outcomes.new(strtemp,@logfilepath).log_screen
    Show_Outcomes.new(strtemp,@logfilepath).log_logfile
    
    #--------Open M$IE using watir to get DUT info.
    arr_testcaseid.each{|id|
      FasterCSV.foreach(testcases_CSV_FILE_PATH, 
                    :headers           => :first_row,
                    :header_converters => :symbol,
                    :converters        => nil ) do |csv_line|
      
        if csv_line[:testcase_id] == id
          #---Exclude 'Apply' parametername
          if csv_line[:paramname].downcase != 'apply'
            
            case csv_line[:prefix]
              when /_LANHostCfgMgt_VLAN[1-6]/
                #pnt_testcase_setparamvalues(csv_line)
                
                #--------Reboout DUT then check whether setting successful or not 
                #Win_IE_Navgator_TR069.new.resetdefaults_reboot("admin_reboot.htm")
                
                #--------Check value of webpage by WinIE 
                Win_IE_Navgator_TR069.new.TR069_LANHostCfgMgt(csv_line,hash_inifile)
                
                #------log out test result to log file
                #puts str_setparametervalue
                test_result=csv_line[:testcase_id]+','+str_setparametervalue+','+@retstr_navigator_web
                
                logfile_test_result=csv_line[:prefix]+'.'+'csv'
                path_log_test=File.join(@dir_Log,logfile_test_result)
                
                self.dump_results_csv(path_log_test,test_result,'a')
            end;#case csv_line[:prefix]
            
          end;#if csv_line[:paramname].downcase != 'apply'
          
        end;#if csv_line[:testcase_id] == id
      
      end;#do |csv_line|
      
    };#arr_testcaseid.each
    
  end
  def ssh_acs_linux(inifile,str_testcaseid,hash_inifile)
                
    #--------Before ACS server setting to check DUT status
    retcode='000'
    status = TimeoutX::timeout(200) {
      # Something that should be interrupted if it takes too much time...
      begin_time = Time.now
      while retcode != '401'
        sleep 10
        retcode=Check_DUT_httpd_alive.new(hash_inifile[:CPE_Host_LAN_IP],@logfilepath).check_alive
      end
      
      elapsed_time = Time.now - begin_time
      Show_Outcomes.new("Navigator Startup Duration=#{elapsed_time} sec.").log_screen
      Show_Outcomes.new("Navigator Startup Duration=#{elapsed_time} sec.",@logfilepath).log_logfile  
    }
    
    #--------ssh to acs server to excute setvlaue.rb.
    navigator_TR069_WinIE_SetParameterValues(inifile,str_testcaseid,hash_inifile)  
    
    
  end
  def test_all(arr_str_testcaseid)
    
    #--------Open M$IE using watir to set DUT value.
    
    #--------sftp testenv.ini to acs server.
    #--------sftp testcases_setvalues.csv to acs server.
    #--------ssh to acs server to excute setvlaue.rb.
    #--------Open M$IE using watir to get DUT info.
    
    ret_hash_ini=TestEnv_Config.new.Read_TestEnv_ini(@inifile_env)
    
    sshlogin_sftp(@inifile_env,ret_hash_ini)            
    
    arr_str_testcaseid.each{|str_testcaseid|
      Win_IE_Navgator_TR069.new.all_websetting(ret_hash_ini);  sleep 30;
      
      ssh_acs_linux(@inifile_env,str_testcaseid,ret_hash_ini)
    };#arr_str_testcaseid.each
    
  end
end

#-------------------------------------------------------------------------------------------------------------#
# Start Here
#-------------------------------------------------------------------------------------------------------------#
begin_time = Time.now

inifile="testenv.ini"
arr_str_testcaseid_vlan6=['0171,0174','0161,0170','0162,0170','0163,0170',\
                          '0166,0170','0168,0170','0169,0170' ]
arr_str_testcaseid_vlan1=['0130-0131', '0140,0143', '0122,0131','0123,0131', \
                         '0126,0131', '0128,0131', '0129,0131', '0121,0131']
arr_str_testcaseid=arr_str_testcaseid_vlan1+arr_str_testcaseid_vlan6
#arr_str_testcaseid=['0171,0174']

Win_IE_SetParamterValues.new(inifile).test_all(arr_str_testcaseid)

Logfilepath=Win_IE_SetParamterValues.new(inifile).logfilepath

elapsed_time = Time.now - begin_time
Show_Outcomes.new("End Test.  Duration=#{elapsed_time} sec.").log_screen
Show_Outcomes.new("End Test.  Duration=#{elapsed_time} sec.",Logfilepath).log_logfile