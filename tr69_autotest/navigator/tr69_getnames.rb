#-------------------------------------------------------------------------------------------------------------#
# Nov23/07 Initialize by Philip Shen                        
# 
#------------------------------------------------------------------------------------------------------------#
path_tr69_autotest=File.dirname(File.dirname(File.expand_path(__FILE__)))
$LOAD_PATH.unshift File.join(path_tr69_autotest, 'lib') if $0 == __FILE__
path_TR069_QAGemtek=File.dirname(path_tr69_autotest)

require 'API_TR69'   # the TR069 Test API

#-------------------------------------------------------------------------------------------------------------#
# main program
#-------------------------------------------------------------------------------------------------------------#
begin_time = Time.now
arr_CPEParamList=[]
arr_CPEParamList << "InternetGatewayDevice."
arr_CPEParamList << "InternetGatewayDevice.DeviceInfo."
arr_CPEParamList << "InternetGatewayDevice.DeviceConfig."
arr_CPEParamList << "InternetGatewayDevice.ManagementServer."
arr_CPEParamList << "InternetGatewayDevice.Time."
arr_CPEParamList << "InternetGatewayDevice.UserInterface."
arr_CPEParamList << "InternetGatewayDevice.Layer3Forwarding."
arr_CPEParamList << "InternetGatewayDevice.LANConfigSecurity."
arr_CPEParamList << "InternetGatewayDevice.LANDevice."
arr_CPEParamList << "InternetGatewayDevice.LANDevice.1."
arr_CPEParamList << "InternetGatewayDevice.WANDevice."
arr_CPEParamList << "InternetGatewayDevice.WANDevice.1."

logfilename=File.basename($0,'.*')+".txt"
#p "#{logfilename}"

#topdir = File.join(File.dirname(__FILE__), '..')
dir_Log = File.join(path_TR069_QAGemtek,'log')
dir_input = File.join(path_TR069_QAGemtek,"tr69_device_test_suite/bin/inputs")

dir_testsuite = File.join(path_TR069_QAGemtek,"tr69_device_test_suite")

dir_testsuiteConf = File.join(dir_testsuite,"conf")
dir_testsuiteBin = File.join(dir_testsuite,"bin")
dir_testsuiteLIB = File.join(dir_testsuite,"lib")
dir_testsuiteInputs = File.join(dir_testsuiteBin,"inputs")
Logfilepath = File.join(dir_Log,logfilename)

LogCPEParaLists = File.join(dir_Log,"paralist.txt")
LogCPEParam = File.join(dir_Log,"paraname.txt")
LogCPEParamRO = File.join(dir_Log,"paranameRO.txt")
LogCPEParamRW = File.join(dir_Log,"paranameRW.txt")
LogCPEParamValues = File.join(dir_Log,"paranameValues.txt")
LogCPETypeValues = File.join(dir_Log,"paraTypeValues.csv")

if FileTest.exist?(LogCPEParamRO) then File.delete(LogCPEParamRO) end
if FileTest.exist?(LogCPEParamRW) then File.delete(LogCPEParamRW) end

inifile="testenv.ini"
rethash=TestEnv_Config.new.Read_TestEnv_ini(inifile)
if rethash.class != Hash  
  exit
end

#-------------------------------------------------------------------------------------------------------------#
# Start Here
#-------------------------------------------------------------------------------------------------------------#

DUT_IP=rethash[:CPE_Host_LAN_IP]
TestEnv_Config.new.Update_CfgFile(dir_testsuiteConf,'task.properties',\
                                  Logfilepath,\
                                  rethash[:ACS_IP],\
                                  rethash[:ACS_Port],\
                                  rethash[:CPE_Username],\
                                  rethash[:CPE_Passwd],\
                                  rethash[:CPE_Host_WAN_IP],\
                                  rethash[:CPE_Port],\
                                  rethash[:NeedConnRequest],\
                                  rethash[:ConnReqUsername],\
                                  rethash[:ConnReqPassword],\
                                  rethash[:ACS_URI],\
                                  rethash[:ConnReqURI])
                                  
                                  
TR69_Do_Test.new(path_TR069_QAGemtek,logfilename,rethash[:JavaSDKVer]).\
      Get_ParameterNames(arr_CPEParamList[0],DUT_IP)


elapsed_time = Time.now - begin_time
Show_Outcomes.new("End Test.  Duration=#{elapsed_time} sec.").log_screen
Show_Outcomes.new("End Test.  Duration=#{elapsed_time} sec.",Logfilepath).log_logfile