#-------------------------------------------------------------------------------------------------------------#
# Nov23/07 Initialize by Philip Shen                        
# 
#------------------------------------------------------------------------------------------------------------#
path_tr69_autotest=File.dirname(File.dirname(File.expand_path(__FILE__)))
$LOAD_PATH.unshift File.join(path_tr69_autotest, 'lib') if $0 == __FILE__
path_TR069_QAGemtek=File.dirname(path_tr69_autotest)

require 'API_TR69'   # the TR069 Test API

#-------------------------------------------------------------------------------------------------------------#
# main program
#-------------------------------------------------------------------------------------------------------------#
begin_time = Time.now

logfilename=File.basename($0,'.*')+".txt"

#topdir = File.join(File.dirname(__FILE__), '..')

dir_Log = File.join(path_TR069_QAGemtek,'log')
dir_input = File.join(path_TR069_QAGemtek,"tr69_device_test_suite/bin/inputs")

dir_testsuite = File.join(path_TR069_QAGemtek,"tr69_device_test_suite")
dir_testsuiteConf = File.join(dir_testsuite,"conf")
dir_testsuiteBin = File.join(dir_testsuite,"bin")
dir_testsuiteLIB = File.join(dir_testsuite,"lib")

dir_testsuiteInputs = File.join(dir_testsuiteBin,"inputs")
Logfilepath = File.join(dir_Log,logfilename)
LogCPEParaLists = File.join(dir_Log,"paralist.txt")
LogCPEParam = File.join(dir_Log,"paraname.txt")
LogCPEParamRO = File.join(dir_Log,"paranameRO.txt")
LogCPEParamRW = File.join(dir_Log,"paranameRW.txt")
LogCPEParamValues = File.join(dir_Log,"paranameValues.txt")
LogCPETypeValues = File.join(dir_Log,"paraTypeValues.csv")
LogCPEParamType = File.join(dir_Log,"paranameType.txt")
renLogCPEParamType=File.join(dir_Log,File.basename(LogCPEParamType,'.*'))

Diff1_file="diff1.txt"
Diff2_file="diff2.txt"
LogResultFileRO=File.join(dir_Log,'tr69DUTStd-RO.html')
LogResultFileRW=File.join(dir_Log,'tr69DUTStd-RW.html')

TR69_CPEParamRO=File.join(dir_Log,"tr69param-RO.txt")
TR69_CPEParamRW=File.join(dir_Log,"tr69param-RW.txt")
TR69_CPEParam=  File.join(dir_Log,"tr69param.txt")

File.delete(LogCPEParamType) if FileTest.exist?(LogCPEParamType)
File.delete(LogCPEParamValues) if FileTest.exist?(LogCPEParamValues)

inifile="testenv.ini"
rethash=TestEnv_Config.new.Read_TestEnv_ini(inifile)
if rethash.class != Hash  
  exit
end

#-------------------------------------------------------------------------------------------------------------#
# Start Here
#-------------------------------------------------------------------------------------------------------------#

TestEnv_Config.new.Update_CfgFile(dir_testsuiteConf,'task.properties',\
                                  Logfilepath,\
                                  rethash[:ACS_IP],\
                                  rethash[:ACS_Port],\
                                  rethash[:CPE_Username],\
                                  rethash[:CPE_Passwd],\
                                  rethash[:CPE_Host_WAN_IP],\
                                  rethash[:CPE_Port],\
                                  rethash[:NeedConnRequest],\
                                  rethash[:ConnReqUsername],\
                                  rethash[:ConnReqPassword],\
                                  rethash[:ACS_URI],\
                                  rethash[:ConnReqURI])
                                  
DUT_IP=rethash[:CPE_Host_LAN_IP]

handle_TR69DoTest=TR69_Do_Test.new(path_TR069_QAGemtek,logfilename,rethash[:JavaSDKVer])

retval=handle_TR69DoTest.Test_GetValues_atonce(TR69_CPEParamRO,LogCPEParamRO,DUT_IP)

if (retval != false) then
  #Update format for compare "tr69param-xx.txt"
  TR69_Rpt_Generator.new(dir_Log,Diff1_file,Diff2_file,logfilename).\
    file_process(LogCPEParamType,TR69_CPEParamRO)
  
  #Creat RO different html file
  TR69_Rpt_Generator.new(dir_Log,Diff1_file,Diff2_file,logfilename).\
      run(LogResultFileRO,'DUT<RO>tr69Std',nocommon_opt=true)
  
  #Rename file "../log/paranameType.txt" to "../log/paranameType" 
  File.rename(LogCPEParamType,renLogCPEParamType) \
    if FileTest.exist?(LogCPEParamType)

  retval=handle_TR69DoTest.Test_GetValues_atonce(TR69_CPEParamRW,LogCPEParamRW,DUT_IP)
  
  if (retval != false) then
    #Update format for compare "tr69param-xx.txt"
    TR69_Rpt_Generator.new(dir_Log,Diff1_file,Diff2_file,logfilename).\
      file_process(LogCPEParamType,TR69_CPEParamRW)
    
    #Creat RW different html file
    TR69_Rpt_Generator.new(dir_Log,Diff1_file,Diff2_file,logfilename).\
      run(LogResultFileRW,'DUT<RW>tr69Std',nocommon_opt=true)
    
    #Merge both "../log/paranameType" and "../log/paranameType.txt" to
    #"../log/paranameType.txt" then delete "../log/paranameType"
    file1_content=nil;file2_content=nil
    File.open(renLogCPEParamType, "r"){|f| file1_content = f.read}
    File.open(LogCPEParamType, "r"){|f| file2_content = f.read}

    hFile=File.new(LogCPEParamType,'w')
    hFile.puts file1_content
    hFile.puts file2_content
    hFile.flush;hFile.close
    File.delete(renLogCPEParamType) \
      if FileTest.exist?(renLogCPEParamType)
  end

end

elapsed_time = Time.now - begin_time
Show_Outcomes.new("End Test.  Duration=#{elapsed_time} sec.").log_screen
Show_Outcomes.new("End Test.  Duration=#{elapsed_time} sec.",Logfilepath).log_logfile