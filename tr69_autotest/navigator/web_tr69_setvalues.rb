#-------------------------------------------------------------------------------------------------------------#
# dec14/07 Initialize by Philip Shen                        
# 
#------------------------------------------------------------------------------------------------------------#
path_tr69_autotest=File.dirname(File.dirname(File.expand_path(__FILE__)))
$LOAD_PATH.unshift File.join(path_tr69_autotest, 'lib') if $0 == __FILE__
path_TR069_QAGemtek=File.dirname(path_tr69_autotest)

require 'API_TR69'   # the TR069 Test API
require "faster_csv"
#-------------------------------------------------------------------------------------------------------------#
# main program
#-------------------------------------------------------------------------------------------------------------#
begin_time = Time.now

logfilename=File.basename($0,'.*')+".txt"

dir_autotest_navigator=File.join(path_tr69_autotest,"navigator")

dir_autotest_test=File.join(path_tr69_autotest,'test')
path_TR069_QAGemtek=File.dirname(path_tr69_autotest)
    
dir_input = File.join(path_TR069_QAGemtek,"tr69_device_test_suite/bin/inputs")
dir_Log = File.join(path_TR069_QAGemtek,'log')

dir_testsuite = File.join(path_TR069_QAGemtek,"tr69_device_test_suite")
dir_testsuiteBin = File.join(dir_testsuite,"bin")
dir_testsuiteLIB = File.join(dir_testsuite,"lib")
dir_testsuiteConf = File.join(dir_testsuite,"conf")
dir_testsuiteInputs = File.join(dir_testsuiteBin,"inputs")


Logfilepath = File.join(dir_Log,logfilename)
LogCPEParaLists = File.join(dir_Log,"paralist.txt")
LogCPEParam = File.join(dir_Log,"paraname.txt")
LogCPEParamRO = File.join(dir_Log,"paranameRO.txt")
LogCPEParamRW = File.join(dir_Log,"paranameRW.txt")
LogCPEParamValues = File.join(dir_Log,"paranameValues.txt")
LogCPETypeValues = File.join(dir_Log,"paraTypeValues.csv")

if ARGV.length < 1
  strtemp="!!!----- Pls input absolute path of testenv.ini file first. -----!!!"
  Show_Outcomes.new(strtemp).log_screen
  exit
end
inifile=ARGV[0].to_s
rethash=TestEnv_Config.new.Read_TestEnv_ini(inifile)
if rethash.class != Hash  
  exit
end
#-------get the same path of inifile
dir_inifile=File.dirname(inifile)
TestCases_CSV_FILE_PATH = File.join(dir_inifile, rethash[:CSV_SetValues])
#-------------------------------------------------------------------------------------------------------------#
# Start Here
#-------------------------------------------------------------------------------------------------------------#
if ARGV.length < 2
  strtemp="!!!----- Pls input testcase IDs from #{rethash[:CSV_SetValues]} second. -----!!!"
  Show_Outcomes.new(strtemp).log_screen
  strtemp="!!!----- By order like 0157,0006,0158-0161,                         -----!!!"
  Show_Outcomes.new(strtemp).log_screen
  exit
end

TestEnv_Config.new.Update_CfgFile(dir_testsuiteConf,'task.properties',\
                                  Logfilepath,\
                                  rethash[:ACS_IP],\
                                  rethash[:ACS_Port],\
                                  rethash[:CPE_Username],\
                                  rethash[:CPE_Passwd],\
                                  rethash[:CPE_Host_WAN_IP],\
                                  rethash[:CPE_Port],\
                                  rethash[:NeedConnRequest],\
                                  rethash[:ConnReqUsername],\
                                  rethash[:ConnReqPassword],\
                                  rethash[:ACS_URI],\
                                  rethash[:ConnReqURI])                             
    
str_testcaseid=ARGV[1].to_s
    
hdtr69_do_test=TR69_Do_Test.new(path_TR069_QAGemtek,logfilename,rethash[:JavaSDKVer])
arr_testcaseid=hdtr69_do_test.testcaseid_number(str_testcaseid)
    
arr_testcaseid.each{|id|
      
  FasterCSV.foreach(TestCases_CSV_FILE_PATH, 
                    :headers           => :first_row,
                    :header_converters => :symbol,
                    :converters        => nil ) do |csv_line|
      
    if csv_line[:testcase_id] == id
      #-------------------------------------------
      # Diff two kinds status:
      # 0006,_WLANCfg,:numindex 1,Apply,
      # 0121,_LANHostCfgMgt_VLAN2,,DHCPServerEnable,1
      #-------------------------------------------
      if csv_line[:numindex].class == String
        if csv_line[:numindex].split(' ').last.include?('.')
          arr_line_index_opt=csv_line[:numindex].split(' ').last.split('.')
        else  
          arr_line_index_opt=csv_line[:numindex].split(' ').last
        end
        
      else
        arr_line_index_opt=[nil]
      end;#if csv_line[:numindex].class == String
        
      csvfilename="paraTypeValues"+csv_line[:prefix]+".csv"
      csvCPETypeValues = File.join(dir_Log,csvfilename)
      
      arr_line_index_opt.each{|y| 
        #-------------------------------------------
        # Diff two kinds status:
        # 0006,_WLANCfg,:numindex 1,Apply,
        # 0121,_LANHostCfgMgt_VLAN2,,DHCPServerEnable,1
        #-------------------------------------------
        if y == nil
          hash_update=nil
        else
          hash_update={ :numindex => y.to_s }
        end
            
        handle_HashTable=hdtr69_do_test.Hash_table_select(csv_line[:prefix],hash_update)

        arr_SetParamValue= hdtr69_do_test.GetSet_ParamList_make(handle_HashTable,'Set',csv_line,hash_update)
        
        arr_SetParamValue.each{|str_setparaname|
          strtemp = '----------------------------------------------------------------------------'
          puts strtemp
          strtemp = "TestCase ID= #{csv_line[:testcase_id]}"
          puts strtemp
          strtemp = "ParamPrefix= #{csv_line[:prefix]};ParamNumIdx= #{csv_line[:numindex]}; ParamName= #{csv_line[:paramname]}; ParamValue= #{csv_line[:paramvalue]}"
          puts strtemp
          strtemp ="SetValues Param is"
          puts strtemp
          puts str_setparaname.to_s
          strtemp = '----------------------------------------------------------------------------'
          puts strtemp
          
          #-------- Set Value test
          hdtr69_do_test.Test_SetValues(str_setparaname,csvCPETypeValues,csv_line)
        };#arr_SetParamValue.each
        
      };#arr_line_index_opt.each
          
    end;#if csv_line[:testcase_id] == id
      
  end;#do |csv_line|
};#arr_testcaseid.each

elapsed_time = Time.now - begin_time
Show_Outcomes.new("End Test.  Duration=#{elapsed_time} sec.").log_screen
Show_Outcomes.new("End Test.  Duration=#{elapsed_time} sec.",Logfilepath).log_logfile